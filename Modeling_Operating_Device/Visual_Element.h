#include "stdafx.h"

namespace Modeling_Operating_Device
{
    using namespace System;
    using namespace System::Drawing;

    public ref class Visual_Element
    {
        public:
            array<Point>^ points;

            Graphics^ graphics;
            Pen^ pen;
            System::Windows::Forms::Label^ label;

            Point size,
                  center;

            virtual Point get_size  ();
            virtual Point get_center();

            virtual void paint(bool anyway);
            virtual void set_color(Color color);
            Color get_color();
            void set_text(String^ text);

            Color^ color_prev;
            bool is_color_changed();
    };
}
