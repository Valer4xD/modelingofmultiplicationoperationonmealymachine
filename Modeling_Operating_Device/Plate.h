#include "stdafx.h"

namespace Modeling_Operating_Device
{
    ref class Plate : Visual_Element
    {
        public:
            Plate(Plate^ previous);
            Plate(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, String^ text, int X_center, int Y_center);
    };
}
