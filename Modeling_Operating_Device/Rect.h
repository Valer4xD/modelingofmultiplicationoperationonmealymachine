#include "stdafx.h"

namespace Modeling_Operating_Device
{
    public ref class Rect : Visual_Element
    {
        public:
            Point up_left;

            Rect(Rect^ previous);
            Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, String^ text, int width, int height, int X_center, int Y_center);
            Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, String^ text, int X_center, int Y_center);
            Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, int width, int height, int X_center, int Y_center);
            Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, int X_center, int Y_center);

            virtual void paint(bool anyway) override;
    };
}