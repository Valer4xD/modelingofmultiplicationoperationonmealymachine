#include "stdafx.h"

namespace Modeling_Operating_Device
{
    using namespace System;
    using namespace System::Drawing;

    public ref class Line : Visual_Element
    {
        public:
            bool add_label;

            array<Point>^ arrow;
            int figure;
            int vertex_figure;

            array<Point>^ outs;
            int in_type,
                in_id;

            Line(Line^ previous);
            Line(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, array<Point>^ _points, String^ text, int X_offset, int Y_offset);
            Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, array<Point>^ _outs, int _in_type, int _in_id, bool _add_arrow, int _vertex_figure);
            Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, array<Point>^ _outs, int _in_type, int _in_id, bool _add_arrow, int _vertex_arrow, System::Windows::Forms::Label^ _label, String^ text, int X_offset, int Y_offset);
            Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, int _figure, int _vertex_figure, System::Windows::Forms::Label^ _label, String^ text, int vertex_label, int X_offset, int Y_offset);
            Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, int _figure, int _vertex_figure);

            bool offset(double BORDER, double &X_1, double &Y_1, double X_2, double Y_2, bool direction);
            void Create_Arrow();

            virtual void paint(bool anyway)     override;
            virtual void set_color(Color color) override;

            bool is_connect_elements(MP_Snapshot cur_snapshot, MP_Snapshot pre_snapshot);
            bool is_connect_lines(Line^ next_line);
    };
}
