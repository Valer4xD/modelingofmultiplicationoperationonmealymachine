#include "stdafx.h"

namespace Modeling_Operating_Device
{
    Line::Line(Line^ previous) {}

    Line::Line(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, array<Point>^ _points, String^ text, int X_offset, int Y_offset)
    {
        add_label = true;

        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label           = _label;

        points = _points;

        label->Text     = text;
        label->Location = Point(points[0].X + (points[1].X - points[0].X) / 2 + X_offset,
                                points[0].Y + (points[1].Y - points[0].Y) / 2 + Y_offset);
        label->ForeColor = pen->Color;
    }

    bool Line::offset(double BORDER, double &X_1, double &Y_1, double X_2, double Y_2, bool direction)
    {
        double dX = X_1 - X_2;
        double dY = Y_1 - Y_2;
        
        
        double hypotenuse = sqrt(dX * dX + dY * dY);
        if(hypotenuse != 0.0)
        {
            int coef = 1;

            if(direction)   coef *= -1;

            X_1 += BORDER / hypotenuse * dY * coef;
            Y_1 -= BORDER / hypotenuse * dX * coef;

            /*
            X_2 += BORDER / hypotenuse * dY * coef;
            Y_2 -= BORDER / hypotenuse * dX * coef;
            */

            return true;
        }

        return false;
    }

    void Line::Create_Arrow()
    {
        // double arrow_len     = 10.0,
        double arrow_len        =  5.0,
               arrow_half_width =  3.0,

               dX = points[vertex_figure].X - points[vertex_figure - 1].X,
               dY = points[vertex_figure].Y - points[vertex_figure - 1].Y,
               hypotenuse = sqrt(dX * dX + dY * dY),

               diff = hypotenuse / arrow_len,
               height_X = points[vertex_figure].X - dX / diff,
               height_Y = points[vertex_figure].Y - dY / diff,

               aX = height_X,
               aY = height_Y,
               bX = height_X,
               bY = height_Y;
        
        offset(arrow_half_width, aX, aY, points[vertex_figure].X, points[vertex_figure].Y, true);
        offset(arrow_half_width, bX, bY, points[vertex_figure].X, points[vertex_figure].Y, false);

        arrow = gcnew array<Point> { Point((int)(aX + 0.5), (int)(aY + 0.5)),
                                     Point((int)(bX + 0.5), (int)(bY + 0.5)) };
    }
    

    Line::Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, array<Point>^ _outs, int _in_type, int _in_id, bool _add_arrow, int _vertex_arrow)
    {
        add_label = false;

        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());

        points = _points;

        outs = _outs;
        in_type  = _in_type;
        in_id    = _in_id;

        if(figure = _add_arrow)
        {
            int last_vertex = points->Length - 1;

            if((vertex_figure = _vertex_arrow) <= 0)        vertex_figure = last_vertex + vertex_figure;

            Create_Arrow();
        }
    }

    Line::Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, array<Point>^ _outs, int _in_type, int _in_id, bool _add_arrow, int _vertex_arrow, System::Windows::Forms::Label^ _label, String^ text, int X_offset, int Y_offset)
    {
        add_label = true;

        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());

        points = _points;

        outs = _outs;
        in_type  = _in_type;
        in_id    = _in_id;

        if(figure = _add_arrow)
        {
            int last_vertex = points->Length - 1;

            if((vertex_figure = _vertex_arrow) <= 0)        vertex_figure = last_vertex + vertex_figure;

            Create_Arrow();
        }

        label           = _label;
        label->Text     = text;
        label->Location = Point(points[0].X + X_offset,
                                points[0].Y + Y_offset);
        label->ForeColor = pen->Color;
    }

    Line::Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, int _figure, int _vertex_figure, System::Windows::Forms::Label^ _label, String^ text, int vertex_label, int X_offset, int Y_offset)
    {
        add_label = true;

        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());

        points = _points;

        if(figure = _figure)
        {
            int last_vertex = points->Length - 1;

            if((vertex_figure = _vertex_figure) <= 0)       vertex_figure = last_vertex + vertex_figure;

            if(figure == 1)     Create_Arrow();
        }

        label           = _label;
        label->Text     = text;
        label->Location = Point(points[vertex_label].X + X_offset,
                                points[vertex_label].Y + Y_offset);
        label->ForeColor = pen->Color;
    }

    Line::Line(Graphics^ _graphics, Pen^ _pen, array<Point>^ _points, int _figure, int _vertex_figure)
    {
        add_label = false;

        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());

        points = _points;

        if(figure = _figure)
        {
            int last_vertex = points->Length - 1;

            if((vertex_figure = _vertex_figure) <= 0)       vertex_figure = last_vertex + vertex_figure;

            if(figure == 1)     Create_Arrow();
        }
    }

    void Line::paint(bool anyway)
    {
        if( ! is_color_changed() && ! anyway) return;

        int last_id = points->Length - 1;
        for(int p = 1; p <= last_id; ++ p)
            graphics->DrawLine(pen, points[p - 1], points[p]);

        switch(figure)
        {
            case 1:
            {
                // graphics->DrawLine(pen, points[vertex_figure], arrow[0]);
                // graphics->DrawLine(pen, points[vertex_figure], arrow[1]);
                SolidBrush^ brush = gcnew SolidBrush(pen->Color);
                array<Point>^ _points = 
                {
                    points[vertex_figure],
                    arrow[0],
                    arrow[1]
                };
                graphics->FillPolygon(brush, _points);
                break;
            }
            case 2:
            {
                SolidBrush^ brush = gcnew SolidBrush(pen->Color);
                graphics->FillEllipse(brush, points[vertex_figure].X - 3, points[vertex_figure].Y - 3, 7, 7);
            }
        }
    }

    void Line::set_color(Color color)
    {
        pen->Color = color;

        if(add_label)   label->ForeColor = color;
    }

    bool Line::is_connect_elements(MP_Snapshot cur_snapshot, MP_Snapshot pre_snapshot)
    {
        if(in_type == cur_snapshot.type
        && in_id   == cur_snapshot.id  )
        {
            int num_out = outs->Length;
            for(int o = 0; o < num_out; ++ o)
                if(outs[o].X == pre_snapshot.type
                && outs[o].Y == pre_snapshot.id  )
                    return true;
        }

        return false;
    }

    bool Line::is_connect_lines(Line^ next_line)
    {
        if(this->in_type == next_line->in_type
        && this->in_id   == next_line->in_id  )
            return true;

        return false;
    }
}
