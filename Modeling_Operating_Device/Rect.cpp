#include "stdafx.h"

namespace Modeling_Operating_Device
{
    Rect::Rect(Rect^ previous) {}

    Rect::Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, String^ text, int width, int height, int X_center, int Y_center)
    {
        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label = _label;

        size    = Point(width, height);
        center  = Point(X_center, Y_center);
        up_left = Point(X_center - width / 2, Y_center - height / 2);

        label->Text = text;
        label->Location = Point(X_center - label->Width / 2, Y_center - label->Height / 2);
        label->ForeColor = pen->Color;
    }

    Rect::Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, String^ text, int X_center, int Y_center)
    {
        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label = _label;

        size    = _size;
        center  = Point(X_center, Y_center);
        up_left = Point(X_center - size.X / 2, Y_center - size.Y / 2);

        label->Text = text;
        label->Location = Point(X_center - label->Width / 2, Y_center - label->Height / 2);
        label->ForeColor = pen->Color;
    }

    Rect::Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, int width, int height, int X_center, int Y_center)
    {
        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label = _label;

        size    = Point(width, height);
        center  = Point(X_center, Y_center);
        up_left = Point(X_center - width / 2, Y_center - height / 2);

        label->Location = Point(X_center - label->Width / 2, Y_center - label->Height / 2);
        label->ForeColor = pen->Color;
    }

    Rect::Rect(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, int X_center, int Y_center)
    {
        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label = _label;

        size    = _size;
        center  = Point(X_center, Y_center);
        up_left = Point(X_center - size.X / 2, Y_center - size.Y / 2);

        label->Location = Point(X_center - label->Width / 2, Y_center - label->Height / 2);
        label->ForeColor = pen->Color;
    }

    void Rect::paint(bool anyway)
    {
        if(! is_color_changed() && ! anyway) return;

        graphics->DrawRectangle(pen, up_left.X, up_left.Y, size.X, size.Y);
    }
}
