#include "stdafx.h"

namespace Modeling_Operating_Device
{
    Rhomb::Rhomb(Rhomb^ previous) {}

    Rhomb::Rhomb(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, String^ text, int X_center, int Y_center)
    {
        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label = _label;

        size   = _size;
        center = Point(X_center, Y_center);

        points = gcnew array<Point>(4);
        points[0] = Point(X_center -  size.X / 2,   Y_center);
        points[1] = Point(X_center,                 Y_center - size.Y / 2);
        points[2] = Point(X_center +  size.X / 2,   Y_center);
        points[3] = Point(X_center,                 Y_center + size.Y / 2);


        label->Text = text;
        label->Location = Point(X_center - label->Width / 2, Y_center - label->Height / 2);
        label->ForeColor = pen->Color;
    }

    Rhomb::Rhomb(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, int X_center, int Y_center)
    {
        graphics = _graphics;
        pen = dynamic_cast<Pen^>(_pen->Clone());
        label = _label;

        size   = _size;
        center = Point(X_center, Y_center);

        points = gcnew array<Point>(4);
        points[0] = Point(X_center -  size.X / 2,   Y_center);
        points[1] = Point(X_center,                 Y_center - size.Y / 2);
        points[2] = Point(X_center +  size.X / 2,   Y_center);
        points[3] = Point(X_center,                 Y_center + size.Y / 2);


        label->Location = Point(X_center - label->Width / 2, Y_center - label->Height / 2);
        label->ForeColor = pen->Color;
    }
}
