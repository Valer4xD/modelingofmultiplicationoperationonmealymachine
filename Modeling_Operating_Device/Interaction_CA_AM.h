#include "stdafx.h"

// ������� � ���������� �������� �� ���������.
#define _set_bit(var,bit)                   ( var |=   1 << (bit)  )
#define _clear_bit(var,bit)                 ( var &= ~(1 << (bit)) )
#define _is_set_bit(var,bit)                ( var &    1 << (bit)  )
// ����� ����� ��� ������� ����� _set_range � _get_range �� ������������ - �� ����������� ���������.
// _set_range � _get_range ������� �� ������.
#define _get_range(var,left,right)          ( (var & ~(~0 << ((left) - (right) + 1)) << (right)) >> (right) )
#define _set_range(dest,left,right,src)     dest = dest & ~( _get_range(~0, (left), (right)) << (right) ) | (src & ~( ~0 << ((left) - (right) + 1) )) << (right)

namespace Modeling_Operating_Device
{
    ref class Interaction_CA_AM
    {
        public:
            Chip_Snapshot dat;

            Micro_Programm micro_program;

            void init(MP_Snapshot snapshot);

            // DC - ����������.
            void decoder(const byte mem_states);

            // �� Y - �������������� ���������� ����� Y.
            void combination_logic_circuit_Y(const byte unitary_code, const unsigned short X_vector, const byte mem_conditions);

            // �� - ������������ �������.
            void operating_automat(const unsigned short Y_vector);

            // �� D - �������������� ���������� ����� D.
            void combination_logic_circuit_D(const byte unitary_code, const unsigned short X_vector, const byte mem_conditions);

            // ��� - ���������� � ������ ���������� �������.
            void save_memory_logical_conditions(const byte X_vector);

            // �� - ���������� � ������ ���������.
            void save_memory_state(const byte D_vector);

            byte tact();

            void set_micro_program_state();
    };
}
