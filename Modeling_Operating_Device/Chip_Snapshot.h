#include "stdafx.h"

typedef unsigned char byte;

namespace Modeling_Operating_Device
{
    public value class Chip_Snapshot
    {
        public:
                                                    //  Бит:    Описание:
                      byte mem_states;              //   3      ПС.     Память состояний.                           = Кол-ву D-триггеров.
                      byte unitary_code;            //   5      Унитарный код состояния.                            = (int)(log2(Кол-во состояний = 5) + 0.5).
            unsigned short X_vector;                //  10      Вектор логических условий.                          = Кол-ву условий.
                      byte mem_conditions;          //   5      ПЛУ.    Память логических условий.                  Для изменяющегося X_vector(7:3).
            unsigned short Y_vector;                //  13      Вектор выходных сигналов.                           = Кол-ву микрокоманд.
                      byte D_vector;                //   3      Вектор управления элементами памяти состояний.      = Кол-ву D-триггеров
    };
}
