#include "stdafx.h"

typedef unsigned char byte;

namespace Modeling_Operating_Device
{
    public value class MP_Snapshot
    {
        public:
            byte type;
            unsigned short id,
                           A,   // Множимое         A (15:0)
                           B;   // Множитель        B (15:0)
            unsigned C;         // Произведение     C (31:0)
            byte D,             // Знак множителя   D ( 1:0)
                 counter,       // Счетчик          CЧ( 3:0)
                 state;         // Состояние          ( 3:0)
    };
}
