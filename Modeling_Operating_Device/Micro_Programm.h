#include "stdafx.h"

// ������� � ���������� �������� �� ���������.
#define _set_bit(var,bit)                   ( var |=   1 << (bit)  )
#define _clear_bit(var,bit)                 ( var &= ~(1 << (bit)) )
#define _is_set_bit(var,bit)                ( var &    1 << (bit)  )
// ����� ����� ��� ������� ����� _set_range � _get_range �� ������������ - �� ����������� ���������.
// _set_range � _get_range ������� �� ������.
#define _get_range(var,left,right)          ( (var & ~(~0 << ((left) - (right) + 1)) << (right)) >> (right) )
#define _set_range(dest,left,right,src)     dest = dest & ~( _get_range(~0, (left), (right)) << (right) ) | (src & ~( ~0 << ((left) - (right) + 1) )) << (right)

namespace Modeling_Operating_Device
{
    ref class Micro_Programm
    {
        public:
            MP_Snapshot dat;
    
            List<MP_Snapshot> snapshots;
            Color black;
            Color red;

            void init(MP_Snapshot snapshot);

            void sub(byte %left, byte right);

            void y0();
            void y1();
            void y2();
            void y3();
            void y4();
            void y5();
            void y6();
            void y7();
            void y8();
            void y9();
            void y10();
            void y11();
            void y12();

            bool x0();
            bool x1();
            bool x2();
            bool x3();
            bool x4();
            bool x5();
            bool x6();
            bool x7();
            bool x8();
            bool x9();

            byte tact();

            void save(int _type, int _id);
    };
}
