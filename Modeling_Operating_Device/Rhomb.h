#include "stdafx.h"

namespace Modeling_Operating_Device
{
    ref class Rhomb : Visual_Element
    {
        public:
            Rhomb(Rhomb^ previous);
            Rhomb(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, String^ text, int X_center, int Y_center);
            Rhomb(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, int X_center, int Y_center);
    };
}
