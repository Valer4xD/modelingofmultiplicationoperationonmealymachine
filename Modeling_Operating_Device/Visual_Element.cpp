#include "stdafx.h"

namespace Modeling_Operating_Device
{
    Point Visual_Element::get_size  ()  {   return size;    }
    Point Visual_Element::get_center()  {   return center;  }

    void Visual_Element::paint(bool anyway)
    {
        if( ! is_color_changed() && ! anyway)   return;

        graphics->DrawPolygon(pen, points);
    }

    void Visual_Element::set_color(Color color)
    {
        pen->Color = color;
        label->ForeColor = color;
    }

    Color Visual_Element::get_color() { return pen->Color; }

    void Visual_Element::set_text(String^ text)
    {
        label->Text = text;
        label->Location = Point(get_center().X - label->Width / 2, get_center().Y - label->Height / 2);
    }

    bool Visual_Element::is_color_changed()
    {
        if (nullptr == color_prev
        ||(color_prev->R != pen->Color.R
        || color_prev->G != pen->Color.G
        || color_prev->B != pen->Color.B))
        {
            color_prev = pen->Color;
            return true;
        }
        else
            return false;
    }
}
