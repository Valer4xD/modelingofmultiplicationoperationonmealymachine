﻿#include "stdafx.h"

// Пробелы в заголовоки макросов не добавлять.
// Когда нужны все разряды числа _set_range и _get_range не использовать - не определённое поведение.
// _set_range и _get_range местами не менять.
#define _get_range(var,left,right)          ( (var & ~(~0 << ((left) - (right) + 1)) << (right)) >> (right) )
#define _set_range(dest,left,right,src)     dest = dest & ~( _get_range(~0, (left), (right)) << (right) ) | (src & ~( ~0 << ((left) - (right) + 1) )) << (right)
#define $ ->default

namespace Modeling_Operating_Device
{
    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;
    using namespace System::Collections::Generic;

    /// <summary>
    /// Сводка для Form1
    ///
    /// Внимание! При изменении имени этого класса необходимо также изменить
    ///          свойство имени файла ресурсов ("Resource File Name") для средства компиляции управляемого ресурса,
    ///          связанного со всеми файлами с расширением .resx, от которых зависит данный класс. В противном случае,
    ///          конструкторы не смогут правильно работать с локализованными
    ///          ресурсами, сопоставленными данной форме.
    /// </summary>
    public ref class Form1 : public System::Windows::Forms::Form
    {
        public:
            Form1(void);

        protected:
            /// <summary>
            /// Освободить все используемые ресурсы.
            /// </summary>
            ~Form1();

        private: System::Windows::Forms::Timer^  timer1;
        private: System::Windows::Forms::ErrorProvider^  errorProvider1;
        private: System::Windows::Forms::Panel^  panel1;
        private: System::Windows::Forms::CheckBox^  checkBox3;
        private: System::Windows::Forms::CheckBox^  checkBox2;
        private: System::Windows::Forms::CheckBox^  checkBox1;
        private: System::Windows::Forms::Button^  button7;
        private: System::Windows::Forms::Button^  button6;

        private: System::Windows::Forms::DataGridView^  dataGridView_C;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column32;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column31;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column30;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column29;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column28;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column27;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column26;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column25;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column24;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column23;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column22;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column21;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column20;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column19;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column18;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column17;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn33;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn34;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn35;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn36;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn37;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn38;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn39;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn40;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn41;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn42;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn43;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn44;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn45;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn46;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn47;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn48;
        private: System::Windows::Forms::DataGridView^  dataGridView_X_save;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn99;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn100;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn101;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn105;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn106;
        private: System::Windows::Forms::DataGridView^  dataGridView_X_common;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn95;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn96;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn102;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn103;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn104;
        private: System::Windows::Forms::DataGridView^  dataGridView_D_vector;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn60;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn80;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn81;
        private: System::Windows::Forms::DataGridView^  dataGridView_Y_vector;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn82;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn83;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn84;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn85;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn86;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn87;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn88;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn89;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn90;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn91;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn92;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn93;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn94;
        private: System::Windows::Forms::DataGridView^  dataGridView_mem_conditions;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn55;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn56;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn57;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn58;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn59;
        private: System::Windows::Forms::DataGridView^  dataGridView_X_vector;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn61;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn66;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn67;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn68;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn69;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn75;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn76;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn77;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn78;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn79;
        private: System::Windows::Forms::DataGridView^  dataGridView_unitary_code;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn70;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn71;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn72;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn73;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn74;
        private: System::Windows::Forms::DataGridView^  dataGridView_mem_states;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn52;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn53;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn54;
        private: System::Windows::Forms::Label^  label88;
        private: System::Windows::Forms::Label^  label87;
        private: System::Windows::Forms::DataGridView^  dataGridView_state;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn49;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn50;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn51;
        private: System::Windows::Forms::DataGridView^  dataGridView_counter;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn62;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn63;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn65;
        private: System::Windows::Forms::DataGridView^  dataGridView_D;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn64;
        private: System::Windows::Forms::DataGridView^  dataGridView_B_run;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn17;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn18;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn19;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn20;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn21;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn22;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn23;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn24;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn25;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn26;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn27;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn28;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn29;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn30;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn31;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn32;
        private: System::Windows::Forms::DataGridView^  dataGridView_B_src;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn1;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn2;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn3;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn4;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn5;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn6;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn7;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn8;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn9;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn10;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn11;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn12;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn13;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn14;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn15;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn16;
        private: System::Windows::Forms::Label^  label86;
        private: System::Windows::Forms::Label^  label85;
        private: System::Windows::Forms::Label^  label84;
        private: System::Windows::Forms::DataGridView^  dataGridView_A;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column1;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column2;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column3;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column4;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column5;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column6;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column7;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column8;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column9;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column10;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column11;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column12;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column13;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column14;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column15;
        private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column16;
        private: System::Windows::Forms::Label^  label83;
        private: System::Windows::Forms::Label^  label82;
        private: System::Windows::Forms::Label^  label81;
        private: System::Windows::Forms::Label^  label80;
        private: System::Windows::Forms::Label^  label79;
        private: System::Windows::Forms::Label^  label78;
        private: System::Windows::Forms::Label^  label77;
        private: System::Windows::Forms::Label^  label76;
        private: System::Windows::Forms::Label^  label75;
        private: System::Windows::Forms::Label^  label74;
        private: System::Windows::Forms::Label^  label73;
        private: System::Windows::Forms::Label^  label72;
        private: System::Windows::Forms::Label^  label71;
        private: System::Windows::Forms::Label^  label70;
        private: System::Windows::Forms::Label^  label69;
        private: System::Windows::Forms::Label^  label68;
        private: System::Windows::Forms::Label^  label67;
        private: System::Windows::Forms::Label^  label66;
        private: System::Windows::Forms::Label^  label65;
        private: System::Windows::Forms::Label^  label64;
        private: System::Windows::Forms::Label^  label63;
        private: System::Windows::Forms::Label^  label62;
        private: System::Windows::Forms::Label^  label61;
        private: System::Windows::Forms::Label^  label60;
        private: System::Windows::Forms::Label^  label59;
        private: System::Windows::Forms::Label^  label58;
        private: System::Windows::Forms::Label^  label57;
        private: System::Windows::Forms::Label^  label56;
        private: System::Windows::Forms::Label^  label55;
        private: System::Windows::Forms::Label^  label54;
        private: System::Windows::Forms::Label^  label53;
        private: System::Windows::Forms::Label^  label52;
        private: System::Windows::Forms::Label^  label51;
        private: System::Windows::Forms::Label^  label50;
        private: System::Windows::Forms::Label^  label49;
        private: System::Windows::Forms::Label^  label48;
        private: System::Windows::Forms::Label^  label47;
        private: System::Windows::Forms::Label^  label46;
        private: System::Windows::Forms::Label^  label45;
        private: System::Windows::Forms::Label^  label44;
        private: System::Windows::Forms::Label^  label43;
        private: System::Windows::Forms::Label^  label42;
        private: System::Windows::Forms::Label^  label41;
        private: System::Windows::Forms::Label^  label40;
        private: System::Windows::Forms::Label^  label39;
        private: System::Windows::Forms::Label^  label38;
        private: System::Windows::Forms::Label^  label37;
        private: System::Windows::Forms::Label^  label36;
        private: System::Windows::Forms::Label^  label35;
        private: System::Windows::Forms::Label^  label34;
        private: System::Windows::Forms::Label^  label33;
        private: System::Windows::Forms::Label^  label32;
        private: System::Windows::Forms::Label^  label31;
        private: System::Windows::Forms::Button^  button3;
        private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
        private: System::Windows::Forms::Label^  label30;
        private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
        private: System::Windows::Forms::Label^  label29;
        private: System::Windows::Forms::Label^  label28;
        private: System::Windows::Forms::Label^  label27;
        private: System::Windows::Forms::Label^  label26;
        private: System::Windows::Forms::Label^  label25;
        private: System::Windows::Forms::Label^  label24;
        private: System::Windows::Forms::Label^  label23;
        private: System::Windows::Forms::Label^  label22;
        private: System::Windows::Forms::Label^  label21;
        private: System::Windows::Forms::Label^  label20;
        private: System::Windows::Forms::Label^  label19;
        private: System::Windows::Forms::Label^  label18;
        private: System::Windows::Forms::Label^  label17;
        private: System::Windows::Forms::Label^  label16;
        private: System::Windows::Forms::Label^  label15;
        private: System::Windows::Forms::Label^  label14;
        private: System::Windows::Forms::Label^  label13;
        private: System::Windows::Forms::Label^  label12;
        private: System::Windows::Forms::Label^  label11;
        private: System::Windows::Forms::Label^  label10;
        private: System::Windows::Forms::Label^  label9;
        private: System::Windows::Forms::Label^  label8;
        private: System::Windows::Forms::Label^  label7;
        private: System::Windows::Forms::Label^  label6;
        private: System::Windows::Forms::Label^  label5;
        private: System::Windows::Forms::Label^  label4;
        private: System::Windows::Forms::Label^  label3;
        private: System::Windows::Forms::Label^  label2;
        private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::Label^  label89;
private: System::Windows::Forms::Label^  label90;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Button^  button1;






        private:
            /// <summary>
            /// Требуется переменная конструктора.
            /// </summary>
            System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code
            /// <summary>
            /// Обязательный метод для поддержки конструктора - не изменяйте
            /// содержимое данного метода при помощи редактора кода.
            /// </summary>
            void InitializeComponent(void);
#pragma endregion

        public:
            bool need_update_MP;
            bool need_update_chip;

            bool change_management;

            int time_step_counter;

            int run;
            static const int STOP         = 0,
                             MICROPROGRAM = 1,
                             CHIP         = 2;

            bool mp_end;
            bool chip_end;

            int mp_right_border;
            int chip_right_border;
            int mp_tact_right_border;
            int chip_tact_right_border;

            List<int> mp_tact_borders;
            List<int> chip_tact_borders;

            List<MP_Snapshot>    mp_snapshots;
            List<MP_Snapshot>    chip_mp_snapshots;
            List<Chip_Snapshot>^ chip_snapshots;





            Graphics^ graphics;
            List<List<Visual_Element^>^> elements;
            List<Line^> lines;

            Color default_color;
            Color work_color;

            Rect^ MS_rect;
            Trapeze^ DC_trapeze;
            Trapeze^ CLC_D_trapeze;

            Rect^ MLC_rect;
            Trapeze^ CLC_Y_trapeze;
            Rect^ OA_rect;

            Line^ reset;
            Line^ SP;
            Line^ SP_MS;
            Line^ SP_MLC;
            Line^ MS_DC;
            Line^ DC;
            Line^ DC_CLC_D;
            Line^ DC_CLC_Y;
            Line^ CLC_D_MS_out;
            Line^ CLC_D_MS_in;
            Line^ CLC_Y_OA;
            Line^ OA_out;
            Line^ OA_MLC;
            Line^ X0;
            Line^ MLC;
            Line^ OA_in;
            Line^ OA_CLC_Y;
            Line^ OA_CLC_D;

        private: void reset_values();
        private: void build_GSA();
        private: void build_chip();
        private: void paint_GSA(bool draw_active, bool all);
        private: void paint_chip(bool draw_active, bool all);
        private: void mp_reset_color();
        private: void chip_reset_color();
        private: void mp_show_registers(MP_Snapshot snapshot);
        private: void chip_show_registers(List<Chip_Snapshot>^ chip_snapshots, int right_border);
        private: void mp_zero_registers();
        private: void chip_zero_registers();
        private: void calculate_MP();
        private: void calculate_chip();

        private: void set_dataGridView(System::Windows::Forms::DataGridView^ dataGridView, int _register);
        private: String^ convert_to_label(System::Windows::Forms::DataGridView^ dataGridView);
        private: int convert_to_register(System::Windows::Forms::DataGridView^ dataGridView);
        private: void switch_GSA(bool src);
        private: void mp_work_draw(int left, int right);

        private: void paint_work_chip(int left_border, int right_border);

        private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e);

        private: System::Void timer1_Tick(System::Object^ sender, System::EventArgs^ e);
        private: System::Boolean is_active(Visual_Element^ element);
        private: System::Void paint(bool all);
        private: System::Void Form1_Shown(System::Object^ sender, System::EventArgs^ e);
        private: System::Void Form1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e);

        private: System::Void dataGridView_A_ColumnHeaderMouseClick    (System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        private: System::Void dataGridView_B_src_ColumnHeaderMouseClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e);
        private: System::Void checkBox1_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e);
        private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e);

        private: System::Void checkBox2_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        private: System::Void checkBox3_CheckedChanged(System::Object^ sender, System::EventArgs^ e);
        private: System::Void numericUpDown1_ValueChanged(System::Object^ sender, System::EventArgs^ e);
        private: System::Void numericUpDown2_ValueChanged(System::Object^ sender, System::EventArgs^ e);

        private: System::Void dataGridView_A_CellPainting             (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_B_src_CellPainting         (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_B_run_CellPainting         (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_C_CellPainting             (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);

        private: System::Void dataGridView_D_CellPainting             (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_counter_CellPainting       (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_state_CellPainting         (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);

        private: System::Void dataGridView_mem_states_CellPainting    (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_unitary_code_CellPainting  (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_X_vector_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_mem_conditions_CellPainting(System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_Y_vector_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_D_vector_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_X_common_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);
        private: System::Void dataGridView_X_save_CellPainting        (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);

        private: void CellPainting(System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e);

        private: void get_top_hf_circle_points(array<Point>^ & points, int radius, int center_X, int center_Y);
        private: double get_distance(int d1, int d2);

        private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
        private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
    };
}

