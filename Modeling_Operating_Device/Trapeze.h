#include "stdafx.h"

namespace Modeling_Operating_Device
{
    public ref class Trapeze : Visual_Element
    {
        public:
            Trapeze(Trapeze^ previous);
            Trapeze(Graphics^ _graphics, Pen^ _pen, System::Windows::Forms::Label^ _label, Point _size, bool right, String^ text, int X_center, int Y_center);
    };
}
