// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
#pragma once

// TODO. ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
#include <cmath>
// #include <cstring>

static const int PLATE = 0,
                 RHOMB = 1,
                 RECT  = 2,
                 STATE = 3;

#include "MP_Snapshot.h"
#include "Chip_Snapshot.h"
#include "Visual_Element.h"
#include "Plate.h"
#include "Rhomb.h"
#include "Rect.h"
#include "Line.h"
#include "Trapeze.h"

#include "Form1.h"

#include "Micro_Programm.h"
#include "Interaction_CA_AM.h"
