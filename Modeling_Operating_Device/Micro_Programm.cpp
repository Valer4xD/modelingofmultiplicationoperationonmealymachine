#include "stdafx.h"

namespace Modeling_Operating_Device
{
    void Micro_Programm::init(MP_Snapshot snapshot)
    {
        dat = snapshot;
    }

    void Micro_Programm::sub(byte %left, byte right)
    {
        byte carry = 0;
        while(right) // EDIT
        {
            carry = ~left & right;
            left ^= right;
            right = carry << 1;
        }
    }

    void Micro_Programm::y0()   {   dat.C = 0;                                                                                                      }
    void Micro_Programm::y1()   {   dat.D = _is_set_bit(dat.B, 15) ? 1 : 0;                                                                         }
    void Micro_Programm::y2()   {   _clear_bit(dat.B, 15);                                                                                          }
    void Micro_Programm::y3()   {   dat.counter = 0;                                                                                                }
    void Micro_Programm::y4()   {   _set_range(dat.C, 31, 15, _get_range(dat.C, 31, 15) +   _get_range(dat.A, 14, 0));                              }
    void Micro_Programm::y5()   {   _set_range(dat.C, 31, 15, _get_range(dat.C, 31, 15) +  (_get_range(dat.A, 14, 0) << 1));                        }
    void Micro_Programm::y6()   {   _set_range(dat.C, 31, 15, _get_range(dat.C, 31, 15) + (~_get_range(dat.A, 14, 0) | (1<<16) | (1<<15)) + 1);     }
    void Micro_Programm::y7()   {   _set_range(dat.B, 15,  0, _get_range(dat.B,15,0) +  1);                                                         }
    void Micro_Programm::y8()   {   _set_range(dat.B, 15,  0, _get_range(dat.B,15,0) >> 2);                                                         }
    void Micro_Programm::y9()   {   dat.C = _is_set_bit(dat.C, 31) | _is_set_bit(dat.C, 31) >> 1 | dat.C >> 2;                                      }
    void Micro_Programm::y10()  {
        if(dat.counter == 0)        dat.counter = 7;
        else                    sub(dat.counter, 1); } // ������� -= 1.
    void Micro_Programm::y11()  {   _set_range(dat.C, 30, 16, _get_range(dat.C, 30, 16) + 1);                                                       }
    void Micro_Programm::y12()  {   _set_bit(dat.C, 31);                                                                                            }

    bool Micro_Programm::x0()   {   save(RHOMB, 0);     return  true;                                               }
    bool Micro_Programm::x1()   {   save(RHOMB, 1);     return  !_get_range(dat.A, 14, 0);                          }
    bool Micro_Programm::x2()   {   save(RHOMB, 2);     return  !_get_range(dat.B, 14, 0);                          }
    bool Micro_Programm::x3()   {   save(RHOMB, 3);     return  !_is_set_bit(dat.B, 1) && !_is_set_bit(dat.B, 0);   }
    bool Micro_Programm::x4()   {   save(RHOMB, 4);     return  !_is_set_bit(dat.B, 1) &&  _is_set_bit(dat.B, 0);   }
    bool Micro_Programm::x5()   {   save(RHOMB, 5);     return   _is_set_bit(dat.B, 1) && !_is_set_bit(dat.B, 0);   }
    bool Micro_Programm::x6()   {   save(RHOMB, 6);     return  (dat.counter == 1)                  ? true: false;  }
    bool Micro_Programm::x7()   {   save(RHOMB, 7);     return  !dat.counter;                                       }
    bool Micro_Programm::x8()   {   save(RHOMB, 8);     return   _is_set_bit(dat.C, 15)             ? true: false;  }
    bool Micro_Programm::x9()   {   save(RHOMB, 9);     return  (_get_range(dat.A, 15, 15) ^ dat.D) ? true: false;  }

    byte Micro_Programm::tact()
    {
        if(!dat.state)
        {
            save(PLATE, 0);
            save(STATE, 0);
        }

        switch(dat.state)
        {
            case 0:
                if(x0())
                    if(x1())            { y0();                     save(RECT, 1);  dat.state = 5; }
                    else
                        if(x2())        { y0();                     save(RECT, 1);  dat.state = 5; }
                        else            { y0(); y1(); y2(); y3();   save(RECT, 0);  dat.state = 1; }
                else                    {                                           dat.state = 0; }
                break;
            case 1:
                if(x3())
                    if(x6())            { y10();                    save(RECT, 6);  dat.state = 3; }
                    else                { y8(); y9(); y10();        save(RECT, 5);  dat.state = 3; }
                else
                    if(x4())            { y4();                     save(RECT, 2);  dat.state = 2; }
                    else
                        if(x5())        { y5();                     save(RECT, 3);  dat.state = 2; }
                        else            { y6(); y7();               save(RECT, 4);  dat.state = 2; }
                break;
            case 2:
                if(x6())                { y10();                    save(RECT, 6);  dat.state = 3; }
                else                    { y8(); y9(); y10();        save(RECT, 5);  dat.state = 3; }
                break;
            case 3:
                if(x7())
                    if(x8())            { y11();                    save(RECT, 7);  dat.state = 4; }
                    else
                        if(x9())        { y12();                    save(RECT, 8);  dat.state = 5; }
                        else            {                                           dat.state = 6; }
                else
                    if(x3())
                        if(x6())        { y10();                    save(RECT, 6);  dat.state = 3; }
                        else            { y8(); y9(); y10();        save(RECT, 5);  dat.state = 3; }
                    else
                        if(x4())        { y4();                     save(RECT, 2);  dat.state = 2; }
                        else
                            if(x5())    { y5();                     save(RECT, 3);  dat.state = 2; }
                            else        { y6(); y7();               save(RECT, 4);  dat.state = 2; }
                break;
            case 4:
                if(x9())                { y12();                    save(RECT, 8);  dat.state = 5; }
                else                    {                                           dat.state = 6; }
            case 5:                     {                           save(RECT, 9);  dat.state = 6; }
        }

        save(STATE, dat.state);

        if(dat.state == 6)      save(PLATE, 1);

        return dat.state;
    }

    void Micro_Programm::save(int type, int id)
    {
        dat.type = type;
        dat.id   = id;
        snapshots.Add(dat);
    }
}
