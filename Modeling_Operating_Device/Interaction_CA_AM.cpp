#include "stdafx.h"

namespace Modeling_Operating_Device
{
    void Interaction_CA_AM::init(MP_Snapshot snapshot)
    {
        micro_program.init(snapshot);

        // ��������� ���������.

        _clear_bit(dat.D_vector, 0);
        _clear_bit(dat.D_vector, 1);
        _clear_bit(dat.D_vector, 2);

        micro_program.x0()  ?   _set_bit(dat.X_vector, 0)   :   _clear_bit(dat.X_vector, 0);
        micro_program.x1()  ?   _set_bit(dat.X_vector, 1)   :   _clear_bit(dat.X_vector, 1);
        micro_program.x2()  ?   _set_bit(dat.X_vector, 2)   :   _clear_bit(dat.X_vector, 2);

        micro_program.x3()  ?   _set_bit(dat.X_vector, 3)   :   _clear_bit(dat.X_vector, 3);
        micro_program.x4()  ?   _set_bit(dat.X_vector, 4)   :   _clear_bit(dat.X_vector, 4);
        micro_program.x5()  ?   _set_bit(dat.X_vector, 5)   :   _clear_bit(dat.X_vector, 5);
        micro_program.x6()  ?   _set_bit(dat.X_vector, 6)   :   _clear_bit(dat.X_vector, 6);
        micro_program.x7()  ?   _set_bit(dat.X_vector, 7)   :   _clear_bit(dat.X_vector, 7);

        micro_program.x8()  ?   _set_bit(dat.X_vector, 8)   :   _clear_bit(dat.X_vector, 8);
        micro_program.x9()  ?   _set_bit(dat.X_vector, 9)   :   _clear_bit(dat.X_vector, 9);
    }

    // �� - ���������� � ������ ���������.
    void Interaction_CA_AM::save_memory_state(const byte D_vector)
    {
        _is_set_bit(D_vector, 0)    ?   _set_bit(dat.mem_states, 0)     :   _clear_bit(dat.mem_states, 0);
        _is_set_bit(D_vector, 1)    ?   _set_bit(dat.mem_states, 1)     :   _clear_bit(dat.mem_states, 1);
        _is_set_bit(D_vector, 2)    ?   _set_bit(dat.mem_states, 2)     :   _clear_bit(dat.mem_states, 2);
    }

    // ��� - ���������� � ������ ���������� �������.
    void Interaction_CA_AM::save_memory_logical_conditions(const byte X_vector)
    {
        _is_set_bit(X_vector, 0) ? _set_bit(dat.mem_conditions, 0) : _clear_bit(dat.mem_conditions, 0);
        _is_set_bit(X_vector, 1) ? _set_bit(dat.mem_conditions, 1) : _clear_bit(dat.mem_conditions, 1);
        _is_set_bit(X_vector, 2) ? _set_bit(dat.mem_conditions, 2) : _clear_bit(dat.mem_conditions, 2);
        _is_set_bit(X_vector, 3) ? _set_bit(dat.mem_conditions, 3) : _clear_bit(dat.mem_conditions, 3);
        _is_set_bit(X_vector, 4) ? _set_bit(dat.mem_conditions, 4) : _clear_bit(dat.mem_conditions, 4);
    }

    // DC - ����������.
    void Interaction_CA_AM::decoder(const byte mem_states)
    {
        (  !_is_set_bit(mem_states, 0)
        && !_is_set_bit(mem_states, 1)
        && !_is_set_bit(mem_states, 2))     ?   _set_bit(dat.unitary_code, 0)   :   _clear_bit(dat.unitary_code, 0);

        (   _is_set_bit(mem_states, 0)
        && !_is_set_bit(mem_states, 1)
        && !_is_set_bit(mem_states, 2))     ?   _set_bit(dat.unitary_code, 1)   :   _clear_bit(dat.unitary_code, 1);

        (  !_is_set_bit(mem_states, 0)
        &&  _is_set_bit(mem_states, 1)
        && !_is_set_bit(mem_states, 2))     ?   _set_bit(dat.unitary_code, 2)   :   _clear_bit(dat.unitary_code, 2);

        (  !_is_set_bit(mem_states, 0)
        && !_is_set_bit(mem_states, 1)
        &&  _is_set_bit(mem_states, 2))     ?   _set_bit(dat.unitary_code, 3)   :   _clear_bit(dat.unitary_code, 3);

        (   _is_set_bit(mem_states, 0)
        &&  _is_set_bit(mem_states, 1)
        && !_is_set_bit(mem_states, 2))     ?   _set_bit(dat.unitary_code, 4)   :   _clear_bit(dat.unitary_code, 4);
    }

    // �� Y - �������������� ���������� ����� Y.
    void Interaction_CA_AM::combination_logic_circuit_Y(const byte unitary_code, const unsigned short X_vector, const byte mem_conditions)
    {
        bool cache  = _is_set_bit(unitary_code, 1) || _is_set_bit(unitary_code, 3) && !_is_set_bit(mem_conditions, 7-3);

        // a0
            _is_set_bit(unitary_code,   0)      ?   _set_bit(dat.Y_vector,  0)  :   _clear_bit(dat.Y_vector,  0);

        // a0x0�x1�x2
        (   _is_set_bit(unitary_code,   0)
        &&  _is_set_bit(X_vector,       0)
        && !_is_set_bit(X_vector,       1)
        && !_is_set_bit(X_vector,       2))     ?  (_set_bit(dat.Y_vector,  1),
                                                    _set_bit(dat.Y_vector,  2),
                                                    _set_bit(dat.Y_vector,  3)) :  (_clear_bit(dat.Y_vector,  1),
                                                                                    _clear_bit(dat.Y_vector,  2),
                                                                                    _clear_bit(dat.Y_vector,  3));

        // �x3x4(a1 v a3�x7)
        (  !_is_set_bit(mem_conditions, 3-3)
        &&  _is_set_bit(mem_conditions, 4-3)
        &&   cache)                             ?   _set_bit(dat.Y_vector,  4)  :   _clear_bit(dat.Y_vector,  4);

        // �x3�x4x5(a1 v a3�x7)
        (  !_is_set_bit(mem_conditions, 3-3)
        && !_is_set_bit(mem_conditions, 4-3)
        &&  _is_set_bit(mem_conditions, 5-3)
        &&   cache)                             ?   _set_bit(dat.Y_vector,  5)  :   _clear_bit(dat.Y_vector,  5);

        // �x3�x4�x5(a1 v a3�x7)
        (  !_is_set_bit(mem_conditions, 3-3)
        && !_is_set_bit(mem_conditions, 4-3)
        && !_is_set_bit(mem_conditions, 5-3)
        &&   cache)                             ?  (_set_bit(dat.Y_vector,  6),
                                                    _set_bit(dat.Y_vector,  7)) :  (_clear_bit(dat.Y_vector,  6),
                                                                                    _clear_bit(dat.Y_vector,  7));

        // �x6(x3(a1 v a3�x7) v a2)
        (  !_is_set_bit(mem_conditions, 6-3)
        && (_is_set_bit(mem_conditions, 3-3)
        &&   cache
        ||  _is_set_bit(unitary_code,   2)))    ?  (_set_bit(dat.Y_vector,  8),
                                                    _set_bit(dat.Y_vector,  9)) :  (_clear_bit(dat.Y_vector,  8),
                                                                                    _clear_bit(dat.Y_vector,  9));

        // x3(a1 v a3�x7) v a2
        (   _is_set_bit(mem_conditions, 3-3)
        &&   cache
        ||  _is_set_bit(unitary_code,   2))     ?   _set_bit(dat.Y_vector, 10)  :   _clear_bit(dat.Y_vector, 10);

        // a3x7x8
        (   _is_set_bit(unitary_code,   3)
        &&  _is_set_bit(mem_conditions, 7-3)
        &&  _is_set_bit(X_vector,       8))     ?   _set_bit(dat.Y_vector, 11)  :   _clear_bit(dat.Y_vector, 11);

        // x9(a3x7�x8 v a4)
        (   _is_set_bit(X_vector,       9)
        && (_is_set_bit(unitary_code,   3)
        &&  _is_set_bit(mem_conditions, 7-3)
        && !_is_set_bit(X_vector,       8)
        ||  _is_set_bit(unitary_code,   4)))    ?   _set_bit(dat.Y_vector, 12)  :   _clear_bit(dat.Y_vector, 12);
    }

    // �� - ������������ �������.
    void Interaction_CA_AM::operating_automat(const unsigned short Y_vector)
    {
        if(_is_set_bit(Y_vector,  0))       micro_program.y0 ();
        if(_is_set_bit(Y_vector,  1))       micro_program.y1 ();
        if(_is_set_bit(Y_vector,  2))       micro_program.y2 ();
        if(_is_set_bit(Y_vector,  3))       micro_program.y3 ();
        if(_is_set_bit(Y_vector,  4))       micro_program.y4 ();
        if(_is_set_bit(Y_vector,  5))       micro_program.y5 ();
        if(_is_set_bit(Y_vector,  6))       micro_program.y6 ();
        if(_is_set_bit(Y_vector,  7))       micro_program.y7 ();
        if(_is_set_bit(Y_vector,  8))       micro_program.y8 ();
        if(_is_set_bit(Y_vector,  9))       micro_program.y9 ();
        if(_is_set_bit(Y_vector, 10))       micro_program.y10();
        if(_is_set_bit(Y_vector, 11))       micro_program.y11();
        if(_is_set_bit(Y_vector, 12))       micro_program.y12();

        micro_program.x0()  ?   _set_bit(dat.X_vector, 0)   :   _clear_bit(dat.X_vector, 0);
        micro_program.x1()  ?   _set_bit(dat.X_vector, 1)   :   _clear_bit(dat.X_vector, 1);
        micro_program.x2()  ?   _set_bit(dat.X_vector, 2)   :   _clear_bit(dat.X_vector, 2);
        micro_program.x3()  ?   _set_bit(dat.X_vector, 3)   :   _clear_bit(dat.X_vector, 3);
        micro_program.x4()  ?   _set_bit(dat.X_vector, 4)   :   _clear_bit(dat.X_vector, 4);
        micro_program.x5()  ?   _set_bit(dat.X_vector, 5)   :   _clear_bit(dat.X_vector, 5);
        micro_program.x6()  ?   _set_bit(dat.X_vector, 6)   :   _clear_bit(dat.X_vector, 6);
        micro_program.x7()  ?   _set_bit(dat.X_vector, 7)   :   _clear_bit(dat.X_vector, 7);
        micro_program.x8()  ?   _set_bit(dat.X_vector, 8)   :   _clear_bit(dat.X_vector, 8);
        micro_program.x9()  ?   _set_bit(dat.X_vector, 9)   :   _clear_bit(dat.X_vector, 9);
    }

    // �� D - �������������� ���������� ����� D.
    void Interaction_CA_AM::combination_logic_circuit_D(const byte unitary_code, const unsigned short X_vector, const byte mem_conditions)
    {
        // a0�x1�x2 v a3x7x8
        (   _is_set_bit(unitary_code,   0)
        && !_is_set_bit(X_vector,       1)
        && !_is_set_bit(X_vector,       2)
        ||  _is_set_bit(unitary_code,   3)
        &&  _is_set_bit(mem_conditions, 7-3)
        &&  _is_set_bit(mem_conditions, 8-3))   ?   _set_bit(dat.D_vector, 0)   :   _clear_bit(dat.D_vector, 0);

        // �x3(a1 v a3�x7) v a3x7x8
        (  !_is_set_bit(mem_conditions, 3-3)
        && (_is_set_bit(unitary_code,   1)
        ||  _is_set_bit(unitary_code,   3)
        && !_is_set_bit(mem_conditions, 7-3))
        ||  _is_set_bit(unitary_code,   3)
        &&  _is_set_bit(mem_conditions, 7-3)
        &&  _is_set_bit(mem_conditions, 8-3))   ?   _set_bit(dat.D_vector, 1)   :   _clear_bit(dat.D_vector, 1);

        // x3(a1 v a3�x7) v a2
        (   _is_set_bit(mem_conditions, 3-3)
        && (_is_set_bit(unitary_code,   1)
        ||  _is_set_bit(unitary_code,   3)
        && !_is_set_bit(mem_conditions, 7-3))
        ||  _is_set_bit(unitary_code,   2))     ?   _set_bit(dat.D_vector, 2)   :   _clear_bit(dat.D_vector, 2);
    }

    byte Interaction_CA_AM::tact()
    {
        save_memory_state(dat.D_vector);
        save_memory_logical_conditions(_get_range(dat.X_vector, 7, 3));
        decoder(dat.mem_states);
        combination_logic_circuit_Y(dat.unitary_code, dat.X_vector, dat.mem_conditions);
        operating_automat(dat.Y_vector);
        combination_logic_circuit_D(dat.unitary_code, dat.X_vector, dat.mem_conditions);
        set_micro_program_state();
        return micro_program.dat.state;
    }

    void Interaction_CA_AM::set_micro_program_state()
    {
        switch(dat.D_vector & 7)
        {
            case 0: { micro_program.dat.state = 0; break; }
            case 1: { micro_program.dat.state = 1; break; }
            case 2: { micro_program.dat.state = 2; break; }
            case 3: { micro_program.dat.state = 4; break; }
            case 4: { micro_program.dat.state = 3; break; }
        }
    }
}
