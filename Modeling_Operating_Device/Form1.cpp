#include "stdafx.h"

namespace Modeling_Operating_Device
{
    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;
    using namespace System::Collections::Generic;

    /// <summary>
    /// ������ ��� Form1
    ///
    /// ��������! ��� ��������� ����� ����� ������ ���������� ����� ��������
    ///          �������� ����� ����� �������� ("Resource File Name") ��� �������� ���������� ������������ �������,
    ///          ���������� �� ����� ������� � ����������� .resx, �� ������� ������� ������ �����. � ��������� ������,
    ///          ������������ �� ������ ��������� �������� � ���������������
    ///          ���������, ��������������� ������ �����.
    /// </summary>
    Form1::Form1(void)
    {
        InitializeComponent();
        //
        //TODO: �������� ��� ������������
        //

        graphics = panel1->CreateGraphics();
        default_color = Color::White;
        // default_color = Color::Black;
        work_color    = Color::Red;

        time_step_counter = 0;
        chip_snapshots    = gcnew List<Chip_Snapshot>;

        set_dataGridView(dataGridView_A,     0);
        set_dataGridView(dataGridView_B_src, 0);

        calculate_MP  ();
        calculate_chip();

        reset_values();
    }

    /// <summary>
    /// ���������� ��� ������������ �������.
    /// </summary>
    Form1::~Form1()
    {
        if(components)
        {
            delete components;
        }
    }

#pragma region Windows Form Designer generated code
    /// <summary>
    /// ������������ ����� ��� ��������� ������������ - �� ���������
    /// ���������� ������� ������ ��� ������ ��������� ����.
    /// </summary>
    void Form1::InitializeComponent(void)
    {
        this->components = (gcnew System::ComponentModel::Container());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle121 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle122 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle123 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle124 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle125 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle126 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle127 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle128 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle129 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle130 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle131 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle132 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle133 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle134 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle135 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle136 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle137 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle138 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle139 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle140 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle141 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle142 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle143 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle144 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle145 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle146 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle147 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle148 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle149 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        System::Windows::Forms::DataGridViewCellStyle^ dataGridViewCellStyle150 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
        this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
        this->errorProvider1 = (gcnew System::Windows::Forms::ErrorProvider(this->components));
        this->panel1 = (gcnew System::Windows::Forms::Panel());
        this->button2 = (gcnew System::Windows::Forms::Button());
        this->button1 = (gcnew System::Windows::Forms::Button());
        this->label90 = (gcnew System::Windows::Forms::Label());
        this->label89 = (gcnew System::Windows::Forms::Label());
        this->dataGridView_C = (gcnew System::Windows::Forms::DataGridView());
        this->Column32 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column31 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column30 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column29 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column28 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column27 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column26 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column25 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column24 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column23 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column22 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column21 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column20 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column19 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column18 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column17 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn33 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn34 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn35 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn36 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn37 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn38 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn39 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn40 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn41 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn42 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn43 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn44 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn45 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn46 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn47 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn48 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
        this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
        this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
        this->button7 = (gcnew System::Windows::Forms::Button());
        this->button6 = (gcnew System::Windows::Forms::Button());
        this->dataGridView_X_save = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn99 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn100 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn101 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn105 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn106 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_X_common = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn95 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn96 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn102 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn103 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn104 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_D_vector = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn60 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn80 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn81 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_Y_vector = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn82 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn83 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn84 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn85 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn86 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn87 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn88 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn89 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn90 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn91 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn92 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn93 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn94 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_mem_conditions = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn55 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn56 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn57 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn58 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn59 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_X_vector = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn61 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn66 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn67 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn68 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn69 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn75 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn76 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn77 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn78 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn79 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_unitary_code = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn70 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn71 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn72 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn73 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn74 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_mem_states = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn52 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn53 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn54 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->label88 = (gcnew System::Windows::Forms::Label());
        this->label87 = (gcnew System::Windows::Forms::Label());
        this->dataGridView_state = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn49 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn50 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn51 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_counter = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn62 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn63 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn65 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_D = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn64 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_B_run = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn17 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn18 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn19 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn20 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn21 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn22 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn23 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn24 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn25 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn26 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn27 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn28 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn29 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn30 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn31 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn32 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridView_B_src = (gcnew System::Windows::Forms::DataGridView());
        this->dataGridViewTextBoxColumn1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn8 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn9 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn10 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn11 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn12 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn13 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn14 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn15 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->dataGridViewTextBoxColumn16 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->label86 = (gcnew System::Windows::Forms::Label());
        this->label85 = (gcnew System::Windows::Forms::Label());
        this->label84 = (gcnew System::Windows::Forms::Label());
        this->dataGridView_A = (gcnew System::Windows::Forms::DataGridView());
        this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column8 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column9 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column10 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column11 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column12 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column13 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column14 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column15 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->Column16 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
        this->label83 = (gcnew System::Windows::Forms::Label());
        this->label82 = (gcnew System::Windows::Forms::Label());
        this->label81 = (gcnew System::Windows::Forms::Label());
        this->label80 = (gcnew System::Windows::Forms::Label());
        this->label79 = (gcnew System::Windows::Forms::Label());
        this->label78 = (gcnew System::Windows::Forms::Label());
        this->label77 = (gcnew System::Windows::Forms::Label());
        this->label76 = (gcnew System::Windows::Forms::Label());
        this->label75 = (gcnew System::Windows::Forms::Label());
        this->label74 = (gcnew System::Windows::Forms::Label());
        this->label73 = (gcnew System::Windows::Forms::Label());
        this->label72 = (gcnew System::Windows::Forms::Label());
        this->label71 = (gcnew System::Windows::Forms::Label());
        this->label70 = (gcnew System::Windows::Forms::Label());
        this->label69 = (gcnew System::Windows::Forms::Label());
        this->label68 = (gcnew System::Windows::Forms::Label());
        this->label67 = (gcnew System::Windows::Forms::Label());
        this->label66 = (gcnew System::Windows::Forms::Label());
        this->label65 = (gcnew System::Windows::Forms::Label());
        this->label64 = (gcnew System::Windows::Forms::Label());
        this->label63 = (gcnew System::Windows::Forms::Label());
        this->label62 = (gcnew System::Windows::Forms::Label());
        this->label61 = (gcnew System::Windows::Forms::Label());
        this->label60 = (gcnew System::Windows::Forms::Label());
        this->label59 = (gcnew System::Windows::Forms::Label());
        this->label58 = (gcnew System::Windows::Forms::Label());
        this->label57 = (gcnew System::Windows::Forms::Label());
        this->label56 = (gcnew System::Windows::Forms::Label());
        this->label55 = (gcnew System::Windows::Forms::Label());
        this->label54 = (gcnew System::Windows::Forms::Label());
        this->label53 = (gcnew System::Windows::Forms::Label());
        this->label52 = (gcnew System::Windows::Forms::Label());
        this->label51 = (gcnew System::Windows::Forms::Label());
        this->label50 = (gcnew System::Windows::Forms::Label());
        this->label49 = (gcnew System::Windows::Forms::Label());
        this->label48 = (gcnew System::Windows::Forms::Label());
        this->label47 = (gcnew System::Windows::Forms::Label());
        this->label46 = (gcnew System::Windows::Forms::Label());
        this->label45 = (gcnew System::Windows::Forms::Label());
        this->label44 = (gcnew System::Windows::Forms::Label());
        this->label43 = (gcnew System::Windows::Forms::Label());
        this->label42 = (gcnew System::Windows::Forms::Label());
        this->label41 = (gcnew System::Windows::Forms::Label());
        this->label40 = (gcnew System::Windows::Forms::Label());
        this->label39 = (gcnew System::Windows::Forms::Label());
        this->label38 = (gcnew System::Windows::Forms::Label());
        this->label37 = (gcnew System::Windows::Forms::Label());
        this->label36 = (gcnew System::Windows::Forms::Label());
        this->label35 = (gcnew System::Windows::Forms::Label());
        this->label34 = (gcnew System::Windows::Forms::Label());
        this->label33 = (gcnew System::Windows::Forms::Label());
        this->label32 = (gcnew System::Windows::Forms::Label());
        this->label31 = (gcnew System::Windows::Forms::Label());
        this->button3 = (gcnew System::Windows::Forms::Button());
        this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
        this->label30 = (gcnew System::Windows::Forms::Label());
        this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
        this->label29 = (gcnew System::Windows::Forms::Label());
        this->label28 = (gcnew System::Windows::Forms::Label());
        this->label27 = (gcnew System::Windows::Forms::Label());
        this->label26 = (gcnew System::Windows::Forms::Label());
        this->label25 = (gcnew System::Windows::Forms::Label());
        this->label24 = (gcnew System::Windows::Forms::Label());
        this->label23 = (gcnew System::Windows::Forms::Label());
        this->label22 = (gcnew System::Windows::Forms::Label());
        this->label21 = (gcnew System::Windows::Forms::Label());
        this->label20 = (gcnew System::Windows::Forms::Label());
        this->label19 = (gcnew System::Windows::Forms::Label());
        this->label18 = (gcnew System::Windows::Forms::Label());
        this->label17 = (gcnew System::Windows::Forms::Label());
        this->label16 = (gcnew System::Windows::Forms::Label());
        this->label15 = (gcnew System::Windows::Forms::Label());
        this->label14 = (gcnew System::Windows::Forms::Label());
        this->label13 = (gcnew System::Windows::Forms::Label());
        this->label12 = (gcnew System::Windows::Forms::Label());
        this->label11 = (gcnew System::Windows::Forms::Label());
        this->label10 = (gcnew System::Windows::Forms::Label());
        this->label9 = (gcnew System::Windows::Forms::Label());
        this->label8 = (gcnew System::Windows::Forms::Label());
        this->label7 = (gcnew System::Windows::Forms::Label());
        this->label6 = (gcnew System::Windows::Forms::Label());
        this->label5 = (gcnew System::Windows::Forms::Label());
        this->label4 = (gcnew System::Windows::Forms::Label());
        this->label3 = (gcnew System::Windows::Forms::Label());
        this->label2 = (gcnew System::Windows::Forms::Label());
        this->label1 = (gcnew System::Windows::Forms::Label());
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->errorProvider1))->BeginInit();
        this->panel1->SuspendLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_C))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_X_save))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_X_common))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_D_vector))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_Y_vector))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_mem_conditions))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_X_vector))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_unitary_code))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_mem_states))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_state))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_counter))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_D))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_B_run))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_B_src))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_A))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
        this->SuspendLayout();
        // 
        // timer1
        // 
        this->timer1->Enabled = true;
        this->timer1->Interval = 50;
        this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
        // 
        // errorProvider1
        // 
        this->errorProvider1->ContainerControl = this;
        // 
        // panel1
        // 
        this->panel1->Controls->Add(this->button2);
        this->panel1->Controls->Add(this->button1);
        this->panel1->Controls->Add(this->label90);
        this->panel1->Controls->Add(this->label89);
        this->panel1->Controls->Add(this->dataGridView_C);
        this->panel1->Controls->Add(this->checkBox3);
        this->panel1->Controls->Add(this->checkBox2);
        this->panel1->Controls->Add(this->checkBox1);
        this->panel1->Controls->Add(this->button7);
        this->panel1->Controls->Add(this->button6);
        this->panel1->Controls->Add(this->dataGridView_X_save);
        this->panel1->Controls->Add(this->dataGridView_X_common);
        this->panel1->Controls->Add(this->dataGridView_D_vector);
        this->panel1->Controls->Add(this->dataGridView_Y_vector);
        this->panel1->Controls->Add(this->dataGridView_mem_conditions);
        this->panel1->Controls->Add(this->dataGridView_X_vector);
        this->panel1->Controls->Add(this->dataGridView_unitary_code);
        this->panel1->Controls->Add(this->dataGridView_mem_states);
        this->panel1->Controls->Add(this->label88);
        this->panel1->Controls->Add(this->label87);
        this->panel1->Controls->Add(this->dataGridView_state);
        this->panel1->Controls->Add(this->dataGridView_counter);
        this->panel1->Controls->Add(this->dataGridView_D);
        this->panel1->Controls->Add(this->dataGridView_B_run);
        this->panel1->Controls->Add(this->dataGridView_B_src);
        this->panel1->Controls->Add(this->label86);
        this->panel1->Controls->Add(this->label85);
        this->panel1->Controls->Add(this->label84);
        this->panel1->Controls->Add(this->dataGridView_A);
        this->panel1->Controls->Add(this->label83);
        this->panel1->Controls->Add(this->label82);
        this->panel1->Controls->Add(this->label81);
        this->panel1->Controls->Add(this->label80);
        this->panel1->Controls->Add(this->label79);
        this->panel1->Controls->Add(this->label78);
        this->panel1->Controls->Add(this->label77);
        this->panel1->Controls->Add(this->label76);
        this->panel1->Controls->Add(this->label75);
        this->panel1->Controls->Add(this->label74);
        this->panel1->Controls->Add(this->label73);
        this->panel1->Controls->Add(this->label72);
        this->panel1->Controls->Add(this->label71);
        this->panel1->Controls->Add(this->label70);
        this->panel1->Controls->Add(this->label69);
        this->panel1->Controls->Add(this->label68);
        this->panel1->Controls->Add(this->label67);
        this->panel1->Controls->Add(this->label66);
        this->panel1->Controls->Add(this->label65);
        this->panel1->Controls->Add(this->label64);
        this->panel1->Controls->Add(this->label63);
        this->panel1->Controls->Add(this->label62);
        this->panel1->Controls->Add(this->label61);
        this->panel1->Controls->Add(this->label60);
        this->panel1->Controls->Add(this->label59);
        this->panel1->Controls->Add(this->label58);
        this->panel1->Controls->Add(this->label57);
        this->panel1->Controls->Add(this->label56);
        this->panel1->Controls->Add(this->label55);
        this->panel1->Controls->Add(this->label54);
        this->panel1->Controls->Add(this->label53);
        this->panel1->Controls->Add(this->label52);
        this->panel1->Controls->Add(this->label51);
        this->panel1->Controls->Add(this->label50);
        this->panel1->Controls->Add(this->label49);
        this->panel1->Controls->Add(this->label48);
        this->panel1->Controls->Add(this->label47);
        this->panel1->Controls->Add(this->label46);
        this->panel1->Controls->Add(this->label45);
        this->panel1->Controls->Add(this->label44);
        this->panel1->Controls->Add(this->label43);
        this->panel1->Controls->Add(this->label42);
        this->panel1->Controls->Add(this->label41);
        this->panel1->Controls->Add(this->label40);
        this->panel1->Controls->Add(this->label39);
        this->panel1->Controls->Add(this->label38);
        this->panel1->Controls->Add(this->label37);
        this->panel1->Controls->Add(this->label36);
        this->panel1->Controls->Add(this->label35);
        this->panel1->Controls->Add(this->label34);
        this->panel1->Controls->Add(this->label33);
        this->panel1->Controls->Add(this->label32);
        this->panel1->Controls->Add(this->label31);
        this->panel1->Controls->Add(this->button3);
        this->panel1->Controls->Add(this->numericUpDown2);
        this->panel1->Controls->Add(this->label30);
        this->panel1->Controls->Add(this->numericUpDown1);
        this->panel1->Controls->Add(this->label29);
        this->panel1->Controls->Add(this->label28);
        this->panel1->Controls->Add(this->label27);
        this->panel1->Controls->Add(this->label26);
        this->panel1->Controls->Add(this->label25);
        this->panel1->Controls->Add(this->label24);
        this->panel1->Controls->Add(this->label23);
        this->panel1->Controls->Add(this->label22);
        this->panel1->Controls->Add(this->label21);
        this->panel1->Controls->Add(this->label20);
        this->panel1->Controls->Add(this->label19);
        this->panel1->Controls->Add(this->label18);
        this->panel1->Controls->Add(this->label17);
        this->panel1->Controls->Add(this->label16);
        this->panel1->Controls->Add(this->label15);
        this->panel1->Controls->Add(this->label14);
        this->panel1->Controls->Add(this->label13);
        this->panel1->Controls->Add(this->label12);
        this->panel1->Controls->Add(this->label11);
        this->panel1->Controls->Add(this->label10);
        this->panel1->Controls->Add(this->label9);
        this->panel1->Controls->Add(this->label8);
        this->panel1->Controls->Add(this->label7);
        this->panel1->Controls->Add(this->label6);
        this->panel1->Controls->Add(this->label5);
        this->panel1->Controls->Add(this->label4);
        this->panel1->Controls->Add(this->label3);
        this->panel1->Controls->Add(this->label2);
        this->panel1->Controls->Add(this->label1);
        this->panel1->Location = System::Drawing::Point(1, 1);
        this->panel1->Name = L"panel1";
        this->panel1->Size = System::Drawing::Size(1262, 958);
        this->panel1->TabIndex = 0;
        // 
        // button2
        // 
        this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->button2->Location = System::Drawing::Point(619, 75);
        this->button2->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
        this->button2->Name = L"button2";
        this->button2->Size = System::Drawing::Size(160, 23);
        this->button2->TabIndex = 256;
        this->button2->Text = L"��������";
        this->button2->UseVisualStyleBackColor = true;
        this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
        // 
        // button1
        // 
        this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->button1->Location = System::Drawing::Point(619, 33);
        this->button1->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
        this->button1->Name = L"button1";
        this->button1->Size = System::Drawing::Size(160, 23);
        this->button1->TabIndex = 255;
        this->button1->Text = L"��������";
        this->button1->UseVisualStyleBackColor = true;
        this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
        // 
        // label90
        // 
        this->label90->AutoSize = true;
        this->label90->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label90->Location = System::Drawing::Point(125, 73);
        this->label90->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label90->Name = L"label90";
        this->label90->Size = System::Drawing::Size(41, 12);
        this->label90->TabIndex = 254;
        this->label90->Text = L"label90";
        // 
        // label89
        // 
        this->label89->AutoSize = true;
        this->label89->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label89->Location = System::Drawing::Point(1, 269);
        this->label89->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label89->Name = L"label89";
        this->label89->Size = System::Drawing::Size(34, 12);
        this->label89->TabIndex = 253;
        this->label89->Text = L"label89";
        this->label89->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // dataGridView_C
        // 
        this->dataGridView_C->AllowUserToDeleteRows = false;
        this->dataGridView_C->AllowUserToResizeColumns = false;
        this->dataGridView_C->AllowUserToResizeRows = false;
        dataGridViewCellStyle121->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle121->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle121->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle121->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle121->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle121->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle121->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_C->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle121;
        this->dataGridView_C->ColumnHeadersHeight = 19;
        this->dataGridView_C->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_C->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(32) {
            this->Column32,
                this->Column31, this->Column30, this->Column29, this->Column28, this->Column27, this->Column26, this->Column25, this->Column24,
                this->Column23, this->Column22, this->Column21, this->Column20, this->Column19, this->Column18, this->Column17, this->dataGridViewTextBoxColumn33,
                this->dataGridViewTextBoxColumn34, this->dataGridViewTextBoxColumn35, this->dataGridViewTextBoxColumn36, this->dataGridViewTextBoxColumn37,
                this->dataGridViewTextBoxColumn38, this->dataGridViewTextBoxColumn39, this->dataGridViewTextBoxColumn40, this->dataGridViewTextBoxColumn41,
                this->dataGridViewTextBoxColumn42, this->dataGridViewTextBoxColumn43, this->dataGridViewTextBoxColumn44, this->dataGridViewTextBoxColumn45,
                this->dataGridViewTextBoxColumn46, this->dataGridViewTextBoxColumn47, this->dataGridViewTextBoxColumn48
        });
        dataGridViewCellStyle122->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle122->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle122->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle122->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle122->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle122->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle122->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_C->DefaultCellStyle = dataGridViewCellStyle122;
        this->dataGridView_C->Location = System::Drawing::Point(464, 159);
        this->dataGridView_C->Name = L"dataGridView_C";
        this->dataGridView_C->ReadOnly = true;
        this->dataGridView_C->RowHeadersVisible = false;
        this->dataGridView_C->Size = System::Drawing::Size(641, 43);
        this->dataGridView_C->TabIndex = 118;
        this->dataGridView_C->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_C_CellPainting);
        // 
        // Column32
        // 
        this->Column32->Frozen = true;
        this->Column32->HeaderText = L"31";
        this->Column32->Name = L"Column32";
        this->Column32->ReadOnly = true;
        this->Column32->Width = 20;
        // 
        // Column31
        // 
        this->Column31->Frozen = true;
        this->Column31->HeaderText = L"30";
        this->Column31->Name = L"Column31";
        this->Column31->ReadOnly = true;
        this->Column31->Width = 20;
        // 
        // Column30
        // 
        this->Column30->Frozen = true;
        this->Column30->HeaderText = L"29";
        this->Column30->Name = L"Column30";
        this->Column30->ReadOnly = true;
        this->Column30->Width = 20;
        // 
        // Column29
        // 
        this->Column29->Frozen = true;
        this->Column29->HeaderText = L"28";
        this->Column29->Name = L"Column29";
        this->Column29->ReadOnly = true;
        this->Column29->Width = 20;
        // 
        // Column28
        // 
        this->Column28->Frozen = true;
        this->Column28->HeaderText = L"27";
        this->Column28->Name = L"Column28";
        this->Column28->ReadOnly = true;
        this->Column28->Width = 20;
        // 
        // Column27
        // 
        this->Column27->Frozen = true;
        this->Column27->HeaderText = L"26";
        this->Column27->Name = L"Column27";
        this->Column27->ReadOnly = true;
        this->Column27->Width = 20;
        // 
        // Column26
        // 
        this->Column26->Frozen = true;
        this->Column26->HeaderText = L"25";
        this->Column26->Name = L"Column26";
        this->Column26->ReadOnly = true;
        this->Column26->Width = 20;
        // 
        // Column25
        // 
        this->Column25->Frozen = true;
        this->Column25->HeaderText = L"24";
        this->Column25->Name = L"Column25";
        this->Column25->ReadOnly = true;
        this->Column25->Width = 20;
        // 
        // Column24
        // 
        this->Column24->Frozen = true;
        this->Column24->HeaderText = L"23";
        this->Column24->Name = L"Column24";
        this->Column24->ReadOnly = true;
        this->Column24->Width = 20;
        // 
        // Column23
        // 
        this->Column23->Frozen = true;
        this->Column23->HeaderText = L"22";
        this->Column23->Name = L"Column23";
        this->Column23->ReadOnly = true;
        this->Column23->Width = 20;
        // 
        // Column22
        // 
        this->Column22->Frozen = true;
        this->Column22->HeaderText = L"21";
        this->Column22->Name = L"Column22";
        this->Column22->ReadOnly = true;
        this->Column22->Width = 20;
        // 
        // Column21
        // 
        this->Column21->Frozen = true;
        this->Column21->HeaderText = L"20";
        this->Column21->Name = L"Column21";
        this->Column21->ReadOnly = true;
        this->Column21->Width = 20;
        // 
        // Column20
        // 
        this->Column20->Frozen = true;
        this->Column20->HeaderText = L"19";
        this->Column20->Name = L"Column20";
        this->Column20->ReadOnly = true;
        this->Column20->Width = 20;
        // 
        // Column19
        // 
        this->Column19->Frozen = true;
        this->Column19->HeaderText = L"18";
        this->Column19->Name = L"Column19";
        this->Column19->ReadOnly = true;
        this->Column19->Width = 20;
        // 
        // Column18
        // 
        this->Column18->Frozen = true;
        this->Column18->HeaderText = L"17";
        this->Column18->Name = L"Column18";
        this->Column18->ReadOnly = true;
        this->Column18->Width = 20;
        // 
        // Column17
        // 
        this->Column17->Frozen = true;
        this->Column17->HeaderText = L"16";
        this->Column17->Name = L"Column17";
        this->Column17->ReadOnly = true;
        this->Column17->Width = 20;
        // 
        // dataGridViewTextBoxColumn33
        // 
        this->dataGridViewTextBoxColumn33->Frozen = true;
        this->dataGridViewTextBoxColumn33->HeaderText = L"15";
        this->dataGridViewTextBoxColumn33->Name = L"dataGridViewTextBoxColumn33";
        this->dataGridViewTextBoxColumn33->ReadOnly = true;
        this->dataGridViewTextBoxColumn33->Width = 20;
        // 
        // dataGridViewTextBoxColumn34
        // 
        this->dataGridViewTextBoxColumn34->Frozen = true;
        this->dataGridViewTextBoxColumn34->HeaderText = L"14";
        this->dataGridViewTextBoxColumn34->Name = L"dataGridViewTextBoxColumn34";
        this->dataGridViewTextBoxColumn34->ReadOnly = true;
        this->dataGridViewTextBoxColumn34->Width = 20;
        // 
        // dataGridViewTextBoxColumn35
        // 
        this->dataGridViewTextBoxColumn35->Frozen = true;
        this->dataGridViewTextBoxColumn35->HeaderText = L"13";
        this->dataGridViewTextBoxColumn35->Name = L"dataGridViewTextBoxColumn35";
        this->dataGridViewTextBoxColumn35->ReadOnly = true;
        this->dataGridViewTextBoxColumn35->Width = 20;
        // 
        // dataGridViewTextBoxColumn36
        // 
        this->dataGridViewTextBoxColumn36->Frozen = true;
        this->dataGridViewTextBoxColumn36->HeaderText = L"12";
        this->dataGridViewTextBoxColumn36->Name = L"dataGridViewTextBoxColumn36";
        this->dataGridViewTextBoxColumn36->ReadOnly = true;
        this->dataGridViewTextBoxColumn36->Width = 20;
        // 
        // dataGridViewTextBoxColumn37
        // 
        this->dataGridViewTextBoxColumn37->Frozen = true;
        this->dataGridViewTextBoxColumn37->HeaderText = L"11";
        this->dataGridViewTextBoxColumn37->Name = L"dataGridViewTextBoxColumn37";
        this->dataGridViewTextBoxColumn37->ReadOnly = true;
        this->dataGridViewTextBoxColumn37->Width = 20;
        // 
        // dataGridViewTextBoxColumn38
        // 
        this->dataGridViewTextBoxColumn38->Frozen = true;
        this->dataGridViewTextBoxColumn38->HeaderText = L"10";
        this->dataGridViewTextBoxColumn38->Name = L"dataGridViewTextBoxColumn38";
        this->dataGridViewTextBoxColumn38->ReadOnly = true;
        this->dataGridViewTextBoxColumn38->Width = 20;
        // 
        // dataGridViewTextBoxColumn39
        // 
        this->dataGridViewTextBoxColumn39->Frozen = true;
        this->dataGridViewTextBoxColumn39->HeaderText = L"9";
        this->dataGridViewTextBoxColumn39->Name = L"dataGridViewTextBoxColumn39";
        this->dataGridViewTextBoxColumn39->ReadOnly = true;
        this->dataGridViewTextBoxColumn39->Width = 20;
        // 
        // dataGridViewTextBoxColumn40
        // 
        this->dataGridViewTextBoxColumn40->Frozen = true;
        this->dataGridViewTextBoxColumn40->HeaderText = L"8";
        this->dataGridViewTextBoxColumn40->Name = L"dataGridViewTextBoxColumn40";
        this->dataGridViewTextBoxColumn40->ReadOnly = true;
        this->dataGridViewTextBoxColumn40->Width = 20;
        // 
        // dataGridViewTextBoxColumn41
        // 
        this->dataGridViewTextBoxColumn41->Frozen = true;
        this->dataGridViewTextBoxColumn41->HeaderText = L"7";
        this->dataGridViewTextBoxColumn41->Name = L"dataGridViewTextBoxColumn41";
        this->dataGridViewTextBoxColumn41->ReadOnly = true;
        this->dataGridViewTextBoxColumn41->Width = 20;
        // 
        // dataGridViewTextBoxColumn42
        // 
        this->dataGridViewTextBoxColumn42->Frozen = true;
        this->dataGridViewTextBoxColumn42->HeaderText = L"6";
        this->dataGridViewTextBoxColumn42->Name = L"dataGridViewTextBoxColumn42";
        this->dataGridViewTextBoxColumn42->ReadOnly = true;
        this->dataGridViewTextBoxColumn42->Width = 20;
        // 
        // dataGridViewTextBoxColumn43
        // 
        this->dataGridViewTextBoxColumn43->Frozen = true;
        this->dataGridViewTextBoxColumn43->HeaderText = L"5";
        this->dataGridViewTextBoxColumn43->Name = L"dataGridViewTextBoxColumn43";
        this->dataGridViewTextBoxColumn43->ReadOnly = true;
        this->dataGridViewTextBoxColumn43->Width = 20;
        // 
        // dataGridViewTextBoxColumn44
        // 
        this->dataGridViewTextBoxColumn44->Frozen = true;
        this->dataGridViewTextBoxColumn44->HeaderText = L"4";
        this->dataGridViewTextBoxColumn44->Name = L"dataGridViewTextBoxColumn44";
        this->dataGridViewTextBoxColumn44->ReadOnly = true;
        this->dataGridViewTextBoxColumn44->Width = 20;
        // 
        // dataGridViewTextBoxColumn45
        // 
        this->dataGridViewTextBoxColumn45->Frozen = true;
        this->dataGridViewTextBoxColumn45->HeaderText = L"3";
        this->dataGridViewTextBoxColumn45->Name = L"dataGridViewTextBoxColumn45";
        this->dataGridViewTextBoxColumn45->ReadOnly = true;
        this->dataGridViewTextBoxColumn45->Width = 20;
        // 
        // dataGridViewTextBoxColumn46
        // 
        this->dataGridViewTextBoxColumn46->Frozen = true;
        this->dataGridViewTextBoxColumn46->HeaderText = L"2";
        this->dataGridViewTextBoxColumn46->Name = L"dataGridViewTextBoxColumn46";
        this->dataGridViewTextBoxColumn46->ReadOnly = true;
        this->dataGridViewTextBoxColumn46->Width = 20;
        // 
        // dataGridViewTextBoxColumn47
        // 
        this->dataGridViewTextBoxColumn47->Frozen = true;
        this->dataGridViewTextBoxColumn47->HeaderText = L"1";
        this->dataGridViewTextBoxColumn47->Name = L"dataGridViewTextBoxColumn47";
        this->dataGridViewTextBoxColumn47->ReadOnly = true;
        this->dataGridViewTextBoxColumn47->Width = 20;
        // 
        // dataGridViewTextBoxColumn48
        // 
        this->dataGridViewTextBoxColumn48->Frozen = true;
        this->dataGridViewTextBoxColumn48->HeaderText = L"0";
        this->dataGridViewTextBoxColumn48->Name = L"dataGridViewTextBoxColumn48";
        this->dataGridViewTextBoxColumn48->ReadOnly = true;
        this->dataGridViewTextBoxColumn48->Width = 20;
        // 
        // checkBox3
        // 
        this->checkBox3->AutoSize = true;
        this->checkBox3->ForeColor = System::Drawing::SystemColors::Window;
        this->checkBox3->Location = System::Drawing::Point(857, 295);
        this->checkBox3->Name = L"checkBox3";
        this->checkBox3->Size = System::Drawing::Size(126, 17);
        this->checkBox3->TabIndex = 252;
        this->checkBox3->Text = L"����-������������";
        this->checkBox3->UseVisualStyleBackColor = true;
        this->checkBox3->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox3_CheckedChanged);
        // 
        // checkBox2
        // 
        this->checkBox2->AutoSize = true;
        this->checkBox2->ForeColor = System::Drawing::SystemColors::Window;
        this->checkBox2->Location = System::Drawing::Point(857, 276);
        this->checkBox2->Name = L"checkBox2";
        this->checkBox2->Size = System::Drawing::Size(170, 17);
        this->checkBox2->TabIndex = 251;
        this->checkBox2->Text = L"�� ���������� / �� ������";
        this->checkBox2->UseVisualStyleBackColor = true;
        this->checkBox2->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox2_CheckedChanged);
        // 
        // checkBox1
        // 
        this->checkBox1->AutoSize = true;
        this->checkBox1->Checked = true;
        this->checkBox1->CheckState = System::Windows::Forms::CheckState::Checked;
        this->checkBox1->ForeColor = System::Drawing::SystemColors::Window;
        this->checkBox1->Location = System::Drawing::Point(857, 257);
        this->checkBox1->Name = L"checkBox1";
        this->checkBox1->Size = System::Drawing::Size(200, 17);
        this->checkBox1->TabIndex = 250;
        this->checkBox1->Text = L"��� - �������� / ��������������";
        this->checkBox1->UseVisualStyleBackColor = true;
        this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &Form1::checkBox1_CheckedChanged);
        // 
        // button7
        // 
        this->button7->Location = System::Drawing::Point(587, 286);
        this->button7->Name = L"button7";
        this->button7->Size = System::Drawing::Size(160, 23);
        this->button7->TabIndex = 249;
        this->button7->Text = L"�������������� �� � ��";
        this->button7->UseVisualStyleBackColor = true;
        this->button7->Click += gcnew System::EventHandler(this, &Form1::button7_Click);
        // 
        // button6
        // 
        this->button6->Location = System::Drawing::Point(586, 257);
        this->button6->Name = L"button6";
        this->button6->Size = System::Drawing::Size(160, 23);
        this->button6->TabIndex = 248;
        this->button6->Text = L"��������������";
        this->button6->UseVisualStyleBackColor = true;
        this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
        // 
        // dataGridView_X_save
        // 
        this->dataGridView_X_save->AllowUserToDeleteRows = false;
        this->dataGridView_X_save->AllowUserToResizeColumns = false;
        this->dataGridView_X_save->AllowUserToResizeRows = false;
        dataGridViewCellStyle123->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle123->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle123->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle123->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle123->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle123->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle123->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_X_save->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle123;
        this->dataGridView_X_save->ColumnHeadersHeight = 19;
        this->dataGridView_X_save->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_X_save->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
            this->dataGridViewTextBoxColumn99,
                this->dataGridViewTextBoxColumn100, this->dataGridViewTextBoxColumn101, this->dataGridViewTextBoxColumn105, this->dataGridViewTextBoxColumn106
        });
        dataGridViewCellStyle124->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle124->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle124->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle124->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle124->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle124->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle124->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_X_save->DefaultCellStyle = dataGridViewCellStyle124;
        this->dataGridView_X_save->Location = System::Drawing::Point(1151, 816);
        this->dataGridView_X_save->Name = L"dataGridView_X_save";
        this->dataGridView_X_save->ReadOnly = true;
        this->dataGridView_X_save->RowHeadersVisible = false;
        this->dataGridView_X_save->Size = System::Drawing::Size(101, 43);
        this->dataGridView_X_save->TabIndex = 246;
        this->dataGridView_X_save->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_X_save_CellPainting);
        // 
        // dataGridViewTextBoxColumn99
        // 
        this->dataGridViewTextBoxColumn99->Frozen = true;
        this->dataGridViewTextBoxColumn99->HeaderText = L"7";
        this->dataGridViewTextBoxColumn99->Name = L"dataGridViewTextBoxColumn99";
        this->dataGridViewTextBoxColumn99->ReadOnly = true;
        this->dataGridViewTextBoxColumn99->Width = 20;
        // 
        // dataGridViewTextBoxColumn100
        // 
        this->dataGridViewTextBoxColumn100->Frozen = true;
        this->dataGridViewTextBoxColumn100->HeaderText = L"6";
        this->dataGridViewTextBoxColumn100->Name = L"dataGridViewTextBoxColumn100";
        this->dataGridViewTextBoxColumn100->ReadOnly = true;
        this->dataGridViewTextBoxColumn100->Width = 20;
        // 
        // dataGridViewTextBoxColumn101
        // 
        this->dataGridViewTextBoxColumn101->Frozen = true;
        this->dataGridViewTextBoxColumn101->HeaderText = L"5";
        this->dataGridViewTextBoxColumn101->Name = L"dataGridViewTextBoxColumn101";
        this->dataGridViewTextBoxColumn101->ReadOnly = true;
        this->dataGridViewTextBoxColumn101->Width = 20;
        // 
        // dataGridViewTextBoxColumn105
        // 
        this->dataGridViewTextBoxColumn105->Frozen = true;
        this->dataGridViewTextBoxColumn105->HeaderText = L"4";
        this->dataGridViewTextBoxColumn105->Name = L"dataGridViewTextBoxColumn105";
        this->dataGridViewTextBoxColumn105->ReadOnly = true;
        this->dataGridViewTextBoxColumn105->Width = 20;
        // 
        // dataGridViewTextBoxColumn106
        // 
        this->dataGridViewTextBoxColumn106->Frozen = true;
        this->dataGridViewTextBoxColumn106->HeaderText = L"3";
        this->dataGridViewTextBoxColumn106->Name = L"dataGridViewTextBoxColumn106";
        this->dataGridViewTextBoxColumn106->ReadOnly = true;
        this->dataGridViewTextBoxColumn106->Width = 20;
        // 
        // dataGridView_X_common
        // 
        this->dataGridView_X_common->AllowUserToDeleteRows = false;
        this->dataGridView_X_common->AllowUserToResizeColumns = false;
        this->dataGridView_X_common->AllowUserToResizeRows = false;
        dataGridViewCellStyle125->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle125->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle125->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle125->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle125->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle125->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle125->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_X_common->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle125;
        this->dataGridView_X_common->ColumnHeadersHeight = 19;
        this->dataGridView_X_common->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_X_common->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
            this->dataGridViewTextBoxColumn95,
                this->dataGridViewTextBoxColumn96, this->dataGridViewTextBoxColumn102, this->dataGridViewTextBoxColumn103, this->dataGridViewTextBoxColumn104
        });
        dataGridViewCellStyle126->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle126->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle126->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle126->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle126->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle126->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle126->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_X_common->DefaultCellStyle = dataGridViewCellStyle126;
        this->dataGridView_X_common->Location = System::Drawing::Point(1151, 767);
        this->dataGridView_X_common->Name = L"dataGridView_X_common";
        this->dataGridView_X_common->ReadOnly = true;
        this->dataGridView_X_common->RowHeadersVisible = false;
        this->dataGridView_X_common->Size = System::Drawing::Size(101, 43);
        this->dataGridView_X_common->TabIndex = 245;
        this->dataGridView_X_common->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_X_common_CellPainting);
        // 
        // dataGridViewTextBoxColumn95
        // 
        this->dataGridViewTextBoxColumn95->Frozen = true;
        this->dataGridViewTextBoxColumn95->HeaderText = L"9";
        this->dataGridViewTextBoxColumn95->Name = L"dataGridViewTextBoxColumn95";
        this->dataGridViewTextBoxColumn95->ReadOnly = true;
        this->dataGridViewTextBoxColumn95->Width = 20;
        // 
        // dataGridViewTextBoxColumn96
        // 
        this->dataGridViewTextBoxColumn96->Frozen = true;
        this->dataGridViewTextBoxColumn96->HeaderText = L"8";
        this->dataGridViewTextBoxColumn96->Name = L"dataGridViewTextBoxColumn96";
        this->dataGridViewTextBoxColumn96->ReadOnly = true;
        this->dataGridViewTextBoxColumn96->Width = 20;
        // 
        // dataGridViewTextBoxColumn102
        // 
        this->dataGridViewTextBoxColumn102->Frozen = true;
        this->dataGridViewTextBoxColumn102->HeaderText = L"2";
        this->dataGridViewTextBoxColumn102->Name = L"dataGridViewTextBoxColumn102";
        this->dataGridViewTextBoxColumn102->ReadOnly = true;
        this->dataGridViewTextBoxColumn102->Width = 20;
        // 
        // dataGridViewTextBoxColumn103
        // 
        this->dataGridViewTextBoxColumn103->Frozen = true;
        this->dataGridViewTextBoxColumn103->HeaderText = L"1";
        this->dataGridViewTextBoxColumn103->Name = L"dataGridViewTextBoxColumn103";
        this->dataGridViewTextBoxColumn103->ReadOnly = true;
        this->dataGridViewTextBoxColumn103->Width = 20;
        // 
        // dataGridViewTextBoxColumn104
        // 
        this->dataGridViewTextBoxColumn104->Frozen = true;
        this->dataGridViewTextBoxColumn104->HeaderText = L"0";
        this->dataGridViewTextBoxColumn104->Name = L"dataGridViewTextBoxColumn104";
        this->dataGridViewTextBoxColumn104->ReadOnly = true;
        this->dataGridViewTextBoxColumn104->Width = 20;
        // 
        // dataGridView_D_vector
        // 
        this->dataGridView_D_vector->AllowUserToDeleteRows = false;
        this->dataGridView_D_vector->AllowUserToResizeColumns = false;
        this->dataGridView_D_vector->AllowUserToResizeRows = false;
        dataGridViewCellStyle127->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle127->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle127->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle127->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle127->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle127->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle127->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_D_vector->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle127;
        this->dataGridView_D_vector->ColumnHeadersHeight = 19;
        this->dataGridView_D_vector->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_D_vector->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
            this->dataGridViewTextBoxColumn60,
                this->dataGridViewTextBoxColumn80, this->dataGridViewTextBoxColumn81
        });
        dataGridViewCellStyle128->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle128->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle128->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle128->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle128->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle128->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle128->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_D_vector->DefaultCellStyle = dataGridViewCellStyle128;
        this->dataGridView_D_vector->Location = System::Drawing::Point(1191, 718);
        this->dataGridView_D_vector->Name = L"dataGridView_D_vector";
        this->dataGridView_D_vector->ReadOnly = true;
        this->dataGridView_D_vector->RowHeadersVisible = false;
        this->dataGridView_D_vector->Size = System::Drawing::Size(61, 43);
        this->dataGridView_D_vector->TabIndex = 244;
        this->dataGridView_D_vector->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_D_vector_CellPainting);
        // 
        // dataGridViewTextBoxColumn60
        // 
        this->dataGridViewTextBoxColumn60->Frozen = true;
        this->dataGridViewTextBoxColumn60->HeaderText = L"2";
        this->dataGridViewTextBoxColumn60->Name = L"dataGridViewTextBoxColumn60";
        this->dataGridViewTextBoxColumn60->ReadOnly = true;
        this->dataGridViewTextBoxColumn60->Width = 20;
        // 
        // dataGridViewTextBoxColumn80
        // 
        this->dataGridViewTextBoxColumn80->Frozen = true;
        this->dataGridViewTextBoxColumn80->HeaderText = L"1";
        this->dataGridViewTextBoxColumn80->Name = L"dataGridViewTextBoxColumn80";
        this->dataGridViewTextBoxColumn80->ReadOnly = true;
        this->dataGridViewTextBoxColumn80->Width = 20;
        // 
        // dataGridViewTextBoxColumn81
        // 
        this->dataGridViewTextBoxColumn81->Frozen = true;
        this->dataGridViewTextBoxColumn81->HeaderText = L"0";
        this->dataGridViewTextBoxColumn81->Name = L"dataGridViewTextBoxColumn81";
        this->dataGridViewTextBoxColumn81->ReadOnly = true;
        this->dataGridViewTextBoxColumn81->Width = 20;
        // 
        // dataGridView_Y_vector
        // 
        this->dataGridView_Y_vector->AllowUserToDeleteRows = false;
        this->dataGridView_Y_vector->AllowUserToResizeColumns = false;
        this->dataGridView_Y_vector->AllowUserToResizeRows = false;
        dataGridViewCellStyle129->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle129->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle129->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle129->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle129->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle129->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle129->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_Y_vector->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle129;
        this->dataGridView_Y_vector->ColumnHeadersHeight = 19;
        this->dataGridView_Y_vector->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_Y_vector->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(13) {
            this->dataGridViewTextBoxColumn82,
                this->dataGridViewTextBoxColumn83, this->dataGridViewTextBoxColumn84, this->dataGridViewTextBoxColumn85, this->dataGridViewTextBoxColumn86,
                this->dataGridViewTextBoxColumn87, this->dataGridViewTextBoxColumn88, this->dataGridViewTextBoxColumn89, this->dataGridViewTextBoxColumn90,
                this->dataGridViewTextBoxColumn91, this->dataGridViewTextBoxColumn92, this->dataGridViewTextBoxColumn93, this->dataGridViewTextBoxColumn94
        });
        dataGridViewCellStyle130->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle130->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle130->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle130->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle130->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle130->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle130->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_Y_vector->DefaultCellStyle = dataGridViewCellStyle130;
        this->dataGridView_Y_vector->Location = System::Drawing::Point(991, 669);
        this->dataGridView_Y_vector->Name = L"dataGridView_Y_vector";
        this->dataGridView_Y_vector->ReadOnly = true;
        this->dataGridView_Y_vector->RowHeadersVisible = false;
        this->dataGridView_Y_vector->Size = System::Drawing::Size(261, 43);
        this->dataGridView_Y_vector->TabIndex = 243;
        this->dataGridView_Y_vector->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_Y_vector_CellPainting);
        // 
        // dataGridViewTextBoxColumn82
        // 
        this->dataGridViewTextBoxColumn82->Frozen = true;
        this->dataGridViewTextBoxColumn82->HeaderText = L"12";
        this->dataGridViewTextBoxColumn82->Name = L"dataGridViewTextBoxColumn82";
        this->dataGridViewTextBoxColumn82->ReadOnly = true;
        this->dataGridViewTextBoxColumn82->Width = 20;
        // 
        // dataGridViewTextBoxColumn83
        // 
        this->dataGridViewTextBoxColumn83->Frozen = true;
        this->dataGridViewTextBoxColumn83->HeaderText = L"11";
        this->dataGridViewTextBoxColumn83->Name = L"dataGridViewTextBoxColumn83";
        this->dataGridViewTextBoxColumn83->ReadOnly = true;
        this->dataGridViewTextBoxColumn83->Width = 20;
        // 
        // dataGridViewTextBoxColumn84
        // 
        this->dataGridViewTextBoxColumn84->Frozen = true;
        this->dataGridViewTextBoxColumn84->HeaderText = L"10";
        this->dataGridViewTextBoxColumn84->Name = L"dataGridViewTextBoxColumn84";
        this->dataGridViewTextBoxColumn84->ReadOnly = true;
        this->dataGridViewTextBoxColumn84->Width = 20;
        // 
        // dataGridViewTextBoxColumn85
        // 
        this->dataGridViewTextBoxColumn85->Frozen = true;
        this->dataGridViewTextBoxColumn85->HeaderText = L"9";
        this->dataGridViewTextBoxColumn85->Name = L"dataGridViewTextBoxColumn85";
        this->dataGridViewTextBoxColumn85->ReadOnly = true;
        this->dataGridViewTextBoxColumn85->Width = 20;
        // 
        // dataGridViewTextBoxColumn86
        // 
        this->dataGridViewTextBoxColumn86->Frozen = true;
        this->dataGridViewTextBoxColumn86->HeaderText = L"8";
        this->dataGridViewTextBoxColumn86->Name = L"dataGridViewTextBoxColumn86";
        this->dataGridViewTextBoxColumn86->ReadOnly = true;
        this->dataGridViewTextBoxColumn86->Width = 20;
        // 
        // dataGridViewTextBoxColumn87
        // 
        this->dataGridViewTextBoxColumn87->Frozen = true;
        this->dataGridViewTextBoxColumn87->HeaderText = L"7";
        this->dataGridViewTextBoxColumn87->Name = L"dataGridViewTextBoxColumn87";
        this->dataGridViewTextBoxColumn87->ReadOnly = true;
        this->dataGridViewTextBoxColumn87->Width = 20;
        // 
        // dataGridViewTextBoxColumn88
        // 
        this->dataGridViewTextBoxColumn88->Frozen = true;
        this->dataGridViewTextBoxColumn88->HeaderText = L"6";
        this->dataGridViewTextBoxColumn88->Name = L"dataGridViewTextBoxColumn88";
        this->dataGridViewTextBoxColumn88->ReadOnly = true;
        this->dataGridViewTextBoxColumn88->Width = 20;
        // 
        // dataGridViewTextBoxColumn89
        // 
        this->dataGridViewTextBoxColumn89->Frozen = true;
        this->dataGridViewTextBoxColumn89->HeaderText = L"5";
        this->dataGridViewTextBoxColumn89->Name = L"dataGridViewTextBoxColumn89";
        this->dataGridViewTextBoxColumn89->ReadOnly = true;
        this->dataGridViewTextBoxColumn89->Width = 20;
        // 
        // dataGridViewTextBoxColumn90
        // 
        this->dataGridViewTextBoxColumn90->Frozen = true;
        this->dataGridViewTextBoxColumn90->HeaderText = L"4";
        this->dataGridViewTextBoxColumn90->Name = L"dataGridViewTextBoxColumn90";
        this->dataGridViewTextBoxColumn90->ReadOnly = true;
        this->dataGridViewTextBoxColumn90->Width = 20;
        // 
        // dataGridViewTextBoxColumn91
        // 
        this->dataGridViewTextBoxColumn91->Frozen = true;
        this->dataGridViewTextBoxColumn91->HeaderText = L"3";
        this->dataGridViewTextBoxColumn91->Name = L"dataGridViewTextBoxColumn91";
        this->dataGridViewTextBoxColumn91->ReadOnly = true;
        this->dataGridViewTextBoxColumn91->Width = 20;
        // 
        // dataGridViewTextBoxColumn92
        // 
        this->dataGridViewTextBoxColumn92->Frozen = true;
        this->dataGridViewTextBoxColumn92->HeaderText = L"2";
        this->dataGridViewTextBoxColumn92->Name = L"dataGridViewTextBoxColumn92";
        this->dataGridViewTextBoxColumn92->ReadOnly = true;
        this->dataGridViewTextBoxColumn92->Width = 20;
        // 
        // dataGridViewTextBoxColumn93
        // 
        this->dataGridViewTextBoxColumn93->Frozen = true;
        this->dataGridViewTextBoxColumn93->HeaderText = L"1";
        this->dataGridViewTextBoxColumn93->Name = L"dataGridViewTextBoxColumn93";
        this->dataGridViewTextBoxColumn93->ReadOnly = true;
        this->dataGridViewTextBoxColumn93->Width = 20;
        // 
        // dataGridViewTextBoxColumn94
        // 
        this->dataGridViewTextBoxColumn94->Frozen = true;
        this->dataGridViewTextBoxColumn94->HeaderText = L"0";
        this->dataGridViewTextBoxColumn94->Name = L"dataGridViewTextBoxColumn94";
        this->dataGridViewTextBoxColumn94->ReadOnly = true;
        this->dataGridViewTextBoxColumn94->Width = 20;
        // 
        // dataGridView_mem_conditions
        // 
        this->dataGridView_mem_conditions->AllowUserToDeleteRows = false;
        this->dataGridView_mem_conditions->AllowUserToResizeColumns = false;
        this->dataGridView_mem_conditions->AllowUserToResizeRows = false;
        dataGridViewCellStyle131->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle131->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle131->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle131->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle131->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle131->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle131->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_mem_conditions->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle131;
        this->dataGridView_mem_conditions->ColumnHeadersHeight = 19;
        this->dataGridView_mem_conditions->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_mem_conditions->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
            this->dataGridViewTextBoxColumn55,
                this->dataGridViewTextBoxColumn56, this->dataGridViewTextBoxColumn57, this->dataGridViewTextBoxColumn58, this->dataGridViewTextBoxColumn59
        });
        dataGridViewCellStyle132->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle132->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle132->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle132->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle132->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle132->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle132->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_mem_conditions->DefaultCellStyle = dataGridViewCellStyle132;
        this->dataGridView_mem_conditions->Location = System::Drawing::Point(1151, 620);
        this->dataGridView_mem_conditions->Name = L"dataGridView_mem_conditions";
        this->dataGridView_mem_conditions->ReadOnly = true;
        this->dataGridView_mem_conditions->RowHeadersVisible = false;
        this->dataGridView_mem_conditions->Size = System::Drawing::Size(101, 43);
        this->dataGridView_mem_conditions->TabIndex = 242;
        this->dataGridView_mem_conditions->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_mem_conditions_CellPainting);
        // 
        // dataGridViewTextBoxColumn55
        // 
        this->dataGridViewTextBoxColumn55->Frozen = true;
        this->dataGridViewTextBoxColumn55->HeaderText = L"4";
        this->dataGridViewTextBoxColumn55->Name = L"dataGridViewTextBoxColumn55";
        this->dataGridViewTextBoxColumn55->ReadOnly = true;
        this->dataGridViewTextBoxColumn55->Width = 20;
        // 
        // dataGridViewTextBoxColumn56
        // 
        this->dataGridViewTextBoxColumn56->Frozen = true;
        this->dataGridViewTextBoxColumn56->HeaderText = L"3";
        this->dataGridViewTextBoxColumn56->Name = L"dataGridViewTextBoxColumn56";
        this->dataGridViewTextBoxColumn56->ReadOnly = true;
        this->dataGridViewTextBoxColumn56->Width = 20;
        // 
        // dataGridViewTextBoxColumn57
        // 
        this->dataGridViewTextBoxColumn57->Frozen = true;
        this->dataGridViewTextBoxColumn57->HeaderText = L"2";
        this->dataGridViewTextBoxColumn57->Name = L"dataGridViewTextBoxColumn57";
        this->dataGridViewTextBoxColumn57->ReadOnly = true;
        this->dataGridViewTextBoxColumn57->Width = 20;
        // 
        // dataGridViewTextBoxColumn58
        // 
        this->dataGridViewTextBoxColumn58->Frozen = true;
        this->dataGridViewTextBoxColumn58->HeaderText = L"1";
        this->dataGridViewTextBoxColumn58->Name = L"dataGridViewTextBoxColumn58";
        this->dataGridViewTextBoxColumn58->ReadOnly = true;
        this->dataGridViewTextBoxColumn58->Width = 20;
        // 
        // dataGridViewTextBoxColumn59
        // 
        this->dataGridViewTextBoxColumn59->Frozen = true;
        this->dataGridViewTextBoxColumn59->HeaderText = L"0";
        this->dataGridViewTextBoxColumn59->Name = L"dataGridViewTextBoxColumn59";
        this->dataGridViewTextBoxColumn59->ReadOnly = true;
        this->dataGridViewTextBoxColumn59->Width = 20;
        // 
        // dataGridView_X_vector
        // 
        this->dataGridView_X_vector->AllowUserToDeleteRows = false;
        this->dataGridView_X_vector->AllowUserToResizeColumns = false;
        this->dataGridView_X_vector->AllowUserToResizeRows = false;
        dataGridViewCellStyle133->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle133->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle133->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle133->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle133->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle133->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle133->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_X_vector->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle133;
        this->dataGridView_X_vector->ColumnHeadersHeight = 19;
        this->dataGridView_X_vector->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_X_vector->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(10) {
            this->dataGridViewTextBoxColumn61,
                this->dataGridViewTextBoxColumn66, this->dataGridViewTextBoxColumn67, this->dataGridViewTextBoxColumn68, this->dataGridViewTextBoxColumn69,
                this->dataGridViewTextBoxColumn75, this->dataGridViewTextBoxColumn76, this->dataGridViewTextBoxColumn77, this->dataGridViewTextBoxColumn78,
                this->dataGridViewTextBoxColumn79
        });
        dataGridViewCellStyle134->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle134->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle134->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle134->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle134->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle134->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle134->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_X_vector->DefaultCellStyle = dataGridViewCellStyle134;
        this->dataGridView_X_vector->Location = System::Drawing::Point(1051, 571);
        this->dataGridView_X_vector->Name = L"dataGridView_X_vector";
        this->dataGridView_X_vector->ReadOnly = true;
        this->dataGridView_X_vector->RowHeadersVisible = false;
        this->dataGridView_X_vector->Size = System::Drawing::Size(201, 43);
        this->dataGridView_X_vector->TabIndex = 241;
        this->dataGridView_X_vector->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_X_vector_CellPainting);
        // 
        // dataGridViewTextBoxColumn61
        // 
        this->dataGridViewTextBoxColumn61->Frozen = true;
        this->dataGridViewTextBoxColumn61->HeaderText = L"9";
        this->dataGridViewTextBoxColumn61->Name = L"dataGridViewTextBoxColumn61";
        this->dataGridViewTextBoxColumn61->ReadOnly = true;
        this->dataGridViewTextBoxColumn61->Width = 20;
        // 
        // dataGridViewTextBoxColumn66
        // 
        this->dataGridViewTextBoxColumn66->Frozen = true;
        this->dataGridViewTextBoxColumn66->HeaderText = L"8";
        this->dataGridViewTextBoxColumn66->Name = L"dataGridViewTextBoxColumn66";
        this->dataGridViewTextBoxColumn66->ReadOnly = true;
        this->dataGridViewTextBoxColumn66->Width = 20;
        // 
        // dataGridViewTextBoxColumn67
        // 
        this->dataGridViewTextBoxColumn67->Frozen = true;
        this->dataGridViewTextBoxColumn67->HeaderText = L"7";
        this->dataGridViewTextBoxColumn67->Name = L"dataGridViewTextBoxColumn67";
        this->dataGridViewTextBoxColumn67->ReadOnly = true;
        this->dataGridViewTextBoxColumn67->Width = 20;
        // 
        // dataGridViewTextBoxColumn68
        // 
        this->dataGridViewTextBoxColumn68->Frozen = true;
        this->dataGridViewTextBoxColumn68->HeaderText = L"6";
        this->dataGridViewTextBoxColumn68->Name = L"dataGridViewTextBoxColumn68";
        this->dataGridViewTextBoxColumn68->ReadOnly = true;
        this->dataGridViewTextBoxColumn68->Width = 20;
        // 
        // dataGridViewTextBoxColumn69
        // 
        this->dataGridViewTextBoxColumn69->Frozen = true;
        this->dataGridViewTextBoxColumn69->HeaderText = L"5";
        this->dataGridViewTextBoxColumn69->Name = L"dataGridViewTextBoxColumn69";
        this->dataGridViewTextBoxColumn69->ReadOnly = true;
        this->dataGridViewTextBoxColumn69->Width = 20;
        // 
        // dataGridViewTextBoxColumn75
        // 
        this->dataGridViewTextBoxColumn75->Frozen = true;
        this->dataGridViewTextBoxColumn75->HeaderText = L"4";
        this->dataGridViewTextBoxColumn75->Name = L"dataGridViewTextBoxColumn75";
        this->dataGridViewTextBoxColumn75->ReadOnly = true;
        this->dataGridViewTextBoxColumn75->Width = 20;
        // 
        // dataGridViewTextBoxColumn76
        // 
        this->dataGridViewTextBoxColumn76->Frozen = true;
        this->dataGridViewTextBoxColumn76->HeaderText = L"3";
        this->dataGridViewTextBoxColumn76->Name = L"dataGridViewTextBoxColumn76";
        this->dataGridViewTextBoxColumn76->ReadOnly = true;
        this->dataGridViewTextBoxColumn76->Width = 20;
        // 
        // dataGridViewTextBoxColumn77
        // 
        this->dataGridViewTextBoxColumn77->Frozen = true;
        this->dataGridViewTextBoxColumn77->HeaderText = L"2";
        this->dataGridViewTextBoxColumn77->Name = L"dataGridViewTextBoxColumn77";
        this->dataGridViewTextBoxColumn77->ReadOnly = true;
        this->dataGridViewTextBoxColumn77->Width = 20;
        // 
        // dataGridViewTextBoxColumn78
        // 
        this->dataGridViewTextBoxColumn78->Frozen = true;
        this->dataGridViewTextBoxColumn78->HeaderText = L"1";
        this->dataGridViewTextBoxColumn78->Name = L"dataGridViewTextBoxColumn78";
        this->dataGridViewTextBoxColumn78->ReadOnly = true;
        this->dataGridViewTextBoxColumn78->Width = 20;
        // 
        // dataGridViewTextBoxColumn79
        // 
        this->dataGridViewTextBoxColumn79->Frozen = true;
        this->dataGridViewTextBoxColumn79->HeaderText = L"0";
        this->dataGridViewTextBoxColumn79->Name = L"dataGridViewTextBoxColumn79";
        this->dataGridViewTextBoxColumn79->ReadOnly = true;
        this->dataGridViewTextBoxColumn79->Width = 20;
        // 
        // dataGridView_unitary_code
        // 
        this->dataGridView_unitary_code->AllowUserToDeleteRows = false;
        this->dataGridView_unitary_code->AllowUserToResizeColumns = false;
        this->dataGridView_unitary_code->AllowUserToResizeRows = false;
        dataGridViewCellStyle135->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle135->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle135->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle135->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle135->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle135->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle135->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_unitary_code->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle135;
        this->dataGridView_unitary_code->ColumnHeadersHeight = 19;
        this->dataGridView_unitary_code->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_unitary_code->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
            this->dataGridViewTextBoxColumn70,
                this->dataGridViewTextBoxColumn71, this->dataGridViewTextBoxColumn72, this->dataGridViewTextBoxColumn73, this->dataGridViewTextBoxColumn74
        });
        dataGridViewCellStyle136->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle136->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle136->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle136->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle136->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle136->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle136->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_unitary_code->DefaultCellStyle = dataGridViewCellStyle136;
        this->dataGridView_unitary_code->Location = System::Drawing::Point(1151, 522);
        this->dataGridView_unitary_code->Name = L"dataGridView_unitary_code";
        this->dataGridView_unitary_code->ReadOnly = true;
        this->dataGridView_unitary_code->RowHeadersVisible = false;
        this->dataGridView_unitary_code->Size = System::Drawing::Size(101, 43);
        this->dataGridView_unitary_code->TabIndex = 240;
        this->dataGridView_unitary_code->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_unitary_code_CellPainting);
        // 
        // dataGridViewTextBoxColumn70
        // 
        this->dataGridViewTextBoxColumn70->Frozen = true;
        this->dataGridViewTextBoxColumn70->HeaderText = L"4";
        this->dataGridViewTextBoxColumn70->Name = L"dataGridViewTextBoxColumn70";
        this->dataGridViewTextBoxColumn70->ReadOnly = true;
        this->dataGridViewTextBoxColumn70->Width = 20;
        // 
        // dataGridViewTextBoxColumn71
        // 
        this->dataGridViewTextBoxColumn71->Frozen = true;
        this->dataGridViewTextBoxColumn71->HeaderText = L"3";
        this->dataGridViewTextBoxColumn71->Name = L"dataGridViewTextBoxColumn71";
        this->dataGridViewTextBoxColumn71->ReadOnly = true;
        this->dataGridViewTextBoxColumn71->Width = 20;
        // 
        // dataGridViewTextBoxColumn72
        // 
        this->dataGridViewTextBoxColumn72->Frozen = true;
        this->dataGridViewTextBoxColumn72->HeaderText = L"2";
        this->dataGridViewTextBoxColumn72->Name = L"dataGridViewTextBoxColumn72";
        this->dataGridViewTextBoxColumn72->ReadOnly = true;
        this->dataGridViewTextBoxColumn72->Width = 20;
        // 
        // dataGridViewTextBoxColumn73
        // 
        this->dataGridViewTextBoxColumn73->Frozen = true;
        this->dataGridViewTextBoxColumn73->HeaderText = L"1";
        this->dataGridViewTextBoxColumn73->Name = L"dataGridViewTextBoxColumn73";
        this->dataGridViewTextBoxColumn73->ReadOnly = true;
        this->dataGridViewTextBoxColumn73->Width = 20;
        // 
        // dataGridViewTextBoxColumn74
        // 
        this->dataGridViewTextBoxColumn74->Frozen = true;
        this->dataGridViewTextBoxColumn74->HeaderText = L"0";
        this->dataGridViewTextBoxColumn74->Name = L"dataGridViewTextBoxColumn74";
        this->dataGridViewTextBoxColumn74->ReadOnly = true;
        this->dataGridViewTextBoxColumn74->Width = 20;
        // 
        // dataGridView_mem_states
        // 
        this->dataGridView_mem_states->AllowUserToDeleteRows = false;
        this->dataGridView_mem_states->AllowUserToResizeColumns = false;
        this->dataGridView_mem_states->AllowUserToResizeRows = false;
        dataGridViewCellStyle137->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle137->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle137->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle137->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle137->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle137->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle137->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_mem_states->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle137;
        this->dataGridView_mem_states->ColumnHeadersHeight = 19;
        this->dataGridView_mem_states->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_mem_states->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
            this->dataGridViewTextBoxColumn52,
                this->dataGridViewTextBoxColumn53, this->dataGridViewTextBoxColumn54
        });
        dataGridViewCellStyle138->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle138->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle138->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle138->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle138->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle138->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle138->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_mem_states->DefaultCellStyle = dataGridViewCellStyle138;
        this->dataGridView_mem_states->Location = System::Drawing::Point(1191, 473);
        this->dataGridView_mem_states->Name = L"dataGridView_mem_states";
        this->dataGridView_mem_states->ReadOnly = true;
        this->dataGridView_mem_states->RowHeadersVisible = false;
        this->dataGridView_mem_states->Size = System::Drawing::Size(61, 43);
        this->dataGridView_mem_states->TabIndex = 239;
        this->dataGridView_mem_states->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_mem_states_CellPainting);
        // 
        // dataGridViewTextBoxColumn52
        // 
        this->dataGridViewTextBoxColumn52->Frozen = true;
        this->dataGridViewTextBoxColumn52->HeaderText = L"2";
        this->dataGridViewTextBoxColumn52->Name = L"dataGridViewTextBoxColumn52";
        this->dataGridViewTextBoxColumn52->ReadOnly = true;
        this->dataGridViewTextBoxColumn52->Width = 20;
        // 
        // dataGridViewTextBoxColumn53
        // 
        this->dataGridViewTextBoxColumn53->Frozen = true;
        this->dataGridViewTextBoxColumn53->HeaderText = L"1";
        this->dataGridViewTextBoxColumn53->Name = L"dataGridViewTextBoxColumn53";
        this->dataGridViewTextBoxColumn53->ReadOnly = true;
        this->dataGridViewTextBoxColumn53->Width = 20;
        // 
        // dataGridViewTextBoxColumn54
        // 
        this->dataGridViewTextBoxColumn54->Frozen = true;
        this->dataGridViewTextBoxColumn54->HeaderText = L"0";
        this->dataGridViewTextBoxColumn54->Name = L"dataGridViewTextBoxColumn54";
        this->dataGridViewTextBoxColumn54->ReadOnly = true;
        this->dataGridViewTextBoxColumn54->Width = 20;
        // 
        // label88
        // 
        this->label88->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label88->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label88->ForeColor = System::Drawing::SystemColors::Window;
        this->label88->Location = System::Drawing::Point(1110, 78);
        this->label88->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label88->Name = L"label88";
        this->label88->Size = System::Drawing::Size(100, 20);
        this->label88->TabIndex = 238;
        this->label88->Text = L"0";
        this->label88->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // label87
        // 
        this->label87->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label87->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label87->ForeColor = System::Drawing::SystemColors::Window;
        this->label87->Location = System::Drawing::Point(1110, 36);
        this->label87->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label87->Name = L"label87";
        this->label87->Size = System::Drawing::Size(100, 20);
        this->label87->TabIndex = 237;
        this->label87->Text = L"0";
        this->label87->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // dataGridView_state
        // 
        this->dataGridView_state->AllowUserToDeleteRows = false;
        this->dataGridView_state->AllowUserToResizeColumns = false;
        this->dataGridView_state->AllowUserToResizeRows = false;
        dataGridViewCellStyle139->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle139->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle139->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle139->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle139->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle139->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle139->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_state->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle139;
        this->dataGridView_state->ColumnHeadersHeight = 19;
        this->dataGridView_state->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_state->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
            this->dataGridViewTextBoxColumn49,
                this->dataGridViewTextBoxColumn50, this->dataGridViewTextBoxColumn51
        });
        dataGridViewCellStyle140->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle140->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle140->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle140->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle140->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle140->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle140->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_state->DefaultCellStyle = dataGridViewCellStyle140;
        this->dataGridView_state->Location = System::Drawing::Point(1044, 340);
        this->dataGridView_state->Name = L"dataGridView_state";
        this->dataGridView_state->ReadOnly = true;
        this->dataGridView_state->RowHeadersVisible = false;
        this->dataGridView_state->Size = System::Drawing::Size(61, 43);
        this->dataGridView_state->TabIndex = 236;
        this->dataGridView_state->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_state_CellPainting);
        // 
        // dataGridViewTextBoxColumn49
        // 
        this->dataGridViewTextBoxColumn49->Frozen = true;
        this->dataGridViewTextBoxColumn49->HeaderText = L"2";
        this->dataGridViewTextBoxColumn49->Name = L"dataGridViewTextBoxColumn49";
        this->dataGridViewTextBoxColumn49->ReadOnly = true;
        this->dataGridViewTextBoxColumn49->Width = 20;
        // 
        // dataGridViewTextBoxColumn50
        // 
        this->dataGridViewTextBoxColumn50->Frozen = true;
        this->dataGridViewTextBoxColumn50->HeaderText = L"1";
        this->dataGridViewTextBoxColumn50->Name = L"dataGridViewTextBoxColumn50";
        this->dataGridViewTextBoxColumn50->ReadOnly = true;
        this->dataGridViewTextBoxColumn50->Width = 20;
        // 
        // dataGridViewTextBoxColumn51
        // 
        this->dataGridViewTextBoxColumn51->Frozen = true;
        this->dataGridViewTextBoxColumn51->HeaderText = L"0";
        this->dataGridViewTextBoxColumn51->Name = L"dataGridViewTextBoxColumn51";
        this->dataGridViewTextBoxColumn51->ReadOnly = true;
        this->dataGridViewTextBoxColumn51->Width = 20;
        // 
        // dataGridView_counter
        // 
        this->dataGridView_counter->AllowUserToDeleteRows = false;
        this->dataGridView_counter->AllowUserToResizeColumns = false;
        this->dataGridView_counter->AllowUserToResizeRows = false;
        dataGridViewCellStyle141->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle141->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle141->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle141->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle141->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle141->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle141->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_counter->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle141;
        this->dataGridView_counter->ColumnHeadersHeight = 19;
        this->dataGridView_counter->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_counter->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
            this->dataGridViewTextBoxColumn62,
                this->dataGridViewTextBoxColumn63, this->dataGridViewTextBoxColumn65
        });
        dataGridViewCellStyle142->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle142->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle142->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle142->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle142->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle142->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle142->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_counter->DefaultCellStyle = dataGridViewCellStyle142;
        this->dataGridView_counter->Location = System::Drawing::Point(1044, 299);
        this->dataGridView_counter->Name = L"dataGridView_counter";
        this->dataGridView_counter->ReadOnly = true;
        this->dataGridView_counter->RowHeadersVisible = false;
        this->dataGridView_counter->Size = System::Drawing::Size(61, 43);
        this->dataGridView_counter->TabIndex = 235;
        this->dataGridView_counter->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_counter_CellPainting);
        // 
        // dataGridViewTextBoxColumn62
        // 
        this->dataGridViewTextBoxColumn62->Frozen = true;
        this->dataGridViewTextBoxColumn62->HeaderText = L"2";
        this->dataGridViewTextBoxColumn62->Name = L"dataGridViewTextBoxColumn62";
        this->dataGridViewTextBoxColumn62->ReadOnly = true;
        this->dataGridViewTextBoxColumn62->Width = 20;
        // 
        // dataGridViewTextBoxColumn63
        // 
        this->dataGridViewTextBoxColumn63->Frozen = true;
        this->dataGridViewTextBoxColumn63->HeaderText = L"1";
        this->dataGridViewTextBoxColumn63->Name = L"dataGridViewTextBoxColumn63";
        this->dataGridViewTextBoxColumn63->ReadOnly = true;
        this->dataGridViewTextBoxColumn63->Width = 20;
        // 
        // dataGridViewTextBoxColumn65
        // 
        this->dataGridViewTextBoxColumn65->Frozen = true;
        this->dataGridViewTextBoxColumn65->HeaderText = L"0";
        this->dataGridViewTextBoxColumn65->Name = L"dataGridViewTextBoxColumn65";
        this->dataGridViewTextBoxColumn65->ReadOnly = true;
        this->dataGridViewTextBoxColumn65->Width = 20;
        // 
        // dataGridView_D
        // 
        this->dataGridView_D->AllowUserToDeleteRows = false;
        this->dataGridView_D->AllowUserToResizeColumns = false;
        this->dataGridView_D->AllowUserToResizeRows = false;
        dataGridViewCellStyle143->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle143->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle143->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle143->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle143->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle143->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle143->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_D->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle143;
        this->dataGridView_D->ColumnHeadersHeight = 19;
        this->dataGridView_D->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_D->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(1) { this->dataGridViewTextBoxColumn64 });
        dataGridViewCellStyle144->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle144->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle144->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle144->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle144->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle144->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle144->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_D->DefaultCellStyle = dataGridViewCellStyle144;
        this->dataGridView_D->Location = System::Drawing::Point(1084, 257);
        this->dataGridView_D->Name = L"dataGridView_D";
        this->dataGridView_D->ReadOnly = true;
        this->dataGridView_D->RowHeadersVisible = false;
        this->dataGridView_D->Size = System::Drawing::Size(21, 43);
        this->dataGridView_D->TabIndex = 234;
        this->dataGridView_D->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_D_CellPainting);
        // 
        // dataGridViewTextBoxColumn64
        // 
        this->dataGridViewTextBoxColumn64->Frozen = true;
        this->dataGridViewTextBoxColumn64->HeaderText = L"0";
        this->dataGridViewTextBoxColumn64->Name = L"dataGridViewTextBoxColumn64";
        this->dataGridViewTextBoxColumn64->ReadOnly = true;
        this->dataGridViewTextBoxColumn64->Width = 20;
        // 
        // dataGridView_B_run
        // 
        this->dataGridView_B_run->AllowUserToDeleteRows = false;
        this->dataGridView_B_run->AllowUserToResizeColumns = false;
        this->dataGridView_B_run->AllowUserToResizeRows = false;
        dataGridViewCellStyle145->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle145->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle145->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle145->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle145->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle145->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle145->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_B_run->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle145;
        this->dataGridView_B_run->ColumnHeadersHeight = 19;
        this->dataGridView_B_run->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_B_run->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(16) {
            this->dataGridViewTextBoxColumn17,
                this->dataGridViewTextBoxColumn18, this->dataGridViewTextBoxColumn19, this->dataGridViewTextBoxColumn20, this->dataGridViewTextBoxColumn21,
                this->dataGridViewTextBoxColumn22, this->dataGridViewTextBoxColumn23, this->dataGridViewTextBoxColumn24, this->dataGridViewTextBoxColumn25,
                this->dataGridViewTextBoxColumn26, this->dataGridViewTextBoxColumn27, this->dataGridViewTextBoxColumn28, this->dataGridViewTextBoxColumn29,
                this->dataGridViewTextBoxColumn30, this->dataGridViewTextBoxColumn31, this->dataGridViewTextBoxColumn32
        });
        dataGridViewCellStyle146->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle146->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle146->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle146->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle146->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle146->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle146->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_B_run->DefaultCellStyle = dataGridViewCellStyle146;
        this->dataGridView_B_run->Location = System::Drawing::Point(784, 117);
        this->dataGridView_B_run->Name = L"dataGridView_B_run";
        this->dataGridView_B_run->ReadOnly = true;
        this->dataGridView_B_run->RowHeadersVisible = false;
        this->dataGridView_B_run->Size = System::Drawing::Size(321, 43);
        this->dataGridView_B_run->TabIndex = 233;
        this->dataGridView_B_run->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_B_run_CellPainting);
        // 
        // dataGridViewTextBoxColumn17
        // 
        this->dataGridViewTextBoxColumn17->Frozen = true;
        this->dataGridViewTextBoxColumn17->HeaderText = L"15";
        this->dataGridViewTextBoxColumn17->Name = L"dataGridViewTextBoxColumn17";
        this->dataGridViewTextBoxColumn17->ReadOnly = true;
        this->dataGridViewTextBoxColumn17->Width = 20;
        // 
        // dataGridViewTextBoxColumn18
        // 
        this->dataGridViewTextBoxColumn18->Frozen = true;
        this->dataGridViewTextBoxColumn18->HeaderText = L"14";
        this->dataGridViewTextBoxColumn18->Name = L"dataGridViewTextBoxColumn18";
        this->dataGridViewTextBoxColumn18->ReadOnly = true;
        this->dataGridViewTextBoxColumn18->Width = 20;
        // 
        // dataGridViewTextBoxColumn19
        // 
        this->dataGridViewTextBoxColumn19->Frozen = true;
        this->dataGridViewTextBoxColumn19->HeaderText = L"13";
        this->dataGridViewTextBoxColumn19->Name = L"dataGridViewTextBoxColumn19";
        this->dataGridViewTextBoxColumn19->ReadOnly = true;
        this->dataGridViewTextBoxColumn19->Width = 20;
        // 
        // dataGridViewTextBoxColumn20
        // 
        this->dataGridViewTextBoxColumn20->Frozen = true;
        this->dataGridViewTextBoxColumn20->HeaderText = L"12";
        this->dataGridViewTextBoxColumn20->Name = L"dataGridViewTextBoxColumn20";
        this->dataGridViewTextBoxColumn20->ReadOnly = true;
        this->dataGridViewTextBoxColumn20->Width = 20;
        // 
        // dataGridViewTextBoxColumn21
        // 
        this->dataGridViewTextBoxColumn21->Frozen = true;
        this->dataGridViewTextBoxColumn21->HeaderText = L"11";
        this->dataGridViewTextBoxColumn21->Name = L"dataGridViewTextBoxColumn21";
        this->dataGridViewTextBoxColumn21->ReadOnly = true;
        this->dataGridViewTextBoxColumn21->Width = 20;
        // 
        // dataGridViewTextBoxColumn22
        // 
        this->dataGridViewTextBoxColumn22->Frozen = true;
        this->dataGridViewTextBoxColumn22->HeaderText = L"10";
        this->dataGridViewTextBoxColumn22->Name = L"dataGridViewTextBoxColumn22";
        this->dataGridViewTextBoxColumn22->ReadOnly = true;
        this->dataGridViewTextBoxColumn22->Width = 20;
        // 
        // dataGridViewTextBoxColumn23
        // 
        this->dataGridViewTextBoxColumn23->Frozen = true;
        this->dataGridViewTextBoxColumn23->HeaderText = L"9";
        this->dataGridViewTextBoxColumn23->Name = L"dataGridViewTextBoxColumn23";
        this->dataGridViewTextBoxColumn23->ReadOnly = true;
        this->dataGridViewTextBoxColumn23->Width = 20;
        // 
        // dataGridViewTextBoxColumn24
        // 
        this->dataGridViewTextBoxColumn24->Frozen = true;
        this->dataGridViewTextBoxColumn24->HeaderText = L"8";
        this->dataGridViewTextBoxColumn24->Name = L"dataGridViewTextBoxColumn24";
        this->dataGridViewTextBoxColumn24->ReadOnly = true;
        this->dataGridViewTextBoxColumn24->Width = 20;
        // 
        // dataGridViewTextBoxColumn25
        // 
        this->dataGridViewTextBoxColumn25->Frozen = true;
        this->dataGridViewTextBoxColumn25->HeaderText = L"7";
        this->dataGridViewTextBoxColumn25->Name = L"dataGridViewTextBoxColumn25";
        this->dataGridViewTextBoxColumn25->ReadOnly = true;
        this->dataGridViewTextBoxColumn25->Width = 20;
        // 
        // dataGridViewTextBoxColumn26
        // 
        this->dataGridViewTextBoxColumn26->Frozen = true;
        this->dataGridViewTextBoxColumn26->HeaderText = L"6";
        this->dataGridViewTextBoxColumn26->Name = L"dataGridViewTextBoxColumn26";
        this->dataGridViewTextBoxColumn26->ReadOnly = true;
        this->dataGridViewTextBoxColumn26->Width = 20;
        // 
        // dataGridViewTextBoxColumn27
        // 
        this->dataGridViewTextBoxColumn27->Frozen = true;
        this->dataGridViewTextBoxColumn27->HeaderText = L"5";
        this->dataGridViewTextBoxColumn27->Name = L"dataGridViewTextBoxColumn27";
        this->dataGridViewTextBoxColumn27->ReadOnly = true;
        this->dataGridViewTextBoxColumn27->Width = 20;
        // 
        // dataGridViewTextBoxColumn28
        // 
        this->dataGridViewTextBoxColumn28->Frozen = true;
        this->dataGridViewTextBoxColumn28->HeaderText = L"4";
        this->dataGridViewTextBoxColumn28->Name = L"dataGridViewTextBoxColumn28";
        this->dataGridViewTextBoxColumn28->ReadOnly = true;
        this->dataGridViewTextBoxColumn28->Width = 20;
        // 
        // dataGridViewTextBoxColumn29
        // 
        this->dataGridViewTextBoxColumn29->Frozen = true;
        this->dataGridViewTextBoxColumn29->HeaderText = L"3";
        this->dataGridViewTextBoxColumn29->Name = L"dataGridViewTextBoxColumn29";
        this->dataGridViewTextBoxColumn29->ReadOnly = true;
        this->dataGridViewTextBoxColumn29->Width = 20;
        // 
        // dataGridViewTextBoxColumn30
        // 
        this->dataGridViewTextBoxColumn30->Frozen = true;
        this->dataGridViewTextBoxColumn30->HeaderText = L"2";
        this->dataGridViewTextBoxColumn30->Name = L"dataGridViewTextBoxColumn30";
        this->dataGridViewTextBoxColumn30->ReadOnly = true;
        this->dataGridViewTextBoxColumn30->Width = 20;
        // 
        // dataGridViewTextBoxColumn31
        // 
        this->dataGridViewTextBoxColumn31->Frozen = true;
        this->dataGridViewTextBoxColumn31->HeaderText = L"1";
        this->dataGridViewTextBoxColumn31->Name = L"dataGridViewTextBoxColumn31";
        this->dataGridViewTextBoxColumn31->ReadOnly = true;
        this->dataGridViewTextBoxColumn31->Width = 20;
        // 
        // dataGridViewTextBoxColumn32
        // 
        this->dataGridViewTextBoxColumn32->Frozen = true;
        this->dataGridViewTextBoxColumn32->HeaderText = L"0";
        this->dataGridViewTextBoxColumn32->Name = L"dataGridViewTextBoxColumn32";
        this->dataGridViewTextBoxColumn32->ReadOnly = true;
        this->dataGridViewTextBoxColumn32->Width = 20;
        // 
        // dataGridView_B_src
        // 
        this->dataGridView_B_src->AllowUserToDeleteRows = false;
        this->dataGridView_B_src->AllowUserToResizeColumns = false;
        this->dataGridView_B_src->AllowUserToResizeRows = false;
        dataGridViewCellStyle147->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle147->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle147->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle147->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle147->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle147->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle147->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_B_src->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle147;
        this->dataGridView_B_src->ColumnHeadersHeight = 19;
        this->dataGridView_B_src->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_B_src->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(16) {
            this->dataGridViewTextBoxColumn1,
                this->dataGridViewTextBoxColumn2, this->dataGridViewTextBoxColumn3, this->dataGridViewTextBoxColumn4, this->dataGridViewTextBoxColumn5,
                this->dataGridViewTextBoxColumn6, this->dataGridViewTextBoxColumn7, this->dataGridViewTextBoxColumn8, this->dataGridViewTextBoxColumn9,
                this->dataGridViewTextBoxColumn10, this->dataGridViewTextBoxColumn11, this->dataGridViewTextBoxColumn12, this->dataGridViewTextBoxColumn13,
                this->dataGridViewTextBoxColumn14, this->dataGridViewTextBoxColumn15, this->dataGridViewTextBoxColumn16
        });
        dataGridViewCellStyle148->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle148->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle148->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle148->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle148->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle148->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle148->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_B_src->DefaultCellStyle = dataGridViewCellStyle148;
        this->dataGridView_B_src->Location = System::Drawing::Point(784, 55);
        this->dataGridView_B_src->Name = L"dataGridView_B_src";
        this->dataGridView_B_src->ReadOnly = true;
        this->dataGridView_B_src->RowHeadersVisible = false;
        this->dataGridView_B_src->Size = System::Drawing::Size(321, 43);
        this->dataGridView_B_src->TabIndex = 232;
        this->dataGridView_B_src->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_B_src_CellPainting);
        this->dataGridView_B_src->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &Form1::dataGridView_B_src_ColumnHeaderMouseClick);
        // 
        // dataGridViewTextBoxColumn1
        // 
        this->dataGridViewTextBoxColumn1->Frozen = true;
        this->dataGridViewTextBoxColumn1->HeaderText = L"15";
        this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
        this->dataGridViewTextBoxColumn1->ReadOnly = true;
        this->dataGridViewTextBoxColumn1->Width = 20;
        // 
        // dataGridViewTextBoxColumn2
        // 
        this->dataGridViewTextBoxColumn2->Frozen = true;
        this->dataGridViewTextBoxColumn2->HeaderText = L"14";
        this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
        this->dataGridViewTextBoxColumn2->ReadOnly = true;
        this->dataGridViewTextBoxColumn2->Width = 20;
        // 
        // dataGridViewTextBoxColumn3
        // 
        this->dataGridViewTextBoxColumn3->Frozen = true;
        this->dataGridViewTextBoxColumn3->HeaderText = L"13";
        this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
        this->dataGridViewTextBoxColumn3->ReadOnly = true;
        this->dataGridViewTextBoxColumn3->Width = 20;
        // 
        // dataGridViewTextBoxColumn4
        // 
        this->dataGridViewTextBoxColumn4->Frozen = true;
        this->dataGridViewTextBoxColumn4->HeaderText = L"12";
        this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
        this->dataGridViewTextBoxColumn4->ReadOnly = true;
        this->dataGridViewTextBoxColumn4->Width = 20;
        // 
        // dataGridViewTextBoxColumn5
        // 
        this->dataGridViewTextBoxColumn5->Frozen = true;
        this->dataGridViewTextBoxColumn5->HeaderText = L"11";
        this->dataGridViewTextBoxColumn5->Name = L"dataGridViewTextBoxColumn5";
        this->dataGridViewTextBoxColumn5->ReadOnly = true;
        this->dataGridViewTextBoxColumn5->Width = 20;
        // 
        // dataGridViewTextBoxColumn6
        // 
        this->dataGridViewTextBoxColumn6->Frozen = true;
        this->dataGridViewTextBoxColumn6->HeaderText = L"10";
        this->dataGridViewTextBoxColumn6->Name = L"dataGridViewTextBoxColumn6";
        this->dataGridViewTextBoxColumn6->ReadOnly = true;
        this->dataGridViewTextBoxColumn6->Width = 20;
        // 
        // dataGridViewTextBoxColumn7
        // 
        this->dataGridViewTextBoxColumn7->Frozen = true;
        this->dataGridViewTextBoxColumn7->HeaderText = L"9";
        this->dataGridViewTextBoxColumn7->Name = L"dataGridViewTextBoxColumn7";
        this->dataGridViewTextBoxColumn7->ReadOnly = true;
        this->dataGridViewTextBoxColumn7->Width = 20;
        // 
        // dataGridViewTextBoxColumn8
        // 
        this->dataGridViewTextBoxColumn8->Frozen = true;
        this->dataGridViewTextBoxColumn8->HeaderText = L"8";
        this->dataGridViewTextBoxColumn8->Name = L"dataGridViewTextBoxColumn8";
        this->dataGridViewTextBoxColumn8->ReadOnly = true;
        this->dataGridViewTextBoxColumn8->Width = 20;
        // 
        // dataGridViewTextBoxColumn9
        // 
        this->dataGridViewTextBoxColumn9->Frozen = true;
        this->dataGridViewTextBoxColumn9->HeaderText = L"7";
        this->dataGridViewTextBoxColumn9->Name = L"dataGridViewTextBoxColumn9";
        this->dataGridViewTextBoxColumn9->ReadOnly = true;
        this->dataGridViewTextBoxColumn9->Width = 20;
        // 
        // dataGridViewTextBoxColumn10
        // 
        this->dataGridViewTextBoxColumn10->Frozen = true;
        this->dataGridViewTextBoxColumn10->HeaderText = L"6";
        this->dataGridViewTextBoxColumn10->Name = L"dataGridViewTextBoxColumn10";
        this->dataGridViewTextBoxColumn10->ReadOnly = true;
        this->dataGridViewTextBoxColumn10->Width = 20;
        // 
        // dataGridViewTextBoxColumn11
        // 
        this->dataGridViewTextBoxColumn11->Frozen = true;
        this->dataGridViewTextBoxColumn11->HeaderText = L"5";
        this->dataGridViewTextBoxColumn11->Name = L"dataGridViewTextBoxColumn11";
        this->dataGridViewTextBoxColumn11->ReadOnly = true;
        this->dataGridViewTextBoxColumn11->Width = 20;
        // 
        // dataGridViewTextBoxColumn12
        // 
        this->dataGridViewTextBoxColumn12->Frozen = true;
        this->dataGridViewTextBoxColumn12->HeaderText = L"4";
        this->dataGridViewTextBoxColumn12->Name = L"dataGridViewTextBoxColumn12";
        this->dataGridViewTextBoxColumn12->ReadOnly = true;
        this->dataGridViewTextBoxColumn12->Width = 20;
        // 
        // dataGridViewTextBoxColumn13
        // 
        this->dataGridViewTextBoxColumn13->Frozen = true;
        this->dataGridViewTextBoxColumn13->HeaderText = L"3";
        this->dataGridViewTextBoxColumn13->Name = L"dataGridViewTextBoxColumn13";
        this->dataGridViewTextBoxColumn13->ReadOnly = true;
        this->dataGridViewTextBoxColumn13->Width = 20;
        // 
        // dataGridViewTextBoxColumn14
        // 
        this->dataGridViewTextBoxColumn14->Frozen = true;
        this->dataGridViewTextBoxColumn14->HeaderText = L"2";
        this->dataGridViewTextBoxColumn14->Name = L"dataGridViewTextBoxColumn14";
        this->dataGridViewTextBoxColumn14->ReadOnly = true;
        this->dataGridViewTextBoxColumn14->Width = 20;
        // 
        // dataGridViewTextBoxColumn15
        // 
        this->dataGridViewTextBoxColumn15->Frozen = true;
        this->dataGridViewTextBoxColumn15->HeaderText = L"1";
        this->dataGridViewTextBoxColumn15->Name = L"dataGridViewTextBoxColumn15";
        this->dataGridViewTextBoxColumn15->ReadOnly = true;
        this->dataGridViewTextBoxColumn15->Width = 20;
        // 
        // dataGridViewTextBoxColumn16
        // 
        this->dataGridViewTextBoxColumn16->Frozen = true;
        this->dataGridViewTextBoxColumn16->HeaderText = L"0";
        this->dataGridViewTextBoxColumn16->Name = L"dataGridViewTextBoxColumn16";
        this->dataGridViewTextBoxColumn16->ReadOnly = true;
        this->dataGridViewTextBoxColumn16->Width = 20;
        // 
        // label86
        // 
        this->label86->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label86->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label86->ForeColor = System::Drawing::SystemColors::Window;
        this->label86->Location = System::Drawing::Point(1213, 117);
        this->label86->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label86->Name = L"label86";
        this->label86->Size = System::Drawing::Size(43, 43);
        this->label86->TabIndex = 231;
        this->label86->Text = L"B";
        this->label86->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label85
        // 
        this->label85->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label85->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label85->ForeColor = System::Drawing::SystemColors::Window;
        this->label85->Location = System::Drawing::Point(1110, 117);
        this->label85->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label85->Name = L"label85";
        this->label85->Size = System::Drawing::Size(100, 23);
        this->label85->TabIndex = 230;
        this->label85->Text = L"���������";
        this->label85->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label84
        // 
        this->label84->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label84->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label84->ForeColor = System::Drawing::SystemColors::Window;
        this->label84->Location = System::Drawing::Point(1110, 140);
        this->label84->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label84->Name = L"label84";
        this->label84->Size = System::Drawing::Size(100, 20);
        this->label84->TabIndex = 229;
        this->label84->Text = L"0";
        this->label84->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // dataGridView_A
        // 
        this->dataGridView_A->AllowUserToDeleteRows = false;
        this->dataGridView_A->AllowUserToResizeColumns = false;
        this->dataGridView_A->AllowUserToResizeRows = false;
        dataGridViewCellStyle149->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle149->BackColor = System::Drawing::SystemColors::Control;
        dataGridViewCellStyle149->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        dataGridViewCellStyle149->ForeColor = System::Drawing::SystemColors::WindowText;
        dataGridViewCellStyle149->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle149->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle149->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
        this->dataGridView_A->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle149;
        this->dataGridView_A->ColumnHeadersHeight = 19;
        this->dataGridView_A->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
        this->dataGridView_A->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(16) {
            this->Column1,
                this->Column2, this->Column3, this->Column4, this->Column5, this->Column6, this->Column7, this->Column8, this->Column9, this->Column10,
                this->Column11, this->Column12, this->Column13, this->Column14, this->Column15, this->Column16
        });
        dataGridViewCellStyle150->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
        dataGridViewCellStyle150->BackColor = System::Drawing::SystemColors::Window;
        dataGridViewCellStyle150->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
            System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
        dataGridViewCellStyle150->ForeColor = System::Drawing::SystemColors::ControlText;
        dataGridViewCellStyle150->SelectionBackColor = System::Drawing::SystemColors::Highlight;
        dataGridViewCellStyle150->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
        dataGridViewCellStyle150->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
        this->dataGridView_A->DefaultCellStyle = dataGridViewCellStyle150;
        this->dataGridView_A->Location = System::Drawing::Point(784, 13);
        this->dataGridView_A->Name = L"dataGridView_A";
        this->dataGridView_A->ReadOnly = true;
        this->dataGridView_A->RowHeadersVisible = false;
        this->dataGridView_A->Size = System::Drawing::Size(321, 43);
        this->dataGridView_A->TabIndex = 228;
        this->dataGridView_A->CellPainting += gcnew System::Windows::Forms::DataGridViewCellPaintingEventHandler(this, &Form1::dataGridView_A_CellPainting);
        this->dataGridView_A->ColumnHeaderMouseClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &Form1::dataGridView_A_ColumnHeaderMouseClick);
        // 
        // Column1
        // 
        this->Column1->Frozen = true;
        this->Column1->HeaderText = L"15";
        this->Column1->Name = L"Column1";
        this->Column1->ReadOnly = true;
        this->Column1->Width = 20;
        // 
        // Column2
        // 
        this->Column2->Frozen = true;
        this->Column2->HeaderText = L"14";
        this->Column2->Name = L"Column2";
        this->Column2->ReadOnly = true;
        this->Column2->Width = 20;
        // 
        // Column3
        // 
        this->Column3->Frozen = true;
        this->Column3->HeaderText = L"13";
        this->Column3->Name = L"Column3";
        this->Column3->ReadOnly = true;
        this->Column3->Width = 20;
        // 
        // Column4
        // 
        this->Column4->Frozen = true;
        this->Column4->HeaderText = L"12";
        this->Column4->Name = L"Column4";
        this->Column4->ReadOnly = true;
        this->Column4->Width = 20;
        // 
        // Column5
        // 
        this->Column5->Frozen = true;
        this->Column5->HeaderText = L"11";
        this->Column5->Name = L"Column5";
        this->Column5->ReadOnly = true;
        this->Column5->Width = 20;
        // 
        // Column6
        // 
        this->Column6->Frozen = true;
        this->Column6->HeaderText = L"10";
        this->Column6->Name = L"Column6";
        this->Column6->ReadOnly = true;
        this->Column6->Width = 20;
        // 
        // Column7
        // 
        this->Column7->Frozen = true;
        this->Column7->HeaderText = L"9";
        this->Column7->Name = L"Column7";
        this->Column7->ReadOnly = true;
        this->Column7->Width = 20;
        // 
        // Column8
        // 
        this->Column8->Frozen = true;
        this->Column8->HeaderText = L"8";
        this->Column8->Name = L"Column8";
        this->Column8->ReadOnly = true;
        this->Column8->Width = 20;
        // 
        // Column9
        // 
        this->Column9->Frozen = true;
        this->Column9->HeaderText = L"7";
        this->Column9->Name = L"Column9";
        this->Column9->ReadOnly = true;
        this->Column9->Width = 20;
        // 
        // Column10
        // 
        this->Column10->Frozen = true;
        this->Column10->HeaderText = L"6";
        this->Column10->Name = L"Column10";
        this->Column10->ReadOnly = true;
        this->Column10->Width = 20;
        // 
        // Column11
        // 
        this->Column11->Frozen = true;
        this->Column11->HeaderText = L"5";
        this->Column11->Name = L"Column11";
        this->Column11->ReadOnly = true;
        this->Column11->Width = 20;
        // 
        // Column12
        // 
        this->Column12->Frozen = true;
        this->Column12->HeaderText = L"4";
        this->Column12->Name = L"Column12";
        this->Column12->ReadOnly = true;
        this->Column12->Width = 20;
        // 
        // Column13
        // 
        this->Column13->Frozen = true;
        this->Column13->HeaderText = L"3";
        this->Column13->Name = L"Column13";
        this->Column13->ReadOnly = true;
        this->Column13->Width = 20;
        // 
        // Column14
        // 
        this->Column14->Frozen = true;
        this->Column14->HeaderText = L"2";
        this->Column14->Name = L"Column14";
        this->Column14->ReadOnly = true;
        this->Column14->Width = 20;
        // 
        // Column15
        // 
        this->Column15->Frozen = true;
        this->Column15->HeaderText = L"1";
        this->Column15->Name = L"Column15";
        this->Column15->ReadOnly = true;
        this->Column15->Width = 20;
        // 
        // Column16
        // 
        this->Column16->Frozen = true;
        this->Column16->HeaderText = L"0";
        this->Column16->Name = L"Column16";
        this->Column16->ReadOnly = true;
        this->Column16->Width = 20;
        // 
        // label83
        // 
        this->label83->AutoSize = true;
        this->label83->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label83->Location = System::Drawing::Point(64, 243);
        this->label83->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label83->Name = L"label83";
        this->label83->Size = System::Drawing::Size(34, 12);
        this->label83->TabIndex = 227;
        this->label83->Text = L"label83";
        // 
        // label82
        // 
        this->label82->AutoSize = true;
        this->label82->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label82->Location = System::Drawing::Point(64, 230);
        this->label82->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label82->Name = L"label82";
        this->label82->Size = System::Drawing::Size(34, 12);
        this->label82->TabIndex = 226;
        this->label82->Text = L"label82";
        // 
        // label81
        // 
        this->label81->AutoSize = true;
        this->label81->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label81->Location = System::Drawing::Point(64, 217);
        this->label81->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label81->Name = L"label81";
        this->label81->Size = System::Drawing::Size(34, 12);
        this->label81->TabIndex = 225;
        this->label81->Text = L"label81";
        // 
        // label80
        // 
        this->label80->AutoSize = true;
        this->label80->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label80->Location = System::Drawing::Point(64, 204);
        this->label80->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label80->Name = L"label80";
        this->label80->Size = System::Drawing::Size(34, 12);
        this->label80->TabIndex = 224;
        this->label80->Text = L"label80";
        // 
        // label79
        // 
        this->label79->AutoSize = true;
        this->label79->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label79->Location = System::Drawing::Point(64, 191);
        this->label79->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label79->Name = L"label79";
        this->label79->Size = System::Drawing::Size(34, 12);
        this->label79->TabIndex = 223;
        this->label79->Text = L"label79";
        // 
        // label78
        // 
        this->label78->AutoSize = true;
        this->label78->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label78->Location = System::Drawing::Point(64, 178);
        this->label78->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label78->Name = L"label78";
        this->label78->Size = System::Drawing::Size(34, 12);
        this->label78->TabIndex = 222;
        this->label78->Text = L"label78";
        // 
        // label77
        // 
        this->label77->AutoSize = true;
        this->label77->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label77->Location = System::Drawing::Point(64, 165);
        this->label77->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label77->Name = L"label77";
        this->label77->Size = System::Drawing::Size(34, 12);
        this->label77->TabIndex = 221;
        this->label77->Text = L"label77";
        // 
        // label76
        // 
        this->label76->AutoSize = true;
        this->label76->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label76->Location = System::Drawing::Point(64, 152);
        this->label76->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label76->Name = L"label76";
        this->label76->Size = System::Drawing::Size(34, 12);
        this->label76->TabIndex = 220;
        this->label76->Text = L"label76";
        // 
        // label75
        // 
        this->label75->AutoSize = true;
        this->label75->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label75->Location = System::Drawing::Point(64, 139);
        this->label75->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label75->Name = L"label75";
        this->label75->Size = System::Drawing::Size(34, 12);
        this->label75->TabIndex = 219;
        this->label75->Text = L"label75";
        // 
        // label74
        // 
        this->label74->AutoSize = true;
        this->label74->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label74->Location = System::Drawing::Point(64, 126);
        this->label74->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label74->Name = L"label74";
        this->label74->Size = System::Drawing::Size(34, 12);
        this->label74->TabIndex = 218;
        this->label74->Text = L"label74";
        // 
        // label73
        // 
        this->label73->AutoSize = true;
        this->label73->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label73->Location = System::Drawing::Point(64, 113);
        this->label73->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label73->Name = L"label73";
        this->label73->Size = System::Drawing::Size(34, 12);
        this->label73->TabIndex = 217;
        this->label73->Text = L"label73";
        // 
        // label72
        // 
        this->label72->AutoSize = true;
        this->label72->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label72->Location = System::Drawing::Point(64, 100);
        this->label72->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label72->Name = L"label72";
        this->label72->Size = System::Drawing::Size(34, 12);
        this->label72->TabIndex = 216;
        this->label72->Text = L"label72";
        // 
        // label71
        // 
        this->label71->AutoSize = true;
        this->label71->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label71->Location = System::Drawing::Point(64, 87);
        this->label71->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label71->Name = L"label71";
        this->label71->Size = System::Drawing::Size(34, 12);
        this->label71->TabIndex = 215;
        this->label71->Text = L"label71";
        // 
        // label70
        // 
        this->label70->AutoSize = true;
        this->label70->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label70->Location = System::Drawing::Point(64, 74);
        this->label70->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label70->Name = L"label70";
        this->label70->Size = System::Drawing::Size(34, 12);
        this->label70->TabIndex = 214;
        this->label70->Text = L"label70";
        // 
        // label69
        // 
        this->label69->AutoSize = true;
        this->label69->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label69->Location = System::Drawing::Point(64, 61);
        this->label69->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label69->Name = L"label69";
        this->label69->Size = System::Drawing::Size(34, 12);
        this->label69->TabIndex = 213;
        this->label69->Text = L"label69";
        // 
        // label68
        // 
        this->label68->AutoSize = true;
        this->label68->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label68->Location = System::Drawing::Point(64, 48);
        this->label68->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label68->Name = L"label68";
        this->label68->Size = System::Drawing::Size(34, 12);
        this->label68->TabIndex = 212;
        this->label68->Text = L"label68";
        // 
        // label67
        // 
        this->label67->AutoSize = true;
        this->label67->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label67->Location = System::Drawing::Point(64, 35);
        this->label67->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label67->Name = L"label67";
        this->label67->Size = System::Drawing::Size(34, 12);
        this->label67->TabIndex = 211;
        this->label67->Text = L"label67";
        // 
        // label66
        // 
        this->label66->AutoSize = true;
        this->label66->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label66->Location = System::Drawing::Point(64, 22);
        this->label66->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label66->Name = L"label66";
        this->label66->Size = System::Drawing::Size(34, 12);
        this->label66->TabIndex = 210;
        this->label66->Text = L"label66";
        // 
        // label65
        // 
        this->label65->AutoSize = true;
        this->label65->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label65->Location = System::Drawing::Point(64, 9);
        this->label65->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label65->Name = L"label65";
        this->label65->Size = System::Drawing::Size(34, 12);
        this->label65->TabIndex = 209;
        this->label65->Text = L"label65";
        // 
        // label64
        // 
        this->label64->AutoSize = true;
        this->label64->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label64->Location = System::Drawing::Point(64, -4);
        this->label64->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label64->Name = L"label64";
        this->label64->Size = System::Drawing::Size(34, 12);
        this->label64->TabIndex = 208;
        this->label64->Text = L"label64";
        // 
        // label63
        // 
        this->label63->AutoSize = true;
        this->label63->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label63->Location = System::Drawing::Point(348, 142);
        this->label63->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label63->Name = L"label63";
        this->label63->Size = System::Drawing::Size(34, 12);
        this->label63->TabIndex = 207;
        this->label63->Text = L"label63";
        // 
        // label62
        // 
        this->label62->AutoSize = true;
        this->label62->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label62->Location = System::Drawing::Point(348, 130);
        this->label62->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label62->Name = L"label62";
        this->label62->Size = System::Drawing::Size(34, 12);
        this->label62->TabIndex = 206;
        this->label62->Text = L"label62";
        // 
        // label61
        // 
        this->label61->AutoSize = true;
        this->label61->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label61->Location = System::Drawing::Point(348, 117);
        this->label61->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label61->Name = L"label61";
        this->label61->Size = System::Drawing::Size(34, 12);
        this->label61->TabIndex = 205;
        this->label61->Text = L"label61";
        // 
        // label60
        // 
        this->label60->AutoSize = true;
        this->label60->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label60->Location = System::Drawing::Point(348, 104);
        this->label60->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label60->Name = L"label60";
        this->label60->Size = System::Drawing::Size(34, 12);
        this->label60->TabIndex = 204;
        this->label60->Text = L"label60";
        // 
        // label59
        // 
        this->label59->AutoSize = true;
        this->label59->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label59->Location = System::Drawing::Point(348, 91);
        this->label59->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label59->Name = L"label59";
        this->label59->Size = System::Drawing::Size(34, 12);
        this->label59->TabIndex = 203;
        this->label59->Text = L"label59";
        // 
        // label58
        // 
        this->label58->AutoSize = true;
        this->label58->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label58->Location = System::Drawing::Point(348, 78);
        this->label58->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label58->Name = L"label58";
        this->label58->Size = System::Drawing::Size(34, 12);
        this->label58->TabIndex = 202;
        this->label58->Text = L"label58";
        // 
        // label57
        // 
        this->label57->AutoSize = true;
        this->label57->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label57->Location = System::Drawing::Point(348, 65);
        this->label57->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label57->Name = L"label57";
        this->label57->Size = System::Drawing::Size(34, 12);
        this->label57->TabIndex = 201;
        this->label57->Text = L"label57";
        // 
        // label56
        // 
        this->label56->AutoSize = true;
        this->label56->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label56->Location = System::Drawing::Point(348, 52);
        this->label56->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label56->Name = L"label56";
        this->label56->Size = System::Drawing::Size(34, 12);
        this->label56->TabIndex = 200;
        this->label56->Text = L"label56";
        // 
        // label55
        // 
        this->label55->AutoSize = true;
        this->label55->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label55->Location = System::Drawing::Point(348, 39);
        this->label55->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label55->Name = L"label55";
        this->label55->Size = System::Drawing::Size(34, 12);
        this->label55->TabIndex = 199;
        this->label55->Text = L"label55";
        // 
        // label54
        // 
        this->label54->AutoSize = true;
        this->label54->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label54->Location = System::Drawing::Point(348, 26);
        this->label54->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label54->Name = L"label54";
        this->label54->Size = System::Drawing::Size(34, 12);
        this->label54->TabIndex = 198;
        this->label54->Text = L"label54";
        // 
        // label53
        // 
        this->label53->AutoSize = true;
        this->label53->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label53->Location = System::Drawing::Point(348, 13);
        this->label53->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label53->Name = L"label53";
        this->label53->Size = System::Drawing::Size(34, 12);
        this->label53->TabIndex = 197;
        this->label53->Text = L"label53";
        // 
        // label52
        // 
        this->label52->AutoSize = true;
        this->label52->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label52->Location = System::Drawing::Point(348, 0);
        this->label52->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label52->Name = L"label52";
        this->label52->Size = System::Drawing::Size(34, 12);
        this->label52->TabIndex = 196;
        this->label52->Text = L"label52";
        // 
        // label51
        // 
        this->label51->AutoSize = true;
        this->label51->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label51->Location = System::Drawing::Point(253, 116);
        this->label51->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label51->Name = L"label51";
        this->label51->Size = System::Drawing::Size(77, 24);
        this->label51->TabIndex = 195;
        this->label51->Text = L"label51";
        this->label51->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label50
        // 
        this->label50->AutoSize = true;
        this->label50->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label50->Location = System::Drawing::Point(253, 92);
        this->label50->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label50->Name = L"label50";
        this->label50->Size = System::Drawing::Size(77, 24);
        this->label50->TabIndex = 194;
        this->label50->Text = L"label50";
        this->label50->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label49
        // 
        this->label49->AutoSize = true;
        this->label49->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label49->Location = System::Drawing::Point(253, 68);
        this->label49->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label49->Name = L"label49";
        this->label49->Size = System::Drawing::Size(77, 24);
        this->label49->TabIndex = 193;
        this->label49->Text = L"label49";
        this->label49->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label48
        // 
        this->label48->AutoSize = true;
        this->label48->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label48->Location = System::Drawing::Point(253, 44);
        this->label48->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label48->Name = L"label48";
        this->label48->Size = System::Drawing::Size(77, 24);
        this->label48->TabIndex = 192;
        this->label48->Text = L"label48";
        this->label48->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label47
        // 
        this->label47->AutoSize = true;
        this->label47->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label47->Location = System::Drawing::Point(253, 20);
        this->label47->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label47->Name = L"label47";
        this->label47->Size = System::Drawing::Size(77, 24);
        this->label47->TabIndex = 191;
        this->label47->Text = L"label47";
        this->label47->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label46
        // 
        this->label46->AutoSize = true;
        this->label46->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label46->Location = System::Drawing::Point(253, -4);
        this->label46->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label46->Name = L"label46";
        this->label46->Size = System::Drawing::Size(77, 24);
        this->label46->TabIndex = 190;
        this->label46->Text = L"label46";
        this->label46->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label45
        // 
        this->label45->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label45->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label45->ForeColor = System::Drawing::SystemColors::Window;
        this->label45->Location = System::Drawing::Point(1110, 363);
        this->label45->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label45->Name = L"label45";
        this->label45->Size = System::Drawing::Size(100, 20);
        this->label45->TabIndex = 189;
        this->label45->Text = L"0";
        this->label45->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // label44
        // 
        this->label44->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label44->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label44->ForeColor = System::Drawing::SystemColors::Window;
        this->label44->Location = System::Drawing::Point(1110, 322);
        this->label44->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label44->Name = L"label44";
        this->label44->Size = System::Drawing::Size(100, 20);
        this->label44->TabIndex = 188;
        this->label44->Text = L"0";
        this->label44->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // label43
        // 
        this->label43->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label43->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label43->ForeColor = System::Drawing::SystemColors::Window;
        this->label43->Location = System::Drawing::Point(1110, 280);
        this->label43->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label43->Name = L"label43";
        this->label43->Size = System::Drawing::Size(100, 20);
        this->label43->TabIndex = 187;
        this->label43->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label42
        // 
        this->label42->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label42->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label42->ForeColor = System::Drawing::SystemColors::Window;
        this->label42->Location = System::Drawing::Point(1110, 340);
        this->label42->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label42->Name = L"label42";
        this->label42->Size = System::Drawing::Size(100, 23);
        this->label42->TabIndex = 186;
        this->label42->Text = L"���������";
        this->label42->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label41
        // 
        this->label41->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label41->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label41->ForeColor = System::Drawing::SystemColors::Window;
        this->label41->Location = System::Drawing::Point(1110, 299);
        this->label41->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label41->Name = L"label41";
        this->label41->Size = System::Drawing::Size(100, 23);
        this->label41->TabIndex = 185;
        this->label41->Text = L"�������";
        this->label41->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label40
        // 
        this->label40->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label40->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label40->ForeColor = System::Drawing::SystemColors::Window;
        this->label40->Location = System::Drawing::Point(1214, 340);
        this->label40->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label40->Name = L"label40";
        this->label40->Size = System::Drawing::Size(43, 43);
        this->label40->TabIndex = 184;
        this->label40->Text = L"a";
        this->label40->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label39
        // 
        this->label39->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label39->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label39->ForeColor = System::Drawing::SystemColors::Window;
        this->label39->Location = System::Drawing::Point(1214, 299);
        this->label39->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label39->Name = L"label39";
        this->label39->Size = System::Drawing::Size(43, 43);
        this->label39->TabIndex = 183;
        this->label39->Text = L"��";
        this->label39->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label38
        // 
        this->label38->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label38->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label38->ForeColor = System::Drawing::SystemColors::Window;
        this->label38->Location = System::Drawing::Point(1110, 257);
        this->label38->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label38->Name = L"label38";
        this->label38->Size = System::Drawing::Size(100, 23);
        this->label38->TabIndex = 182;
        this->label38->Text = L"���� ���������";
        this->label38->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label37
        // 
        this->label37->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label37->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label37->ForeColor = System::Drawing::SystemColors::Window;
        this->label37->Location = System::Drawing::Point(1214, 257);
        this->label37->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label37->Name = L"label37";
        this->label37->Size = System::Drawing::Size(43, 43);
        this->label37->TabIndex = 181;
        this->label37->Text = L"D";
        this->label37->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label36
        // 
        this->label36->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label36->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label36->ForeColor = System::Drawing::SystemColors::Window;
        this->label36->Location = System::Drawing::Point(1213, 159);
        this->label36->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label36->Name = L"label36";
        this->label36->Size = System::Drawing::Size(43, 43);
        this->label36->TabIndex = 180;
        this->label36->Text = L"C";
        this->label36->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label35
        // 
        this->label35->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label35->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label35->ForeColor = System::Drawing::SystemColors::Window;
        this->label35->Location = System::Drawing::Point(1213, 55);
        this->label35->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label35->Name = L"label35";
        this->label35->Size = System::Drawing::Size(43, 43);
        this->label35->TabIndex = 179;
        this->label35->Text = L"B";
        this->label35->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label34
        // 
        this->label34->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label34->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->label34->ForeColor = System::Drawing::SystemColors::Window;
        this->label34->Location = System::Drawing::Point(1213, 13);
        this->label34->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label34->Name = L"label34";
        this->label34->Size = System::Drawing::Size(43, 43);
        this->label34->TabIndex = 178;
        this->label34->Text = L"A";
        this->label34->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label33
        // 
        this->label33->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label33->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label33->ForeColor = System::Drawing::SystemColors::Window;
        this->label33->Location = System::Drawing::Point(1110, 13);
        this->label33->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label33->Name = L"label33";
        this->label33->Size = System::Drawing::Size(100, 23);
        this->label33->TabIndex = 177;
        this->label33->Text = L"��������";
        this->label33->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label32
        // 
        this->label32->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label32->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label32->ForeColor = System::Drawing::SystemColors::Window;
        this->label32->Location = System::Drawing::Point(1110, 55);
        this->label32->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label32->Name = L"label32";
        this->label32->Size = System::Drawing::Size(100, 23);
        this->label32->TabIndex = 176;
        this->label32->Text = L"���������";
        this->label32->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label31
        // 
        this->label31->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label31->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label31->ForeColor = System::Drawing::SystemColors::Window;
        this->label31->Location = System::Drawing::Point(1110, 159);
        this->label31->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label31->Name = L"label31";
        this->label31->Size = System::Drawing::Size(100, 23);
        this->label31->TabIndex = 175;
        this->label31->Text = L"������������";
        this->label31->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // button3
        // 
        this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(204)));
        this->button3->Location = System::Drawing::Point(587, 315);
        this->button3->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
        this->button3->Name = L"button3";
        this->button3->Size = System::Drawing::Size(160, 23);
        this->button3->TabIndex = 174;
        this->button3->Text = L"�����";
        this->button3->UseVisualStyleBackColor = true;
        this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
        // 
        // numericUpDown2
        // 
        this->numericUpDown2->Location = System::Drawing::Point(752, 322);
        this->numericUpDown2->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
        this->numericUpDown2->Name = L"numericUpDown2";
        this->numericUpDown2->Size = System::Drawing::Size(100, 20);
        this->numericUpDown2->TabIndex = 173;
        this->numericUpDown2->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
        this->numericUpDown2->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 2, 0, 0, 0 });
        this->numericUpDown2->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown2_ValueChanged);
        // 
        // label30
        // 
        this->label30->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label30->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label30->ForeColor = System::Drawing::SystemColors::Window;
        this->label30->Location = System::Drawing::Point(752, 299);
        this->label30->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label30->Name = L"label30";
        this->label30->Size = System::Drawing::Size(100, 20);
        this->label30->TabIndex = 172;
        this->label30->Text = L"�����";
        this->label30->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // numericUpDown1
        // 
        this->numericUpDown1->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, 0 });
        this->numericUpDown1->Location = System::Drawing::Point(752, 280);
        this->numericUpDown1->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 2000, 0, 0, 0 });
        this->numericUpDown1->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, 0 });
        this->numericUpDown1->Name = L"numericUpDown1";
        this->numericUpDown1->Size = System::Drawing::Size(100, 20);
        this->numericUpDown1->TabIndex = 171;
        this->numericUpDown1->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
        this->numericUpDown1->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 300, 0, 0, 0 });
        this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &Form1::numericUpDown1_ValueChanged);
        // 
        // label29
        // 
        this->label29->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label29->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label29->ForeColor = System::Drawing::SystemColors::Window;
        this->label29->Location = System::Drawing::Point(752, 257);
        this->label29->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label29->Name = L"label29";
        this->label29->Size = System::Drawing::Size(100, 20);
        this->label29->TabIndex = 170;
        this->label29->Text = L"����� ���� (��)";
        this->label29->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
        // 
        // label28
        // 
        this->label28->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
        this->label28->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
            static_cast<System::Byte>(0)));
        this->label28->ForeColor = System::Drawing::SystemColors::Window;
        this->label28->Location = System::Drawing::Point(1110, 182);
        this->label28->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label28->Name = L"label28";
        this->label28->Size = System::Drawing::Size(100, 20);
        this->label28->TabIndex = 169;
        this->label28->Text = L"0";
        this->label28->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
        // 
        // label27
        // 
        this->label27->AutoSize = true;
        this->label27->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label27->Location = System::Drawing::Point(125, 61);
        this->label27->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label27->Name = L"label27";
        this->label27->Size = System::Drawing::Size(41, 12);
        this->label27->TabIndex = 168;
        this->label27->Text = L"label27";
        // 
        // label26
        // 
        this->label26->AutoSize = true;
        this->label26->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label26->Location = System::Drawing::Point(125, 48);
        this->label26->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label26->Name = L"label26";
        this->label26->Size = System::Drawing::Size(41, 12);
        this->label26->TabIndex = 167;
        this->label26->Text = L"label26";
        // 
        // label25
        // 
        this->label25->AutoSize = true;
        this->label25->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label25->Location = System::Drawing::Point(125, 35);
        this->label25->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label25->Name = L"label25";
        this->label25->Size = System::Drawing::Size(41, 12);
        this->label25->TabIndex = 166;
        this->label25->Text = L"label25";
        // 
        // label24
        // 
        this->label24->AutoSize = true;
        this->label24->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label24->Location = System::Drawing::Point(125, 22);
        this->label24->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label24->Name = L"label24";
        this->label24->Size = System::Drawing::Size(41, 12);
        this->label24->TabIndex = 165;
        this->label24->Text = L"label24";
        // 
        // label23
        // 
        this->label23->AutoSize = true;
        this->label23->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label23->Location = System::Drawing::Point(125, 9);
        this->label23->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label23->Name = L"label23";
        this->label23->Size = System::Drawing::Size(41, 12);
        this->label23->TabIndex = 164;
        this->label23->Text = L"label23";
        // 
        // label22
        // 
        this->label22->AutoSize = true;
        this->label22->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F, System::Drawing::FontStyle::Bold));
        this->label22->Location = System::Drawing::Point(125, -4);
        this->label22->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label22->Name = L"label22";
        this->label22->Size = System::Drawing::Size(41, 12);
        this->label22->TabIndex = 163;
        this->label22->Text = L"label22";
        // 
        // label21
        // 
        this->label21->AutoSize = true;
        this->label21->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label21->Location = System::Drawing::Point(1, 257);
        this->label21->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label21->Name = L"label21";
        this->label21->Size = System::Drawing::Size(34, 12);
        this->label21->TabIndex = 162;
        this->label21->Text = L"label21";
        this->label21->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label20
        // 
        this->label20->AutoSize = true;
        this->label20->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label20->Location = System::Drawing::Point(1, 244);
        this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label20->Name = L"label20";
        this->label20->Size = System::Drawing::Size(34, 12);
        this->label20->TabIndex = 161;
        this->label20->Text = L"label20";
        this->label20->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label19
        // 
        this->label19->AutoSize = true;
        this->label19->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label19->Location = System::Drawing::Point(1, 231);
        this->label19->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label19->Name = L"label19";
        this->label19->Size = System::Drawing::Size(34, 12);
        this->label19->TabIndex = 160;
        this->label19->Text = L"label19";
        this->label19->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label18
        // 
        this->label18->AutoSize = true;
        this->label18->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label18->Location = System::Drawing::Point(1, 218);
        this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label18->Name = L"label18";
        this->label18->Size = System::Drawing::Size(34, 12);
        this->label18->TabIndex = 159;
        this->label18->Text = L"label18";
        this->label18->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label17
        // 
        this->label17->AutoSize = true;
        this->label17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label17->Location = System::Drawing::Point(1, 205);
        this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label17->Name = L"label17";
        this->label17->Size = System::Drawing::Size(34, 12);
        this->label17->TabIndex = 158;
        this->label17->Text = L"label17";
        this->label17->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label16
        // 
        this->label16->AutoSize = true;
        this->label16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label16->Location = System::Drawing::Point(1, 192);
        this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label16->Name = L"label16";
        this->label16->Size = System::Drawing::Size(34, 12);
        this->label16->TabIndex = 157;
        this->label16->Text = L"label16";
        this->label16->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label15
        // 
        this->label15->AutoSize = true;
        this->label15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label15->Location = System::Drawing::Point(1, 179);
        this->label15->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label15->Name = L"label15";
        this->label15->Size = System::Drawing::Size(34, 12);
        this->label15->TabIndex = 156;
        this->label15->Text = L"label15";
        this->label15->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label14
        // 
        this->label14->AutoSize = true;
        this->label14->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label14->Location = System::Drawing::Point(1, 166);
        this->label14->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label14->Name = L"label14";
        this->label14->Size = System::Drawing::Size(34, 12);
        this->label14->TabIndex = 155;
        this->label14->Text = L"label14";
        this->label14->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label13
        // 
        this->label13->AutoSize = true;
        this->label13->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label13->Location = System::Drawing::Point(1, 153);
        this->label13->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label13->Name = L"label13";
        this->label13->Size = System::Drawing::Size(34, 12);
        this->label13->TabIndex = 154;
        this->label13->Text = L"label13";
        this->label13->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label12
        // 
        this->label12->AutoSize = true;
        this->label12->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label12->Location = System::Drawing::Point(1, 140);
        this->label12->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label12->Name = L"label12";
        this->label12->Size = System::Drawing::Size(34, 12);
        this->label12->TabIndex = 153;
        this->label12->Text = L"label12";
        this->label12->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label11
        // 
        this->label11->AutoSize = true;
        this->label11->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label11->Location = System::Drawing::Point(1, 127);
        this->label11->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label11->Name = L"label11";
        this->label11->Size = System::Drawing::Size(34, 12);
        this->label11->TabIndex = 152;
        this->label11->Text = L"label11";
        this->label11->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label10
        // 
        this->label10->AutoSize = true;
        this->label10->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label10->Location = System::Drawing::Point(1, 114);
        this->label10->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label10->Name = L"label10";
        this->label10->Size = System::Drawing::Size(34, 12);
        this->label10->TabIndex = 151;
        this->label10->Text = L"label10";
        this->label10->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label9
        // 
        this->label9->AutoSize = true;
        this->label9->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label9->Location = System::Drawing::Point(1, 101);
        this->label9->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label9->Name = L"label9";
        this->label9->Size = System::Drawing::Size(29, 12);
        this->label9->TabIndex = 150;
        this->label9->Text = L"label9";
        this->label9->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label8
        // 
        this->label8->AutoSize = true;
        this->label8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label8->Location = System::Drawing::Point(1, 88);
        this->label8->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label8->Name = L"label8";
        this->label8->Size = System::Drawing::Size(29, 12);
        this->label8->TabIndex = 149;
        this->label8->Text = L"label8";
        this->label8->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label7
        // 
        this->label7->AutoSize = true;
        this->label7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label7->Location = System::Drawing::Point(1, 75);
        this->label7->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label7->Name = L"label7";
        this->label7->Size = System::Drawing::Size(29, 12);
        this->label7->TabIndex = 148;
        this->label7->Text = L"label7";
        this->label7->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label6
        // 
        this->label6->AutoSize = true;
        this->label6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label6->Location = System::Drawing::Point(1, 62);
        this->label6->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label6->Name = L"label6";
        this->label6->Size = System::Drawing::Size(29, 12);
        this->label6->TabIndex = 147;
        this->label6->Text = L"label6";
        this->label6->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label5
        // 
        this->label5->AutoSize = true;
        this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label5->Location = System::Drawing::Point(1, 49);
        this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label5->Name = L"label5";
        this->label5->Size = System::Drawing::Size(29, 12);
        this->label5->TabIndex = 146;
        this->label5->Text = L"label5";
        this->label5->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label4
        // 
        this->label4->AutoSize = true;
        this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label4->Location = System::Drawing::Point(1, 36);
        this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label4->Name = L"label4";
        this->label4->Size = System::Drawing::Size(29, 12);
        this->label4->TabIndex = 145;
        this->label4->Text = L"label4";
        this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label3
        // 
        this->label3->AutoSize = true;
        this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label3->Location = System::Drawing::Point(1, 23);
        this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label3->Name = L"label3";
        this->label3->Size = System::Drawing::Size(29, 12);
        this->label3->TabIndex = 144;
        this->label3->Text = L"label3";
        this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label2
        // 
        this->label2->AutoSize = true;
        this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label2->Location = System::Drawing::Point(1, 10);
        this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label2->Name = L"label2";
        this->label2->Size = System::Drawing::Size(29, 12);
        this->label2->TabIndex = 143;
        this->label2->Text = L"label2";
        this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // label1
        // 
        this->label1->AutoSize = true;
        this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.75F));
        this->label1->Location = System::Drawing::Point(1, -3);
        this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
        this->label1->Name = L"label1";
        this->label1->Size = System::Drawing::Size(29, 12);
        this->label1->TabIndex = 142;
        this->label1->Text = L"label1";
        this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
        // 
        // Form1
        // 
        this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
        this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
        this->AutoScroll = true;
        this->BackColor = System::Drawing::SystemColors::HotTrack;
        this->ClientSize = System::Drawing::Size(1272, 960);
        this->Controls->Add(this->panel1);
        this->Margin = System::Windows::Forms::Padding(2, 3, 2, 3);
        this->Name = L"Form1";
        this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
        this->Text = L"������ �������������� ����������";
        this->Shown += gcnew System::EventHandler(this, &Form1::Form1_Shown);
        this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::Form1_Paint);
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->errorProvider1))->EndInit();
        this->panel1->ResumeLayout(false);
        this->panel1->PerformLayout();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_C))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_X_save))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_X_common))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_D_vector))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_Y_vector))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_mem_conditions))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_X_vector))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_unitary_code))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_mem_states))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_state))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_counter))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_D))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_B_run))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_B_src))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView_A))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
        (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
        this->ResumeLayout(false);

    }
#pragma endregion

// ======================================================================================================================================================================

    System::Void Form1::checkBox1_CheckedChanged   (System::Object^ sender, System::EventArgs^ e)   { checkBox1->Checked ? switch_GSA(true) : switch_GSA(false); }
    System::Void Form1::checkBox2_CheckedChanged   (System::Object^ sender, System::EventArgs^ e)   { change_management = true; }
    System::Void Form1::checkBox3_CheckedChanged   (System::Object^ sender, System::EventArgs^ e)   { change_management = true; }
    System::Void Form1::numericUpDown1_ValueChanged(System::Object^ sender, System::EventArgs^ e)   { change_management = true; }
    System::Void Form1::numericUpDown2_ValueChanged(System::Object^ sender, System::EventArgs^ e)   { change_management = true; }

    void Form1::calculate_MP()
    {
        need_update_MP   = false;

        mp_snapshots   .Clear();
        mp_tact_borders.Clear();

        MP_Snapshot snapshot;
        snapshot.A       = convert_to_register(dataGridView_A);
        snapshot.B       = convert_to_register(dataGridView_B_src);
        snapshot.C       = 0;
        snapshot.D       = 0;
        snapshot.counter = 0;
        snapshot.state   = 0;

        Micro_Programm micro_program;
        micro_program.init(snapshot);

        const int BEGIN = 0;
        const int END   = 6;
        int state = -1;
        while((state = micro_program.tact()) != END && state != BEGIN)      mp_tact_borders.Add(micro_program.snapshots.Count - 1);
        mp_tact_borders.Add(micro_program.snapshots.Count - 1);

        int num_mp_snapshots = micro_program.snapshots.Count;
        for(int s = 0; s < num_mp_snapshots; ++ s)
            mp_snapshots.Add(micro_program.snapshots[s]);
    }

    void Form1::calculate_chip()
    {
        need_update_chip = false;

        chip_mp_snapshots .Clear();
        chip_snapshots   ->Clear();

        MP_Snapshot snapshot;
        snapshot.A       = convert_to_register(dataGridView_A);
        snapshot.B       = convert_to_register(dataGridView_B_src);
        snapshot.C       = 0;
        snapshot.D       = 0;
        snapshot.counter = 0;
        snapshot.state   = 0;
        Interaction_CA_AM interaction_ca_am;
        interaction_ca_am.init(snapshot);
        snapshot.B       = 0;
        chip_mp_snapshots.Add(snapshot);

        Chip_Snapshot zero_snapshot;
        zero_snapshot.mem_states     = 0;
        zero_snapshot.unitary_code   = 0;
        zero_snapshot.X_vector       = 0;
        zero_snapshot.mem_conditions = 0;
        zero_snapshot.Y_vector       = 0;
        zero_snapshot.D_vector       = 0;
        chip_snapshots->Add(zero_snapshot);

        const int END = 0;
        int state = -1;
        while((state = interaction_ca_am.tact()) != END)
        {
            chip_mp_snapshots .Add(interaction_ca_am.micro_program.dat);
            chip_snapshots   ->Add(interaction_ca_am.dat);
        }
        chip_mp_snapshots .Add(interaction_ca_am.micro_program.dat);
        chip_snapshots   ->Add(interaction_ca_am.dat);

        chip_tact_borders.Clear();
        int last_snapshot = chip_snapshots->Count - 1;
        for(int i = 1; i <= last_snapshot; ++ i)            chip_tact_borders.Add(i * 6);
    }

    void Form1::mp_work_draw(int left, int right)
    {
        int l = 0,
            num_lines = lines.Count;
        bool paint_line = false;

        for(int id = left; id <= right; ++ id)
        {
            elements[mp_snapshots[id].type]$[mp_snapshots[id].id]->set_color(work_color);

            if(paint_line)
            {
                for(l = 0; l < num_lines; ++ l)
                    if(lines[l]->is_connect_elements(mp_snapshots[id], mp_snapshots[id - 1]))
                        lines[l]->set_color(work_color);
            }
            else    paint_line = true;
        }
    }

    // ��������������
    System::Void Form1::button6_Click(System::Object^ sender, System::EventArgs^ e)
    {
        int num_steps = (int)numericUpDown2->Value;

        if(mp_end
        &&!num_steps
        && checkBox2->Checked)
        {
            mp_end = false;

            mp_right_border = mp_tact_right_border = -1;

            mp_zero_registers();
            mp_reset_color   ();

            run = STOP;

            return;
        }

        if(need_update_MP)
        {
            need_update_MP = false;
            calculate_MP();
        }

        run               = MICROPROGRAM;
        time_step_counter = (int)numericUpDown1->Value;
    }

    // �������������� �� � ��
    System::Void Form1::button7_Click(System::Object^ sender, System::EventArgs^ e)
    {
        int num_steps = (int)numericUpDown2->Value;

        if(chip_end
        &&!num_steps
        && checkBox2->Checked)
        {
            chip_end = false;

            chip_right_border = chip_tact_right_border = -1;

            mp_zero_registers();
            chip_zero_registers();
            chip_reset_color ();

            run = STOP;

            return;
        }

        if(need_update_chip)
        {
            need_update_chip = false;
            calculate_chip();
        }

        run               = CHIP;
        time_step_counter = (int)numericUpDown1->Value;
    }

    System::Void Form1::timer1_Tick(System::Object^  sender, System::EventArgs^  e)
    {
        int  time_step = (int)numericUpDown1->Value  ;
        int  num_steps = (int)numericUpDown2->Value  ;
        bool tacts     =     !checkBox2     ->Checked;

        if(checkBox3->Checked && time_step)
            time_step_counter += timer1->Interval;

        if( time_step_counter >= time_step
        ||  change_management)
        {
            time_step_counter = 0;

            mp_reset_color  ();
            chip_reset_color();

            mp_zero_registers  ();
            chip_zero_registers();

            // mp_end   = false;
            // chip_end = false;

            switch(run)
            {
                case STOP:
                {
                    mp_end   = false;
                    chip_end = false;
                    break;
                }
                case MICROPROGRAM:
                {
                    int last_snapshot = mp_snapshots.Count - 1;

                    if(num_steps)
                    {
                        int left_border  = 0;
                        int right_border = 0;

                        if(!tacts)
                        {
                            if(!change_management)             mp_tact_right_border = ++ mp_right_border;

                            left_border  = (mp_right_border - num_steps + 1 < 0) ? 0             : (mp_right_border - num_steps + 1);
                            right_border = (mp_right_border > last_snapshot)     ? last_snapshot : mp_right_border;

                            if(left_border <= last_snapshot)
                            {
                                mp_work_draw(left_border,      right_border);
                                mp_show_registers(mp_snapshots[right_border]);
                            }
                        }
                        else
                        {
                            int tact_left  = 0;
                            int tact_right = 0;
                            bool add = false;
                            int last_border = mp_tact_borders.Count - 1;
                            bool found = false;

                            if(mp_tact_right_border - num_steps > mp_right_border
                            &&    mp_right_border               < mp_tact_borders[last_border])
                            {
                                add = true;
                                mp_tact_right_border = ++ mp_right_border;
                                
                            }

                            for(int b = 0; b <= last_border; ++ b)
                                if(mp_right_border <= mp_tact_borders[b])
                                {
                                    if(b > 0)       tact_left = mp_tact_borders[b - 1];
                                    tact_right                = mp_tact_borders[b    ];
                                    found = true;
                                    break;
                                }

                            if(!found)
                            {
                                if(last_border - 1 > 0)     tact_left  = mp_tact_borders[last_border - 1];
                                tact_right = mp_tact_borders[last_border    ];
                            }

                            if(!change_management
                            && !add)
                            {
                                mp_tact_right_border ++;
                                if(mp_right_border  <  tact_right
                                || tact_right       == mp_tact_borders[last_border])               mp_right_border ++;
                            }

                            left_border = ((mp_tact_right_border - num_steps + 1) < 0) ? 0 : (mp_tact_right_border - num_steps + 1);
                            if(left_border < tact_left)         left_border  = tact_left;

                            if(!change_management
                            && tact_right      < mp_tact_borders[last_border]
                            && left_border - 1 > mp_right_border)
                            {
                                mp_work_draw(mp_right_border  , mp_right_border );
                                mp_show_registers(mp_snapshots [mp_right_border]);
                            }
                            else if(left_border <= last_snapshot)
                            {
                                right_border = (mp_right_border > last_snapshot) ? last_snapshot : mp_right_border;
                                mp_work_draw(left_border,      right_border);
                                mp_show_registers(mp_snapshots[right_border]);
                            }
                        }

                        if(!change_management
                        && left_border > last_snapshot)
                        {
                            mp_right_border = mp_tact_right_border = -1;

                            run = STOP;
                        }
                    }
                    else
                    {
                        if(checkBox2->Checked)
                        {
                            mp_work_draw     (0,           last_snapshot );
                            mp_show_registers(mp_snapshots[last_snapshot]);

                            mp_end = true;

                            if(!change_management)
                            {
                                mp_right_border = mp_tact_right_border = -1;

                                run = STOP;
                            }
                        }
                        else
                        {
                            int left_border  = 0;
                            int right_border = 0;
                            int b = 0;
                            int last_border = mp_tact_borders.Count - 1;
                            bool found = false;

                            for(; b <= last_border; ++ b)
                                if(mp_right_border <= mp_tact_borders[b])
                                {
                                    if(b > 0)       left_border = mp_tact_borders[b - 1];
                                    right_border                = mp_tact_borders[b    ];
                                    found = true;
                                    break;
                                }

                            if(!found)
                            {
                                if(last_border - 1 > 0)     left_border  = mp_tact_borders[last_border - 1];
                                right_border = mp_tact_borders[last_border    ];
                            }

                            if(!change_management)
                            {
                                if(mp_right_border > -1)
                                {
                                    left_border         = right_border;
                                    if(b < last_border)   right_border = mp_tact_borders[b + 1];
                                }

                                mp_right_border = mp_tact_right_border = right_border;
                            }

                            if(left_border != right_border)
                            {
                                mp_work_draw(left_border,      right_border);
                                mp_show_registers(mp_snapshots[right_border]);
                            }
                            else if(!change_management)
                            {
                                mp_right_border = mp_tact_right_border = -1;

                                run = STOP;
                            }
                        }
                    }
                    break;
                }
                case CHIP:
                {
                    int last_snapshot = chip_snapshots->Count - 1;

                    if(num_steps)
                    {
                        int left_border  = 0;
                        int right_border = 0;

                        if(!tacts)
                        {
                            if(!change_management)             chip_tact_right_border = ++ chip_right_border;

                            left_border  = (chip_right_border - num_steps + 1 < 0)     ? 0                       : (chip_right_border - num_steps + 1);
                            right_border = (chip_right_border > last_snapshot * 6 - 1) ? (last_snapshot * 6 - 1) : chip_right_border;

                            if(left_border <= last_snapshot * 6 - 1)
                            {
                                paint_work_chip    (left_border, right_border);
                                mp_show_registers  (chip_mp_snapshots[(right_border / 6) + ((right_border % 6) >= 4)]);
                                chip_show_registers(chip_snapshots,    right_border);
                            }
                        }
                        else
                        {
                            int tact_left  = 0;
                            int tact_right = 0;
                            bool add = false;
                            int last_border = chip_tact_borders.Count - 1;
                            bool found = false;

                            if(chip_tact_right_border - num_steps > chip_right_border
                            &&      chip_right_border             < chip_tact_borders[last_border])
                            {
                                add = true;
                                chip_tact_right_border = ++ chip_right_border;
                            }

                            for(int b = 0; b <= last_border; ++ b)
                                if(chip_right_border <= chip_tact_borders[b])
                                {
                                    if(b > 0)       tact_left = chip_tact_borders[b - 1];
                                    tact_right                = chip_tact_borders[b    ];
                                    found = true;
                                    break;
                                }

                            if(!found)
                            {
                                if(last_border - 1 > 0)     tact_left  = chip_tact_borders[last_border - 1];
                                tact_right = chip_tact_borders[last_border    ];
                            }

                            if(!change_management
                            && !add)
                            {
                                chip_tact_right_border ++;
                                if(chip_right_border <  tact_right - 1
                                || tact_right        == chip_tact_borders[last_border])            chip_right_border ++;
                            }

                            left_border = ((chip_tact_right_border - num_steps + 1) < 0) ? 0 : (chip_tact_right_border - num_steps + 1);
                            if(left_border < tact_left)         left_border  = tact_left;

                            if(!change_management
                            && tact_right      < chip_tact_borders[last_border]
                            && left_border - 1 > chip_right_border)
                            {
                                chip_tact_right_border ++;
                                chip_right_border      ++;
                                paint_work_chip    (chip_right_border, chip_right_border);
                                mp_show_registers  (chip_mp_snapshots[(chip_right_border / 6) + ((chip_right_border % 6) >= 4)]);
                                chip_show_registers(chip_snapshots,    chip_right_border);
                            }
                            else if(left_border <= last_snapshot * 6 - 1)
                            {
                                right_border = (chip_right_border > last_snapshot * 6 - 1) ? (last_snapshot * 6 - 1) : chip_right_border;
                                paint_work_chip    (left_border, right_border);
                                mp_show_registers  (chip_mp_snapshots[(right_border / 6) + ((right_border % 6) >= 4)]);
                                chip_show_registers(chip_snapshots,    right_border);
                            }
                        }

                        if(!change_management
                        && left_border > last_snapshot * 6 - 1)
                        {
                            chip_right_border = chip_tact_right_border = -1;

                            run = STOP;
                        }
                    }
                    else
                    {
                        if(checkBox2->Checked)
                        {
                            mp_show_registers  (chip_mp_snapshots[last_snapshot]);
                            chip_show_registers(chip_snapshots,   last_snapshot * 6);

                            chip_end = true;

                            if(!change_management)
                            {
                                chip_right_border = chip_tact_right_border = -1;

                                run = STOP;
                            }
                        }
                        else
                        {
                            int left_border  = 0;
                            int right_border = 0;
                            int b = 0;
                            int last_border = chip_tact_borders.Count - 1;
                            bool found = false;

                            for(; b <= last_border; ++ b)
                                if(chip_right_border <= chip_tact_borders[b])
                                {
                                    if(b > 0)       left_border = chip_tact_borders[b - 1];
                                    right_border                = chip_tact_borders[b    ];
                                    found = true;
                                    break;
                                }

                            if(!found)
                            {
                                if(last_border - 1 > 0)     left_border  = chip_tact_borders[last_border - 1];
                                right_border = chip_tact_borders[last_border    ];
                            }

                            if(!change_management)
                            {
                                if(chip_right_border > -1)
                                {
                                    left_border         = right_border;
                                    if(b < last_border)   right_border = chip_tact_borders[b + 1];
                                }

                                chip_right_border = chip_tact_right_border = right_border;
                            }

                            if(left_border != right_border)
                            {
                                mp_show_registers  (chip_mp_snapshots[right_border / 6]);
                                chip_show_registers(chip_snapshots,   right_border);
                            }
                            else if(!change_management)
                            {
                                chip_right_border = chip_tact_right_border = -1;

                                run = STOP;
                            }
                        }
                    }
                    break;
                }
            }
        }

        paint(false);

        change_management = false;
    }

    System::Boolean Form1::is_active(Visual_Element^ element)
    {
        Color current_color = element->get_color();

        if(current_color.R == work_color.R
        && current_color.G == work_color.G
        && current_color.B == work_color.B)
            return true;

        return false;
    }

    System::Void Form1::paint(bool all)
    {
        paint_GSA(false, false || all);
        paint_GSA(true, true);
        paint_chip(false, false || all);
        paint_chip(true, true);
    }

    System::Void Form1::Form1_Shown(System::Object^  sender, System::EventArgs^  e)
    {
        build_GSA ();
        build_chip();

        paint(false);
    }

    System::Void Form1::Form1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e) { paint(true); }

    void Form1::mp_reset_color()
    {
        int t = 0,
            f = 0,
            num_elements = 0,
            num_type = elements.Count;
        
        for(t = 0; t < num_type; ++ t)
        {
            num_elements = elements[t]->Count;
            for(f = 0; f < num_elements; ++ f)
                elements[t]$[f]->set_color(default_color);
        }

        int l = 0,
            num_lines = lines.Count;

        for(l = 0; l < num_lines; ++ l)     lines[l]->set_color(default_color);
    }

    void Form1::chip_reset_color()
    {
        MS_rect      ->set_color(default_color);
        DC_trapeze   ->set_color(default_color);
        CLC_D_trapeze->set_color(default_color);

        MLC_rect     ->set_color(default_color);
        CLC_Y_trapeze->set_color(default_color);
        OA_rect      ->set_color(default_color);

        reset        ->set_color(default_color);
        SP           ->set_color(default_color);
        SP_MS        ->set_color(default_color);
        SP_MLC       ->set_color(default_color);
        MS_DC        ->set_color(default_color);
        DC           ->set_color(default_color);
        DC_CLC_D     ->set_color(default_color);
        DC_CLC_Y     ->set_color(default_color);
        CLC_D_MS_out ->set_color(default_color);
        CLC_D_MS_in  ->set_color(default_color);
        CLC_Y_OA     ->set_color(default_color);
        OA_out       ->set_color(default_color);
        OA_MLC       ->set_color(default_color);
        X0           ->set_color(default_color);
        MLC          ->set_color(default_color);
        OA_in        ->set_color(default_color);
        OA_CLC_Y     ->set_color(default_color);
        OA_CLC_D     ->set_color(default_color);
    }

    void Form1::paint_chip(bool active, bool all)
    {
        if(active == is_active(MS_rect))          MS_rect      ->paint(all);
        if(active == is_active(DC_trapeze))       DC_trapeze   ->paint(all);
        if(active == is_active(CLC_D_trapeze))    CLC_D_trapeze->paint(all);

        if(active == is_active(MLC_rect))         MLC_rect     ->paint(all);
        if(active == is_active(CLC_Y_trapeze))    CLC_Y_trapeze->paint(all);
        if(active == is_active(OA_rect))          OA_rect      ->paint(all);

        if(active == is_active(reset))            reset        ->paint(all);
        if(active == is_active(SP))               SP           ->paint(all);
        if(active == is_active(SP_MS))            SP_MS        ->paint(all);
        if(active == is_active(SP_MLC))           SP_MLC       ->paint(all);
        if(active == is_active(MS_DC))            MS_DC        ->paint(all);
        // if(active == is_active(DC))               DC           ->paint(all);
        if(active == is_active(DC_CLC_D))         DC_CLC_D     ->paint(all);
        if(active == is_active(DC_CLC_Y))         DC_CLC_Y     ->paint(all);
        if(active == is_active(CLC_D_MS_out))     CLC_D_MS_out ->paint(all);
        if(active == is_active(CLC_D_MS_in))      CLC_D_MS_in  ->paint(all);
        if(active == is_active(CLC_Y_OA))         CLC_Y_OA     ->paint(all);
        if(active == is_active(OA_out))           OA_out       ->paint(all);
        if(active == is_active(OA_MLC))           OA_MLC       ->paint(all);
        if(active == is_active(X0))               X0           ->paint(all);
        if(active == is_active(MLC))              MLC          ->paint(all);
        // if(active == is_active(OA_in))            OA_in        ->paint(all);
        if(active == is_active(OA_CLC_Y))         OA_CLC_Y     ->paint(all);
        if(active == is_active(OA_CLC_D))         OA_CLC_D     ->paint(all);

        if(active == is_active(DC))               DC           ->paint(all);
        if(active == is_active(OA_in))            OA_in        ->paint(all);
    }

    void Form1::paint_GSA(bool active, bool all)
    {
        int t = 0,
            f = 0,
            num_elements = 0,
            num_type = elements.Count;
        
        for(t = 0; t < num_type; ++ t)
        {
            num_elements = elements[t]->Count;
            for(f = 0; f < num_elements; ++ f)
                if(active == is_active(elements[t]$[f]))
                    elements[t]$[f]->paint(all);
        }

        int l = 0,
            num_lines = lines.Count;

        for(l = 0; l < num_lines; ++ l)
            if(active == is_active(lines[l]))
                lines[l]->paint(all);
    }

    void Form1::build_chip()
    {
        Pen^ work_pen    = gcnew Pen(work_color   , 1.0f);
        Pen^ default_pen = gcnew Pen(default_color, 1.0f);

        const int X = 660,
                  Y = 560,
                  Y_offset = 80;

        const int RECT_X = 60,
                  RECT_Y = 100,
                  TRAPEZE_X =  60,
                  TRAPEZE_Y =  100;

        const int WITHOUT        = 0,
                  ARROW          = 1,
                  // CIRCLE      = 2;
                  CIRCLE         = 0;

        Point size_rect    = Point(RECT_X,    RECT_Y   ),
              size_trapeze = Point(TRAPEZE_X, TRAPEZE_Y);

        // ��
        MS_rect                                                             = gcnew Rect   (graphics, default_pen, label46, size_rect,
        "��",
        X,
        Y);

        // ��
        DC_trapeze                                                          = gcnew Trapeze(graphics, default_pen, label47, size_trapeze, false,
        "��",
        X + RECT_X / 2 + 100 + TRAPEZE_X / 2,
        Y);

        // �� D
        CLC_D_trapeze                                                       = gcnew Trapeze(graphics, default_pen, label48, size_trapeze, true,
        "��\nD",
        DC_trapeze->get_center().X + TRAPEZE_X / 2 + 300 + TRAPEZE_X / 2,
        Y);



        // ���
        MLC_rect                                                            = gcnew Rect   (graphics, default_pen, label49, size_rect,
        "���",
        X,
        Y + Y_offset + RECT_Y / 2 + RECT_Y / 2);

        // �� Y
        CLC_Y_trapeze                                                       = gcnew Trapeze(graphics, default_pen, label50, size_trapeze, true,
        "��\nY",
        X + RECT_X / 2 + 320 + TRAPEZE_X / 2,
        Y + Y_offset + RECT_Y / 2 + RECT_Y / 2);

        // ��
        OA_rect                                                             = gcnew Rect   (graphics, default_pen, label51, size_rect,
        "��",
        CLC_Y_trapeze->get_center().X + TRAPEZE_X / 2 + 80 + RECT_X / 2,
        Y + Y_offset + RECT_Y / 2 + RECT_Y / 2);

        reset                                                               = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40 - 40,    MS_rect      ->get_center().Y + MS_rect      ->get_size().Y / 4),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2,              MS_rect      ->get_center().Y + MS_rect      ->get_size().Y / 4) },
        ARROW  , 0, label57, "�����", 0, -10, -20);

        SP                                                                  = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40 - 40,    MS_rect      ->get_center().Y),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         MS_rect      ->get_center().Y) },
        CIRCLE , 0, label52, "��", 0, -10, -20);

        SP_MS                                                               = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         MS_rect      ->get_center().Y),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2,              MS_rect      ->get_center().Y) },
        ARROW  , 0);

        SP_MLC                                                              = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         MS_rect      ->get_center().Y),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         MLC_rect     ->get_center().Y - MLC_rect     ->get_size().Y / 4),
        Point(MLC_rect     ->get_center().X - MLC_rect     ->get_size().X / 2,              MLC_rect     ->get_center().Y - MLC_rect     ->get_size().Y / 4) },
        ARROW  , 0);

        MS_DC                                                               = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MS_rect      ->get_center().X + MS_rect      ->get_size().X / 2,              MS_rect      ->get_center().Y),
        Point(DC_trapeze   ->get_center().X - DC_trapeze   ->get_size().X / 2,              DC_trapeze   ->get_center().Y) },
        ARROW  , 0, label53, "D�", 0, +5, -20);

        DC                                                                  = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2,              DC_trapeze   ->get_center().Y),
        Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2 + 40,         DC_trapeze   ->get_center().Y) },
        CIRCLE , 0, label54, "A",     0,  +5, -20);

        // CIRCLE
        DC_CLC_D                                                            = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        // Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2 + 40 + 4,      DC_trapeze   ->get_center().Y),
        Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2 + 40 + 1,     DC_trapeze   ->get_center().Y),
        Point(CLC_D_trapeze->get_center().X - CLC_D_trapeze->get_size().X / 2,              CLC_D_trapeze->get_center().Y) },
        ARROW  , 0);

        // CIRCLE
        DC_CLC_Y                                                            = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        // Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2 + 40,          DC_trapeze   ->get_center().Y + 4),
        Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2 + 40,         DC_trapeze   ->get_center().Y + 1),
        Point(DC_trapeze   ->get_center().X + DC_trapeze   ->get_size().X / 2 + 40,         CLC_Y_trapeze->get_center().Y - CLC_Y_trapeze->get_size().Y / 4),
        Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2,              CLC_Y_trapeze->get_center().Y - CLC_Y_trapeze->get_size().Y / 4) },
        ARROW  , 0);

        CLC_D_MS_out                                                        = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(CLC_D_trapeze->get_center().X + CLC_D_trapeze->get_size().X / 2,              CLC_D_trapeze->get_center().Y),
        Point(CLC_D_trapeze->get_center().X + CLC_D_trapeze->get_size().X / 2 + 40,         CLC_D_trapeze->get_center().Y),
        Point(CLC_D_trapeze->get_center().X + CLC_D_trapeze->get_size().X / 2 + 40,         CLC_D_trapeze->get_center().Y - CLC_D_trapeze->get_size().Y / 2 - 60),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         CLC_D_trapeze->get_center().Y - CLC_D_trapeze->get_size().Y / 2 - 60),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         MS_rect      ->get_center().Y - MS_rect      ->get_size().Y / 4) },
        WITHOUT, 0, label55, "D", 0, +5, -20);

        CLC_D_MS_in                                                         = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2 - 40,         MS_rect      ->get_center().Y - MS_rect      ->get_size().Y / 4),
        Point(MS_rect      ->get_center().X - MS_rect      ->get_size().X / 2,              MS_rect      ->get_center().Y - MS_rect      ->get_size().Y / 4) },
        ARROW  , 0, label56, "D", 0, +5, -20);

        CLC_Y_OA                                                            = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(CLC_Y_trapeze->get_center().X + CLC_Y_trapeze->get_size().X / 2,              CLC_Y_trapeze->get_center().Y),
        Point(OA_rect      ->get_center().X - OA_rect      ->get_size().X / 2,              OA_rect      ->get_center().Y) },
        ARROW  , 0, label58, "Y", 0, +5, -20);

        int X_switch = MLC_rect->get_center().X + MLC_rect->get_size().X / 2 + ((CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2) - (MLC_rect->get_center().X + MLC_rect->get_size().X / 2)) / 3 * 1;

        OA_out                                                              = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(OA_rect      ->get_center().X + OA_rect      ->get_size().X / 2,              OA_rect      ->get_center().Y),
        Point(OA_rect      ->get_center().X + OA_rect      ->get_size().X / 2 + 40,         OA_rect      ->get_center().Y),
        Point(OA_rect      ->get_center().X + OA_rect      ->get_size().X / 2 + 40,         OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 60),
        Point(X_switch,                                                                     OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 60) },
        WITHOUT, 0, label59, "X", 0, +5, -20);

        OA_MLC                                                              = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(X_switch,                                                                     OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 60),
        Point(MLC_rect     ->get_center().X - MLC_rect     ->get_size().X / 2 - 80,         OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 60),
        Point(MLC_rect     ->get_center().X - MLC_rect     ->get_size().X / 2 - 80,         MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4),
        Point(MLC_rect     ->get_center().X - MLC_rect     ->get_size().X / 2,              MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4) },
        ARROW  , 0, label60, "X3, X4, X5, X6, X7", 2, -5, -20);

        X0                                                                  = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MLC_rect     ->get_center().X - MLC_rect     ->get_size().X / 2 - 80 - 40,    OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 40),
        Point(MLC_rect     ->get_center().X - MLC_rect     ->get_size().X / 2 - 80,         OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 40) },
        ARROW  , 0, label61, "X0", 0, +5, -20);
    
        MLC                                                                 = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(MLC_rect     ->get_center().X + MLC_rect     ->get_size().X / 2,              MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4),
        Point(X_switch,                                                                     MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4) },
        ARROW  , 0, label62, "X3', X4', X5', X6', X7'", 0, +5, -20);

        OA_in                                                               = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        Point(X_switch,                                                                     OA_rect      ->get_center().Y + OA_rect      ->get_size().Y / 2 + 60),
        Point(X_switch,                                                                     MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4),
        Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2 - 40,         MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4) },
        CIRCLE , 0, label63, "X0, X1, X2, X3', X4', X5', X6', X7', X8, X9", 1, 0, -20);

        // CIRCLE
        OA_CLC_Y                                                            = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        // Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2 - 40 + 4,      MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4),
        Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2 - 40 + 1,     MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4),
        Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2,              MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4) },
        ARROW  , 0);

        // CIRCLE
        OA_CLC_D                                                            = gcnew Line   (graphics, default_pen, gcnew array <Point> {
        // Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2 - 40,          MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4 - 3),
        Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2 - 40,         MLC_rect     ->get_center().Y + MLC_rect     ->get_size().Y / 4 - 1),
        Point(CLC_Y_trapeze->get_center().X - CLC_Y_trapeze->get_size().X / 2 - 40,         CLC_D_trapeze->get_center().Y + CLC_D_trapeze->get_size().Y / 4),
        Point(CLC_D_trapeze->get_center().X - CLC_D_trapeze->get_size().X / 2,              CLC_D_trapeze->get_center().Y + CLC_D_trapeze->get_size().Y / 4) },
        ARROW  , 0);

        dataGridView_mem_states->Location       = System::Drawing::Point(MS_rect->get_center().X - dataGridView_mem_states->Size.Width / 2,
                                                                         MS_rect->get_center().Y - MS_rect->get_size().Y / 2 - 4 - dataGridView_mem_states->Size.Height);

        dataGridView_unitary_code->Location     = System::Drawing::Point(DC_trapeze->get_center().X - dataGridView_unitary_code->Size.Width / 2,
                                                                         DC_trapeze->get_center().Y - DC_trapeze->get_size().Y / 2 - 4 - dataGridView_unitary_code->Size.Height);

        dataGridView_X_vector->Location         = System::Drawing::Point(OA_rect->get_center().X + OA_rect->get_size().X / 2 - dataGridView_X_vector->Size.Width,
                                                                         OA_rect->get_center().Y + OA_rect->get_size().Y / 2 + 60 - 4 - dataGridView_X_vector->Size.Height);

        dataGridView_mem_conditions->Location   = System::Drawing::Point(MLC_rect->get_center().X - dataGridView_mem_conditions->Size.Width / 2,
                                                                         MLC_rect->get_center().Y - MLC_rect->get_size().Y / 2 - 4 - dataGridView_mem_conditions->Size.Height);

        dataGridView_Y_vector->Location         = System::Drawing::Point(CLC_Y_trapeze->get_center().X + (OA_rect->get_center().X - CLC_Y_trapeze->get_center().X) / 2 - dataGridView_Y_vector->Size.Width / 2,
                                                                         CLC_Y_trapeze->get_center().Y - CLC_Y_trapeze->get_size().Y / 2 - 4 - dataGridView_Y_vector->Size.Height);

        dataGridView_D_vector->Location         = System::Drawing::Point(CLC_D_trapeze->get_center().X - dataGridView_D_vector->Size.Width / 2,
                                                                         CLC_D_trapeze->get_center().Y - CLC_D_trapeze->get_size().Y / 2 - 4 - dataGridView_D_vector->Size.Height);

        dataGridView_X_common->Location         = System::Drawing::Point(X_switch + 4,
                                                                         (MLC_rect->get_center().Y + MLC_rect->get_size().Y / 4) + ((OA_rect->get_center().Y + OA_rect->get_size().Y / 2 + 60) - (MLC_rect->get_center().Y + MLC_rect->get_size().Y / 4)) / 2 - dataGridView_X_common->Size.Height / 2);

        dataGridView_X_save->Location           = System::Drawing::Point(MLC_rect->get_center().X - dataGridView_X_save->Size.Width / 2,
                                                                         OA_rect->get_center().Y + OA_rect->get_size().Y / 2 + 60 - 4 - dataGridView_X_save->Size.Height);
    }

    void Form1::build_GSA()
    {
        Pen^ work_pen    = gcnew Pen(work_color   , 1.0f);
        Pen^ default_pen = gcnew Pen(default_color, 1.0f);

        // const int X = 200;
        // int Y = 30;
        const int X = 248;
        int Y = 15;

        const int PLATE_X =  60,
                  PLATE_Y =  16,
                  // RHOMB_X = 100,
                  // RHOMB_Y =  32,
                  RHOMB_X = 90,
                  RHOMB_Y = 28,

                  RECT_Y = 6,
                  // LINE_Y = 12,
                  LINE_Y = 11,

                  // DIST_1 = 12,
                  // DIST_2 = 24,
                  DIST_1 = 10,
                  DIST_2 = 24, // ������ ���� ������ 3.

                  X_LEFT  = -150,
                  X_RIGHT =  150;

        int       line_y_current =   0;
        const int rect_width     = 150;
        const int DIST_3         =  16; // ������ ���� ������ 2.

        Point size_rhomb = Point(RHOMB_X, RHOMB_Y),
              size_plate = Point(PLATE_X, PLATE_Y);

        elements.Add(gcnew List<Visual_Element^>);
        elements.Add(gcnew List<Visual_Element^>);
        elements.Add(gcnew List<Visual_Element^>);
        elements.Add(gcnew List<Visual_Element^>);

        elements[PLATE]->Add    /* 0 */                                             (gcnew Plate(graphics, default_pen, label1, size_plate,
        "������",
        X,
        Y));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + PLATE_Y / 2),
                Point(X, Y + PLATE_Y / 2 + DIST_2 / 3 * 1) },
                gcnew array <Point> { Point(PLATE, 0) },
                STATE, 0, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + PLATE_Y / 2 + DIST_2 / 3 * 1),
                Point(X, Y + PLATE_Y / 2 + DIST_2 / 3 * 2) },
                gcnew array <Point> { Point(PLATE, 0), Point(RHOMB, 0) },
                STATE, 0, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + PLATE_Y / 2 + DIST_2 / 3 * 2),
                Point(X, Y + PLATE_Y / 2 + DIST_2) },
                gcnew array <Point> { Point(STATE, 0) },
                RHOMB, 0, true , 0));

        elements[RHOMB]->Add    /* 0 */                                             (gcnew Rhomb(graphics, default_pen, label2, size_rhomb,
        X,
        Y += PLATE_Y / 2 + RHOMB_Y / 2 + DIST_2));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                Point(X, Y + RHOMB_Y / 2 + DIST_1) },
                gcnew array <Point> { Point(RHOMB, 0) },
                RHOMB, 1, true , 0, label65, "1", +5, 0));

        elements[RHOMB]->Add    /* 1 */                                             (gcnew Rhomb(graphics, default_pen, label3, size_rhomb,
        X,
        Y += RHOMB_Y / 2 +  RHOMB_Y / 2 + DIST_1));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                Point(X, Y + RHOMB_Y / 2 + DIST_1) },
                gcnew array <Point> { Point(RHOMB, 1) },
                RHOMB, 2, true , 0, label66, "0", +5, 0));

        elements[RHOMB]->Add    /* 2 */                                             (gcnew Rhomb(graphics, default_pen, label4, size_rhomb,
        X,
        Y += RHOMB_Y / 2 +  RHOMB_Y / 2 + DIST_1));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                // Point(X, Y + RHOMB_Y / 2 + DIST_2) },
                Point(X, Y + RHOMB_Y / 2 + DIST_3) },
                gcnew array <Point> { Point(RHOMB, 2) },
                RECT , 0, true , 0, label68, "0", +5, 0));

        line_y_current = LINE_Y * 4;                                        // CURRENT

        elements[RECT ]->Add    /* 0 */                                             (gcnew Rect (graphics, default_pen, label5,
        // 60,
        rect_width,
        RECT_Y + line_y_current,
        X,
        // Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_2));
        Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_3));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1) },
                gcnew array <Point> { Point(RECT , 0) },
                STATE, 1, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2) },
                gcnew array <Point> { Point(STATE, 1) },
                RHOMB, 3, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2) },
                gcnew array <Point> { Point(STATE, 1), Point(RHOMB, 7) },
                RHOMB, 3, true , 0));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

            elements[RECT ]->Add    /* 1 */                                         (gcnew Rect (graphics, default_pen, label6,
            // 60,
            rect_width,
            RECT_Y + line_y_current,
            // X + X_LEFT,
            X + X_LEFT - DIST_1,
            Y));

        line_y_current = LINE_Y * 4;                                        // CURRENT

        elements[RHOMB]->Add    /* 3 */                                             (gcnew Rhomb(graphics, default_pen, label7, size_rhomb,
        X,
        Y += (RECT_Y + line_y_current) / 2 + RHOMB_Y / 2 + DIST_2));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                Point(X, Y + RHOMB_Y / 2 + DIST_1) },
                gcnew array <Point> { Point(RHOMB, 3) },
                RHOMB, 4, true , 0, label70, "0", +5, 0));

        elements[RHOMB]->Add    /* 4 */                                             (gcnew Rhomb(graphics, default_pen, label8, size_rhomb,
        X,
        Y += RHOMB_Y / 2 + RHOMB_Y / 2 + DIST_1));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                // Point(X, Y + RHOMB_Y / 2 + DIST_1) },
                Point(X, Y + RHOMB_Y / 2 + DIST_1 + DIST_2) },
                gcnew array <Point> { Point(RHOMB, 4) },
                RHOMB, 5, true , 0, label72, "0", +5, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X + RHOMB_X / 2, Y),
                // Point(X + X_RIGHT - 150 / 2, Y) },
                Point(X + X_RIGHT + DIST_1 - 150 / 2, Y) },
                gcnew array <Point> { Point(RHOMB, 4) },
                RECT , 2, true , 0, label73, "1", -5, -15));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

            elements[RECT ]->Add    /* 2 */                                         (gcnew Rect (graphics, default_pen, label9,
            // 150,
            rect_width,
            RECT_Y + line_y_current,
            // X + X_RIGHT,
            X + X_RIGHT + DIST_1,
            Y));

        elements[RHOMB]->Add    /* 5 */                                             (gcnew Rhomb(graphics, default_pen, label10, size_rhomb,
        X,
//      Y += RHOMB_Y / 2 + RHOMB_Y / 2 + DIST_1));
        Y += RHOMB_Y / 2 + RHOMB_Y / 2 + DIST_1 + DIST_2));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                // Point(X, Y + RHOMB_Y / 2 + DIST_2) },
                Point(X, Y + RHOMB_Y / 2 + DIST_3) },
                gcnew array <Point> { Point(RHOMB, 5) },
                RECT , 4, true , 0, label74, "0", +5, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X + RHOMB_X / 2, Y),
                // Point(X + X_RIGHT - 150 / 2, Y) },
                Point(X + X_RIGHT + DIST_1 - 150 / 2, Y) },
                gcnew array <Point> { Point(RHOMB, 5) },
                RECT , 3, true , 0, label75, "1", -5, -15));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

            elements[RECT ]->Add    /* 3 */                                         (gcnew Rect (graphics, default_pen, label11,
            // 150,
            rect_width,
            RECT_Y + line_y_current,
            // X + X_RIGHT,
            X + X_RIGHT + DIST_1,
            Y));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 2;     // CURRENT

        elements[RECT ]->Add    /* 4 */                                             (gcnew Rect (graphics, default_pen, label12,
        // 180,
        rect_width,
        RECT_Y + line_y_current,
        X,
        // Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_2));
        Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_3));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2),
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 4 * 1) },
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3 / 2) },
                gcnew array <Point> { Point(RECT , 4) },
                STATE, 2, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 4 * 1),
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 4 * 2) },
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3 / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3) },
                gcnew array <Point> { Point(RECT , 2), Point(RECT , 3), Point(RECT , 4) },
                STATE, 2, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 4 * 2),
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 4 * 3) },
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3 + DIST_3 / 2) },
                gcnew array <Point> { Point(STATE, 2) },
                RHOMB, 6, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 4 * 3),
                // Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2) },
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3 + DIST_3 / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_3 + DIST_3) },
                gcnew array <Point> { Point(RHOMB, 3), Point(STATE, 2) },
                RHOMB, 6, true , 0));

        elements[RHOMB]->Add    /* 6 */                                             (gcnew Rhomb(graphics, default_pen, label13, size_rhomb,
        X,
        // Y += (RECT_Y + line_y_current) / 2 + RHOMB_Y / 2 + DIST_2));
        Y += (RECT_Y + line_y_current) / 2 + RHOMB_Y / 2 + DIST_3 + DIST_3));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                // Point(X, Y + RHOMB_Y / 2 + DIST_2) },
                Point(X, Y + RHOMB_Y / 2 + DIST_3) },
                gcnew array <Point> { Point(RHOMB, 6) },
                RECT , 5, true , 0, label76, "0", +5, 0));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 3;     // CURRENT

        elements[RECT ]->Add    /* 5 */                                             (gcnew Rect (graphics, default_pen, label14,
        // 170,
        rect_width,
        RECT_Y + line_y_current,
        X,
        // Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_2));
        Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_3));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1) },
                gcnew array <Point> { Point(RECT , 5) },
                STATE, 3, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2) },
                gcnew array <Point> { Point(RECT , 5), Point(RECT , 6) },
                STATE, 3, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2) },
                gcnew array <Point> { Point(STATE, 3) },
                RHOMB, 7, true , 0));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

            elements[RECT ]->Add    /* 6 */                                         (gcnew Rect (graphics, default_pen, label15,
            // 60,
            rect_width,
            RECT_Y + line_y_current,
            // X + X_RIGHT,
            X + X_RIGHT + DIST_1,
            Y));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 3;     // CURRENT

        elements[RHOMB]->Add    /* 7 */                                             (gcnew Rhomb(graphics, default_pen, label16, size_rhomb,
        X,
        Y += (RECT_Y + line_y_current) / 2 + RHOMB_Y / 2 + DIST_2));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                Point(X, Y + RHOMB_Y / 2 + DIST_1) },
                gcnew array <Point> { Point(RHOMB, 7) },
                RHOMB, 8, true , 0, label79, "1", +5, 0));

        elements[RHOMB]->Add    /* 8 */                                             (gcnew Rhomb(graphics, default_pen, label17, size_rhomb,
        X,
        Y += RHOMB_Y / 2 + RHOMB_Y / 2 + DIST_1));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                // Point(X, Y + RHOMB_Y / 2 + DIST_2) },
                Point(X, Y + RHOMB_Y / 2 + DIST_3) },
                gcnew array <Point> { Point(RHOMB, 8) },
                RECT , 7, true , 0, label81, "1", +5, 0));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

        elements[RECT ]->Add    /* 7 */                                             (gcnew Rect (graphics, default_pen, label18,
        // 120,
        rect_width,
        RECT_Y + line_y_current,
        X,
        // Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_2));
        Y += RHOMB_Y / 2 + (RECT_Y + line_y_current) / 2 + DIST_3));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1) },
                gcnew array <Point> { Point(RECT , 7) },
                STATE, 4, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2) },
                gcnew array <Point> { Point(STATE, 4) },
                RHOMB, 9, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2) },
                gcnew array <Point> { Point(STATE, 4), Point(RHOMB, 8) },
                RHOMB, 9, true , 0));

        elements[RHOMB]->Add    /* 9 */                                             (gcnew Rhomb(graphics, default_pen, label19, size_rhomb,
        X,
        Y += (RECT_Y + line_y_current) / 2 + RHOMB_Y / 2 + DIST_2));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + RHOMB_Y / 2),
                // Point(X, Y + RHOMB_Y / 2 + DIST_2) },
                Point(X, Y + RHOMB_Y / 2 + DIST_3) },
                gcnew array <Point> { Point(RHOMB, 9) },
                RECT , 8, true , 0, label83, "1", +5, 0));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

        elements[RECT ]->Add    /* 8 */                                             (gcnew Rect (graphics, default_pen, label20,
        // 60,
        rect_width,
        RECT_Y + line_y_current,
        X,
        // Y += RHOMB_Y / 2 + (RECT_Y + LINE_Y * 4) / 2 + DIST_2));
        Y += RHOMB_Y / 2 + (RECT_Y + LINE_Y * 4) / 2 + DIST_3));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1) },
                gcnew array <Point> { Point(RECT , 8) },
                STATE, 5, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2) },
                // gcnew array <Point> { Point(RECT , 8), Point(RHOMB, 9), Point(RECT , 1) },
                gcnew array <Point> { Point(RECT , 8), Point(RECT , 1) },
                STATE, 5, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2) },
                gcnew array <Point> { Point(STATE, 5) },
                // PLATE, 1, false, 0));
                RECT , 9, true , 0));

        line_y_current = LINE_Y * 4;    // line_y_current = LINE_Y * 1;     // CURRENT

        elements[RECT ]->Add    /* 9 */                                             (gcnew Rect (graphics, default_pen, label89,
        rect_width,
        RECT_Y + line_y_current,
        X,
        Y += (RECT_Y + LINE_Y * 4) / 2 + (RECT_Y + LINE_Y * 4) / 2 + DIST_2));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1) },
                gcnew array <Point> { Point(RECT , 9) },
                STATE, 6, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 1),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2) },
                gcnew array <Point> { Point(RECT , 9), Point(RHOMB, 9) },
                STATE, 6, false, 0));

                lines.Add                                                           (gcnew Line (graphics, default_pen, gcnew array <Point> {
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2 / 3 * 2),
                Point(X, Y + (RECT_Y + line_y_current) / 2 + DIST_2) },
                gcnew array <Point> { Point(STATE, 6) },
                PLATE, 1, true, 0));

        elements[PLATE]->Add    /* 1 */                                             (gcnew Plate(graphics, default_pen, label21, size_plate,
        "�����",
        X,
        Y += (RECT_Y + line_y_current) / 2 + PLATE_Y / 2 + DIST_2));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[0]->get_center().X - elements[RHOMB]$[0]->get_size().X / 2,      elements[RHOMB]$[0]->get_center().Y),
        Point(elements[RHOMB]$[0]->get_center().X - elements[RHOMB]$[0]->get_size().X / 2 - 50, elements[RHOMB]$[0]->get_center().Y),
        Point(elements[RHOMB]$[0]->get_center().X - elements[RHOMB]$[0]->get_size().X / 2 - 50, elements[RHOMB]$[0]->get_center().Y - elements[RHOMB]$[0]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[RHOMB]$[0]->get_center().X,                                              elements[RHOMB]$[0]->get_center().Y - elements[RHOMB]$[0]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        gcnew array <Point> { Point(RHOMB, 0) },
        STATE, 0, true, 0, label64, "0", -5, -15));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[1]->get_center().X - elements[RHOMB]$[1]->get_size().X / 2,      elements[RHOMB]$[1]->get_center().Y),
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RHOMB]$[1]->get_center().Y),
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RHOMB]$[2]->get_center().Y) },
        gcnew array <Point> { Point(RHOMB, 1) },
        RECT , 1, false, 0, label67, "1", -5, -15));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[2]->get_center().X - elements[RHOMB]$[2]->get_size().X / 2,      elements[RHOMB]$[2]->get_center().Y),
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RHOMB]$[2]->get_center().Y) },
        gcnew array <Point> { Point(RHOMB, 2) },
        RECT , 1, true , 0, label69, "1", -5, -15));

        // EDIT
        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RHOMB]$[2]->get_center().Y),
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RECT ]$[1]->get_center().Y - elements[RECT ]$[1]->get_size().Y / 2) },
        gcnew array <Point> { Point(RHOMB, 1), Point(RHOMB, 2) },
        RECT , 1, true , 0));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RECT ]$[1]->get_center().Y + elements[RECT ]$[1]->get_size().Y / 2),
        // Point(elements[RECT ]$[1]->get_center().X,                                               elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2),
        // Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 50,  elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        Point(elements[RECT ]$[1]->get_center().X,                                              elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 60, elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        gcnew array <Point> { Point(RECT , 1) },
        STATE, 5, false , 0));

        array<Point>^ points = gcnew array<Point>{};
        get_top_hf_circle_points(points, 10,    elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 50,
                                                elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2);

        lines.Add                                                                   (gcnew Line (graphics, default_pen, points,
        gcnew array <Point> { Point(RECT , 1) },
        STATE, 5, false , 0));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        // Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 50,  elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2),
        // Point(elements[PLATE]$[1]->get_center().X,                                               elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 40, elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[PLATE]$[1]->get_center().X,                                              elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        gcnew array <Point> { Point(RECT , 1), Point(RHOMB, 9) },
        STATE, 5, true , 0));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[3]->get_center().X - elements[RHOMB]$[3]->get_size().X / 2,      elements[RHOMB]$[3]->get_center().Y),
        Point(elements[RHOMB]$[3]->get_center().X - elements[RHOMB]$[3]->get_size().X / 2 - 50, elements[RHOMB]$[3]->get_center().Y),
        // Point(elements[RHOMB]$[3]->get_center().X - elements[RHOMB]$[3]->get_size().X / 2 - 50,  elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_2 / 4 * 1),
        // Point(elements[RHOMB]$[6]->get_center().X,                                               elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_2 / 4 * 1) },
        Point(elements[RHOMB]$[3]->get_center().X - elements[RHOMB]$[3]->get_size().X / 2 - 50, elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_3 / 2),
        Point(elements[RHOMB]$[6]->get_center().X,                                              elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_3 / 2) },
        gcnew array <Point> { Point(RHOMB, 3) },
        RHOMB, 6, true , 0, label71, "1", -5, -15));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2,      elements[RECT ]$[2]->get_center().Y),
        Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2 + 30, elements[RECT ]$[2]->get_center().Y),
        Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2 + 30, elements[RECT ]$[3]->get_center().Y) },
        gcnew array <Point> { Point(RECT , 2) },
        STATE, 2, false, 0));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RECT ]$[3]->get_center().X + elements[RECT ]$[3]->get_size().X / 2,      elements[RECT ]$[3]->get_center().Y),
        Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2 + 30, elements[RECT ]$[3]->get_center().Y) },
        gcnew array <Point> { Point(RECT , 3) },
        STATE, 2, true , 0));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2 + 30, elements[RECT ]$[3]->get_center().Y),
        // Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2 + 30,  elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_2 / 4 * 3),
        // Point(elements[RHOMB]$[6]->get_center().X,                                               elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_2 / 4 * 3) },
        Point(elements[RECT ]$[2]->get_center().X + elements[RECT ]$[2]->get_size().X / 2 + 30, elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_3 - DIST_3 / 2),
        Point(elements[RHOMB]$[6]->get_center().X,                                              elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_3 - DIST_3 / 2) },
        gcnew array <Point> { Point(RECT , 2), Point(RECT , 3) },
        STATE, 2, true , 0));

        // EDIT
        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[6]->get_center().X + elements[RHOMB]$[6]->get_size().X / 2,      elements[RHOMB]$[6]->get_center().Y),
        Point(elements[RECT ]$[6]->get_center().X,                                              elements[RHOMB]$[6]->get_center().Y),
        Point(elements[RECT ]$[6]->get_center().X,                                              elements[RECT ]$[6]->get_center().Y - elements[RECT ]$[6]->get_size().Y / 2) },
        gcnew array <Point> { Point(RHOMB, 6) },
        RECT , 6, true , 0, label77, "1", -5, -15));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RECT ]$[6]->get_center().X,                                              elements[RECT ]$[6]->get_center().Y + elements[RECT ]$[6]->get_size().Y / 2),
        Point(elements[RECT ]$[6]->get_center().X,                                              elements[RHOMB]$[7]->get_center().Y - elements[RHOMB]$[7]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[RHOMB]$[7]->get_center().X,                                              elements[RHOMB]$[7]->get_center().Y - elements[RHOMB]$[7]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        gcnew array <Point> { Point(RECT , 6) },
        STATE, 3, true, 0));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[7]->get_center().X - elements[RHOMB]$[7]->get_size().X / 2,      elements[RHOMB]$[7]->get_center().Y),
        Point(elements[RHOMB]$[7]->get_center().X - elements[RHOMB]$[7]->get_size().X / 2 - 70, elements[RHOMB]$[7]->get_center().Y),
        Point(elements[RHOMB]$[7]->get_center().X - elements[RHOMB]$[7]->get_size().X / 2 - 70, elements[RHOMB]$[3]->get_center().Y - elements[RHOMB]$[3]->get_size().Y / 2 - DIST_2 / 3 * 1),
        Point(elements[RHOMB]$[3]->get_center().X,                                              elements[RHOMB]$[3]->get_center().Y - elements[RHOMB]$[3]->get_size().Y / 2 - DIST_2 / 3 * 1) },
        gcnew array <Point> { Point(RHOMB, 7) },
        RHOMB, 3, true, 0, label78, "0", -5, -15));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[8]->get_center().X - elements[RHOMB]$[8]->get_size().X / 2,      elements[RHOMB]$[8]->get_center().Y),
        Point(elements[RHOMB]$[8]->get_center().X - elements[RHOMB]$[8]->get_size().X / 2 - 50, elements[RHOMB]$[8]->get_center().Y),
        Point(elements[RHOMB]$[8]->get_center().X - elements[RHOMB]$[8]->get_size().X / 2 - 50, elements[RHOMB]$[9]->get_center().Y - elements[RHOMB]$[9]->get_size().Y / 2 - DIST_2 / 3 * 1),
        Point(elements[RHOMB]$[9]->get_center().X,                                              elements[RHOMB]$[9]->get_center().Y - elements[RHOMB]$[9]->get_size().Y / 2 - DIST_2 / 3 * 1) },
        gcnew array <Point> { Point(RHOMB, 8) },
        RHOMB, 9, true, 0, label80, "0", -5, -15));

        lines.Add                                                                   (gcnew Line (graphics, default_pen, gcnew array <Point> {
        Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2,      elements[RHOMB]$[9]->get_center().Y),
        Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 50, elements[RHOMB]$[9]->get_center().Y),
        // Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 50,  elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        Point(elements[RHOMB]$[9]->get_center().X - elements[RHOMB]$[9]->get_size().X / 2 - 50, elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[PLATE]$[1]->get_center().X,                                              elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        gcnew array <Point> { Point(RHOMB, 9) },
        STATE, 6, true, 0, label82, "0", -5, -15));


        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label22, gcnew array <Point> {
        Point(elements[RHOMB]$[0]->get_center().X - 10,                                         elements[RHOMB]$[0]->get_center().Y - elements[RHOMB]$[0]->get_size().Y / 2 - DIST_2 / 3 * 1),
        Point(elements[RHOMB]$[0]->get_center().X + 10,                                         elements[RHOMB]$[0]->get_center().Y - elements[RHOMB]$[0]->get_size().Y / 2 - DIST_2 / 3 * 1) },
        "a0", 20, 0));

        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label23, gcnew array <Point> {
        Point(elements[RHOMB]$[3]->get_center().X - 10,                                         elements[RHOMB]$[3]->get_center().Y - elements[RHOMB]$[3]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[RHOMB]$[3]->get_center().X + 10,                                         elements[RHOMB]$[3]->get_center().Y - elements[RHOMB]$[3]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        "a1", 20, 0));

        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label24, gcnew array <Point> {
        // Point(elements[RHOMB]$[6]->get_center().X - 10,                                          elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_2 / 4 * 2),
        // Point(elements[RHOMB]$[6]->get_center().X + 10,                                          elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_2 / 4 * 2) },
        Point(elements[RHOMB]$[6]->get_center().X - 10,                                         elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_3),
        Point(elements[RHOMB]$[6]->get_center().X + 10,                                         elements[RHOMB]$[6]->get_center().Y - elements[RHOMB]$[6]->get_size().Y / 2 - DIST_3) },
        "a2", 20, 0));

        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label25, gcnew array <Point> {
        Point(elements[RHOMB]$[7]->get_center().X - 10,                                         elements[RHOMB]$[7]->get_center().Y - elements[RHOMB]$[7]->get_size().Y / 2 - DIST_2 / 3 * 1),
        Point(elements[RHOMB]$[7]->get_center().X + 10,                                         elements[RHOMB]$[7]->get_center().Y - elements[RHOMB]$[7]->get_size().Y / 2 - DIST_2 / 3 * 1) },
        "a3", -36, 0));

        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label26, gcnew array <Point> {
        Point(elements[RHOMB]$[9]->get_center().X - 10,                                         elements[RHOMB]$[9]->get_center().Y - elements[RHOMB]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2),
        Point(elements[RHOMB]$[9]->get_center().X + 10,                                         elements[RHOMB]$[9]->get_center().Y - elements[RHOMB]$[9]->get_size().Y / 2 - DIST_2 / 3 * 2) },
        "a4", 20, 0));

        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label90, gcnew array <Point> {
        Point(elements[PLATE]$[1]->get_center().X - 10,                                         elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 1),
        Point(elements[PLATE]$[1]->get_center().X + 10,                                         elements[RECT ]$[9]->get_center().Y - elements[RECT ]$[9]->get_size().Y / 2 - DIST_2 / 3 * 1) },
        "a5", 20, -10));

        elements[STATE]->Add                                                        (gcnew Line (graphics, default_pen, label27, gcnew array <Point> {
        Point(elements[PLATE]$[1]->get_center().X - 10,                                         elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 1),
        Point(elements[PLATE]$[1]->get_center().X + 10,                                         elements[PLATE]$[1]->get_center().Y - elements[PLATE]$[1]->get_size().Y / 2 - DIST_2 / 3 * 1) },
        "a0", 20, -10));

        switch_GSA(true);
    }

    System::Void Form1::dataGridView_A_ColumnHeaderMouseClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^ e)
    {
        if(!run && !mp_end && !chip_end)
        {
            need_update_MP   = true;
            need_update_chip = true;

            int cell = e->ColumnIndex;
            if(dataGridView_A->Rows[0]->Cells[cell]->Value == "0")
                dataGridView_A->Rows[0]->Cells[cell]->Value = "1";
            else
                dataGridView_A->Rows[0]->Cells[cell]->Value = "0";

            label87->Text = convert_to_label(dataGridView_A);
        }
    }

    System::Void Form1::dataGridView_B_src_ColumnHeaderMouseClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^ e)
    {
        if(!run && !mp_end && !chip_end)
        {
            need_update_MP   = true;
            need_update_chip = true;

            int cell = e->ColumnIndex;
            if(dataGridView_B_src->Rows[0]->Cells[cell]->Value == "0")
                dataGridView_B_src->Rows[0]->Cells[cell]->Value = "1";
            else
                dataGridView_B_src->Rows[0]->Cells[cell]->Value = "0";

            label88->Text = convert_to_label(dataGridView_B_src);
        }
    }

    int Form1::convert_to_register(System::Windows::Forms::DataGridView^ dataGridView)
    {
        int result = 0,
            num_cells = dataGridView->ColumnCount;

        for(int b = 0, c = num_cells - 1; b < num_cells; ++ b, -- c)
            if(dataGridView->Rows[0]->Cells[c]->Value == "1")
                result |= 1 << b;

        return result;
    }

    String^ Form1::convert_to_label(System::Windows::Forms::DataGridView^ dataGridView)
    {
        int result = 0,
            last_cell = dataGridView->ColumnCount - 1;

        for(int b = 0, c = last_cell; b < last_cell; ++ b, -- c)
            if(dataGridView->Rows[0]->Cells[c]->Value == "1")
                result |= 1 << b;

        if(result)
            if(dataGridView->Rows[0]->Cells[0]->Value == "1")
                result *= -1;

        return ((double)result / (unsigned)(1 << last_cell)).ToString("0.#####");
    }

    void Form1::mp_zero_registers()
    {
        MP_Snapshot reset;
        reset.B       = 0;
        reset.C       = 0;
        reset.D       = 0;
        reset.counter = 0;
        reset.state   = 0;      
        mp_show_registers(reset);

        label43->Text = "";
    }

    void Form1::chip_zero_registers()
    {
        List<Chip_Snapshot>^ reset_chip_snapshots = gcnew List<Chip_Snapshot>;
        Chip_Snapshot reset;
        reset.mem_states     = 0;
        reset.unitary_code   = 0;
        reset.X_vector       = 0;
        reset.mem_conditions = 0;
        reset.Y_vector       = 0;
        reset.D_vector       = 0;
        reset_chip_snapshots->Add(reset);
        chip_show_registers(reset_chip_snapshots, 0);
    }

    // �����
    System::Void Form1::button3_Click(System::Object^ sender, System::EventArgs^ e)
    {
        reset_values();

        mp_reset_color  ();
        chip_reset_color();
    }

    void Form1::reset_values()
    {
        change_management = false;

        run = STOP;

        mp_end   = false;
        chip_end = false;

        mp_right_border      = mp_tact_right_border   = -1;
        chip_right_border    = chip_tact_right_border = -1;

        mp_zero_registers  ();
        chip_zero_registers();
    }

    void Form1::mp_show_registers(MP_Snapshot snapshot)
    {
        set_dataGridView(dataGridView_B_run,   snapshot.B);
        set_dataGridView(dataGridView_C,       snapshot.C);
        set_dataGridView(dataGridView_D,       snapshot.D);
        set_dataGridView(dataGridView_counter, snapshot.counter);
        // set_dataGridView(dataGridView_state,   snapshot.state);

        label84->Text = convert_to_label(dataGridView_B_run);
        label28->Text = convert_to_label(dataGridView_C);

        snapshot.D ? label43->Text = "-" : label43->Text = "+";

        label44->Text = snapshot.counter.ToString();
        // label45->Text = snapshot.state   .ToString();

        if(snapshot.state == 6)
        {
            set_dataGridView(dataGridView_state, 0);
            label45->Text = "0";
        }
        else
        {
            set_dataGridView(dataGridView_state, snapshot.state);
            label45->Text = snapshot.state.ToString();
        }
    }

    void Form1::chip_show_registers(List<Chip_Snapshot>^ chip_snapshots, int right_border)
    {
        int tact = right_border / 6;
        int step = right_border % 6;

        set_dataGridView(dataGridView_mem_states,     chip_snapshots[tact + (step >= 1)].mem_states);
        set_dataGridView(dataGridView_unitary_code,   chip_snapshots[tact + (step >= 2)].unitary_code);
        set_dataGridView(dataGridView_X_vector,       chip_snapshots[tact + (step >= 4)].X_vector);
        set_dataGridView(dataGridView_mem_conditions, chip_snapshots[tact + (step >= 1)].mem_conditions);
        set_dataGridView(dataGridView_Y_vector,       chip_snapshots[tact + (step >= 3)].Y_vector);
        set_dataGridView(dataGridView_D_vector,       chip_snapshots[tact + (step >= 5)].D_vector);

        int X_common = _get_range(chip_snapshots[tact + (step >= 4)].X_vector, 9, 8) << 3 | _get_range(chip_snapshots[tact + (step >= 4)].X_vector, 2, 0),
            X_save   = _get_range(chip_snapshots[tact].X_vector, 7, 3);
        set_dataGridView(dataGridView_X_common,       X_common);
        set_dataGridView(dataGridView_X_save,         X_save);
    }

    void Form1::set_dataGridView(System::Windows::Forms::DataGridView^ dataGridView, int _register)
    {
        int num_cells = dataGridView->ColumnCount,
            b = 0,
            c = 0;

        for(b = 0, c = num_cells - 1; b < num_cells; ++ b, -- c)
            if(_register & 1 << b)      dataGridView->Rows[0]->Cells[c]->Value = "1";
            else                        dataGridView->Rows[0]->Cells[c]->Value = "0";
    }

    void Form1::switch_GSA(bool src)
    {
        elements[RECT]$[0]->set_text(src ? "C:=0\nD:=B(15)\nB(15):=0\n��:=0"                                        : "y0\ny1\ny2\ny3");
        elements[RECT]$[1]->set_text(src ? "C:=0"                                                                   : "y0");
        elements[RECT]$[2]->set_text(src ? "C(31:15):=C(31:15)+A(14:0)"                                             : "y4");
        elements[RECT]$[3]->set_text(src ? "C(31:15):=C(31:15)+A(14:0).0"                                           : "y5");
        elements[RECT]$[4]->set_text(src ? "C(31:15):=C(31:15)+11.�A(14:0)+1\nB(15:0):=B(15:0)+1"                   : "y6\ny7");
        elements[RECT]$[5]->set_text(src ? "B(15:0):=R2(00.B(15:0))\nC(31:0):=R2(C(31).C(31).C(31:0))\n��:=��-1"    : "y8\ny9\ny10");
        elements[RECT]$[6]->set_text(src ? "��:=��-1"                                                               : "y10");
        elements[RECT]$[7]->set_text(src ? "C(30:16):=C(30:16)+1"                                                   : "y11");
        elements[RECT]$[8]->set_text(src ? "C(31):=1"                                                               : "y12");
        elements[RECT]$[9]->set_text(src ? "���������"                                                              : "y13");

        elements[RHOMB]$[0]->set_text(src ? "����"          : "x0");
        elements[RHOMB]$[1]->set_text(src ? "A(14:0)=0"     : "x1");
        elements[RHOMB]$[2]->set_text(src ? "B(14:0) = 0"   : "x2");
        elements[RHOMB]$[3]->set_text(src ? "B(1:0) = 00"   : "x3");
        elements[RHOMB]$[4]->set_text(src ? "B(1:0) = 01"   : "x4");
        elements[RHOMB]$[5]->set_text(src ? "B(1:0) = 10"   : "x5");
        elements[RHOMB]$[6]->set_text(src ? "��=1"          : "x6");
        elements[RHOMB]$[7]->set_text(src ? "��=0"          : "x7");
        elements[RHOMB]$[8]->set_text(src ? "C(15)"         : "x8");
        elements[RHOMB]$[9]->set_text(src ? "A(15)?D"       : "x9");
    }

    void Form1::paint_work_chip(int left_border, int right_border)
    {
        for(int step = 0, i = 0; i < 6; ++ i)
            if(left_border > right_border)      break;
            else
            {
                step = right_border % 6;
                right_border --;

                switch(step)
                {
                    case 0:
                    {
                        SP           ->set_color(work_color);
                        SP_MS        ->set_color(work_color);
                        SP_MLC       ->set_color(work_color);

                        break;
                    }
                    case 1:
                    {
                        MS_rect      ->set_color(work_color);
                        MLC_rect     ->set_color(work_color);

                        CLC_D_MS_out ->set_color(work_color);
                        CLC_D_MS_in  ->set_color(work_color);

                        OA_out       ->set_color(work_color);
                        OA_MLC       ->set_color(work_color);

                        break;
                    }
                    case 2:
                    {
                        MS_rect      ->set_color(work_color);
                        DC_trapeze   ->set_color(work_color);

                        MS_DC        ->set_color(work_color);

                        break;
                    }
                    case 3:
                    {
                        DC_trapeze   ->set_color(work_color);
                        MLC_rect     ->set_color(work_color);
                        CLC_Y_trapeze->set_color(work_color);

                        DC           ->set_color(work_color);
                        DC_CLC_Y     ->set_color(work_color);

                        MLC          ->set_color(work_color);

                        OA_out       ->set_color(work_color);
                        OA_in        ->set_color(work_color);
                        OA_CLC_Y     ->set_color(work_color);

                        break;
                    }
                    case 4:
                    {
                        CLC_Y_trapeze->set_color(work_color);
                        OA_rect      ->set_color(work_color);

                        CLC_Y_OA     ->set_color(work_color);

                        break;
                    }
                    case 5:
                    {
                        DC_trapeze   ->set_color(work_color);
                        MLC_rect     ->set_color(work_color);
                        CLC_D_trapeze->set_color(work_color);

                        DC           ->set_color(work_color);
                        DC_CLC_D     ->set_color(work_color);

                        MLC          ->set_color(work_color);

                        OA_out       ->set_color(work_color);
                        OA_in        ->set_color(work_color);
                        OA_CLC_D     ->set_color(work_color);

                        break;
                    }
                }
            }
    }

    System::Void Form1::dataGridView_A_CellPainting             (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_B_src_CellPainting         (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_B_run_CellPainting         (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_C_CellPainting             (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}

    System::Void Form1::dataGridView_D_CellPainting             (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_counter_CellPainting       (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_state_CellPainting         (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}

    System::Void Form1::dataGridView_mem_states_CellPainting    (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_unitary_code_CellPainting  (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_X_vector_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_mem_conditions_CellPainting(System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_Y_vector_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_D_vector_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_X_common_CellPainting      (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}
    System::Void Form1::dataGridView_X_save_CellPainting        (System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)    {CellPainting(sender, e);}

    void Form1::CellPainting(System::Object^  sender, System::Windows::Forms::DataGridViewCellPaintingEventArgs^  e)
    {
        int cell = e->ColumnIndex;
        if(((DataGridView^)sender)->Rows[0]->Cells[cell]->Value == "0")     ((DataGridView^)sender)->Rows[0]->Cells[cell]->Selected = false;
        else                                                                ((DataGridView^)sender)->Rows[0]->Cells[cell]->Selected = true;
    }

    void Form1::get_top_hf_circle_points(array<Point>^ & _points, int radius, int center_X, int center_Y)
    {
        List<Point>^ points = gcnew List<Point>;
        int X_offset = -radius;
        int Y_offset = 0;
        int last = 0;

        while(X_offset <= Y_offset)
        {
            points->Add(Point(X_offset, Y_offset));

            if(Math::Abs(get_distance(X_offset + 1, Y_offset) - radius) < Math::Abs(get_distance(X_offset, Y_offset - 1) - radius)) X_offset += 1;
            else                                                                                                                    Y_offset -= 1;
        }

        last = points->Count - 1;
        for(int i = last - (points[last].X == points[last].Y); i >= 0   ; -- i)         points->Add(Point( points[i].Y, points[i].X));

        last = points->Count - 1;
        for(int i = last - 1                                 ; i >= 0   ; -- i)         points->Add(Point(-points[i].X, points[i].Y));

        last = points->Count - 1;
        for(int i = 0                                        ; i <= last; ++ i)         points[i] = Point(center_X + points[i].X, center_Y + points[i].Y);

        _points->Resize(_points, points->Count);
        _points = points->ToArray();
    }

    double Form1::get_distance(int d1, int d2)      {return Math::Sqrt(d1 * d1 + d2 * d2);}

    System::Void Form1::button1_Click(System::Object^ sender, System::EventArgs^ e)
    {
        if(!run && !mp_end && !chip_end)
        {
            need_update_MP   = true;
            need_update_chip = true;
            set_dataGridView(dataGridView_A, 0);
            label87->Text = convert_to_label(dataGridView_A    );
        }
    }
    System::Void Form1::button2_Click(System::Object^ sender, System::EventArgs^ e)
    {
        if(!run && !mp_end && !chip_end)
        {
            need_update_MP   = true;
            need_update_chip = true;
            set_dataGridView(dataGridView_B_src, 0);
            label88->Text = convert_to_label(dataGridView_B_src);
        }
    }
}
